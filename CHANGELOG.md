# Changelog

All notable changes to this project will be documented in this file.

## 1.2.0 (2024-04-04)
[Issues](https://gitlab.com/cgrewe/emborado-syntax-highlighter/-/issues/?sort=created_date&state=closed&milestone_title=v1.2.0)
[Commits](https://gitlab.com/cgrewe/emborado-syntax-highlighter/-/compare/v1.1.0...v1.2.0) 

## 1.1.0 (2022-08-12)
[Issues](https://gitlab.com/cgrewe/emborado-syntax-highlighter/-/issues/?sort=created_date&state=closed&milestone_title=v1.1.0)
[Commits](https://gitlab.com/cgrewe/emborado-syntax-highlighter/-/compare/v1.0.0...v1.1.0) 

## 1.0.0 (2022-06-09)