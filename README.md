# Emborado Syntax Highlighter

# Description

Emborado Syntax Highlighter enables syntax highlighting of source codes inside of text blocks in Microsoft PowerPoint (2019+) and PowerPoint in Microsoft 365 (On-premises version). The add-in connects the Windows version of PowerPoint with André Simon's excellent [Highlight processor](https://gitlab.com/saalen/highlight). By this over 200 programming languages are supported and you can choose from a bunch of different styles. The GUI language is German or English depending on your settings.

![teaser](docs/teaser.png "Description")
![teaser](docs/highlighter-demo-clip.webm "Video")

# Installation

There is no one-click installer existing, but the manual installation steps are easy:

* Download and install André Simon's Windows version of his **Highlight processor** 4.2 or higher from the [project page](http://andre-simon.de/zip/download.php). You can use either the Windows installer package or the ZIP archive. Choose a proper installation path.

* Download and unzip the package [**Emborado-Syntax-Highlighter-vX.X.X.zip**](https://gitlab.com/cgrewe/emborado-syntax-highlighter/-/releases) to a folder of your choice, for instance _C:\Tools_.

* Start **PowerPoint**
*  For the integration of the add-in, the developer tools must be enabled in PowerPoint. If the *Developer* tab is not visible, go to *File > Options > Customise Ribbon* and enable the *Developer tools*. 

![options](docs/customise-ribbon-eng.png "Options")

* Go to the *Developer* ribbon tab and click to button *PowerPoint Add-Ins*. Next click on the *Add New...* button and choose the file **Emborado Syntax Highlighter v1.X.X.ppam** from the unzipped archive.

![dev-ribbon](docs/dev-ribbon-eng.png "Developer Tools")

* PowerPoint informs you about the security risk by installing an additional add-in. This warning is very important, because you should never blindly trust other people's code :-) So, if you have any doubts or you are not allowed to install add-ins, then stop here.

![risk-warning](docs/pp-macro-warning-eng.png "Macro warning")

* Otherwise, you can take a look to the source code and accept it by pressing *Enable macros*. The add-in is activated now. You immediately see a welcome message.

![welcome](docs/welcome-eng.png "Welcome")

* By clicking the *OK* button an initialization dialog opens. You have to configure the folder, where the Highlight processor is installed. 

* After setting the right folder, the background color changes to green and you can press *OK*.

![init-dialog](docs/init-dialog-filled-eng.png "Initialization dialog")

# Deinstallation
* Go to the *Developer* ribbon tab and click to button *PowerPoint Add-Ins*. Select the entry *Emborado Syntax Highlighter vX.X.X* in the list and press  *Remove...* button.

# Usage

## First Steps

After installation the language and style menus are still empty for good reasons. The external Highlight processor supports over 250 programming languages and round about 200 themes. Putting all entries to the checkboxes leads to awkwark working. 

![ribbon-tab](docs/ribbon-tab-eng.png "Highlight ribbon tab")

Usually, a user handles only with a small number of programming languages and a handful of themes. For this reason the user can preselect the visible checkbox entries by preference dialogs. Click to the *Preferences* button in the *Highlighting* ribbon tab.

In the pages "Languages" and "Styles" select those entries you really need. Move them from the left to the right side by the arrow buttons. You can also change their position in the list.

![langs-preselection](docs/pref-dialog-languages-eng.png "Language preselection")

![styles-preselection](docs/pref-dialog-styles-eng.png "Style preselection")

After closing the preference dialog, the *Highlighting* ribbon tab is refreshed.

![ribbon-tab](docs/ribbon-tab-with-selection-eng.png "Highlight ribbon tab")

## Menu Entries

In the *Highlighting* ribbon tab you can set the control parameters for the external Highlight processor:

![ribbon-tab-numbered](docs/ribbon-tab-numbered-eng.png "Highlight ribbon tab")

1. **Language**: preselected list of supported programming languages
2. **Style**: preselected list of supported highlighting themes
3. **Font**
     - no assignment: preserve the current font
     - Consolas
     - Courier
     - Lucida Console
     - up to four user defined system fonts (see preferences)
4. **Background color (of text box)**
     -  no assignment: preserve the current background color
     -  opaque: set the background opaque
     -  according to style: the background color is taken from the chosen theme style
     -  color 1: use user-defined color 1 (see preferences)
     -  color 2: use user-defined color 2 (see preferences)
5. **Frame color (of text block)**
     -  no assignment: preserve the current frame color
     -  opaque: set the frame opaque
     -  color 1: use user-defined color 1 (see preferences)
     -  color 2: use user-defined color 2 (see preferences)

## Highlighting

Now it's time to bring color to your source code. Select a single text block including source code. Select language, style and font. Press the *Highlight text block* button. A small progress bar informs you about the process. 

![text-block-without-highlighting](docs/textblock-wo-hl.png "Text block without highlighting")

Tada!

![text-block-without-highlighting](docs/textblock-with-hl.png "Text block with highlighting")

## Roadmap

The matching counterpart for Word is missing ...

## Limitations

The slowness of the highlighting process mainly results from the really slow start of Powershell that I need to transport the text block content to the highlight processor and back. Another reason is the necessary waiting time until the data has been copied to the clipboard and is available in PowerPoint. 

The internal lookup for system fonts is limitied to a maximum of 300 fonts. Their handling is time consuming and leads to waiting times.

Some virus scanners, e.g. Sophos, do not like the way how the external highlight processor is started by the VBA code inside PowerPoint and interrupt it (starting external code is unfortunately also popular in malicious code). For details see method <code>RunHighlightProcessor()</code> inside [ESHExternalProcessor.bas](src/vba-ppt/ESHExternalProcessor.bas). 

## Contributing

* If anyone is interested in translating the GUI texts to another language, you are welcome! Have a look on the file [ESHLocalization.bas](src/vba-ppt/ESHLocalization.bas).
* Perhaps someone will also be found who would like to try out the plug-in with PowerPoint 2016. I would be very happy to receive a feedback.

## Acknowledgment

jennybc provided an important tip on integrating the clipboard. Thanks for the great [post](https://rdrr.io/github/jennybc/reprex/f/internal/write-clipboard-rtf-windows.md).

## License

This software is released under the terms of the GNU General Public License. 
For more information about these matters, see the file named [LICENSE](LICENSE).

# Background

For my job I have been creating several hundreds of slides including source code sniplets. And I have looked out for a integrated solution in PowerPoint for a long time. I didn't find a suitable solution for my purposes. So I decided to create my own one as a spare time project. 

I set myself an important constraint: Use as few additional technologies and development tools as possible. In the best case all needed components are already available on a machine with Windows 10 and PowerPoint 2019. In the first experiments I started with Python and a Python-based highlighting processor. This solution worked far quicker, however the visual result was disappointing due to some strange interpretation of RTF in PowerPoint 2019. This finally led to VBA and Powershell.

I could persuade myself to deal with VBA by the well-known advice "learn a new language every year". I interpreted the adjective "new" as "unknown to me", not "fresh" ;-). And I learnt so much: Java and Python are both old languages too, but really beautiful ...

