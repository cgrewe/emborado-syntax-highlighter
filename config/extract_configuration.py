# -*- coding: utf-8 -*-
#!/usr/bin/python
#
# highlight data extractor
#
# This script extracxts the known themes and supported languages
# from a highlight installation in file system. The result is a 
# XML configuration for the MS Office add-ins.
#
# (C) 2022, Claus Grewe
#
# external dependencies: lxml
#
import sys
import os
import glob
import re
import datetime
from lxml import etree

# Reads the language data from a language definition
def readLanguage(dir_path):
  f = open(dir_path, encoding='utf-8', errors='ignore')
  #print(f)      	  
  lines = f.readlines()
  param = os.path.basename(dir_path).replace(".lang","")
      
  for line in lines:		
    if line.startswith("Description"):
      #print(line)
      desc = re.split("\"", line)[1]
      #print("--" , desc)
      break
	  
  return param, desc

# Reads the theme data from a standard path
def readTheme(dir_path):
  f = open(dir_path)
      	  
  lines = f.readlines()
  param = os.path.basename(dir_path).replace(".theme","")
      
  for line in lines:		
    if "Description" in line:
      desc =  line.split("\"")[1] 
		  
    if "Canvas" in line:
      color = line.split("\"")[1].replace("#","")
		  	
  return param, desc, color
  
# Reads the theme data from a base16 path 
def readBase16Theme(dir_path):
  f = open(dir_path)
      	  
  lines = f.readlines()
  param = os.path.basename(dir_path).replace(".theme","")
      
  for line in lines:		
    if "Description" in line:
      desc =  line.split("\"")[1] 
		  
    if "base00 =" in line:
      color = line.split("\"")[1].replace("#", "") 
		  	
  return param, desc, color
  
# 
# main method
#
if __name__ == '__main__':

  if (len(sys.argv) != 2):
    print("Please define the base path of your highlight installation ...")
    sys.exit()

  basepath = sys.argv[1]
 
  attr_qname = etree.QName("http://www.w3.org/2001/XMLSchema-instance", "noNamespaceSchemaLocation")
  
  nsmap = {"xsi": "http://www.w3.org/2001/XMLSchema-instance"}
	
  dt = datetime.datetime.now()
	
  the_doc = etree.Element("syntax-highlighter", {attr_qname: "emborado-syntax-highlighter.xsd"}, nsmap=nsmap, version="1.0",  created=dt.strftime("%G-%m-%dT%H:%M:%S"))
  languages = etree.SubElement(the_doc, "languages")
  styles = etree.SubElement(the_doc, "styles")
  
  #
  # get languages
  #
  print("Retrieving language data from %s/langDefs ..." % basepath)
  for dir_path in glob.glob(basepath + "/langDefs/*.lang"):
    if os.path.isfile(dir_path): 
      param, desc = readLanguage(dir_path)
      lang = etree.Element("language")
      lang.attrib["name"] = desc
      lang.attrib["id"] = "lang-" + param
      lang.attrib["parameter"] = param
      languages.append(lang)

  #
  # get themes
  #
  print("Retrieving theme data from %s/themes ..." % basepath)
  
  for dir_path in glob.glob(basepath + "/themes/*.theme"):
    if os.path.isfile(dir_path): 
      param, desc, color = readTheme(dir_path)
      style = etree.Element("style")
      style.attrib["name"] = desc
      style.attrib["id"] = "style-" + param
      style.attrib["parameter"] = param
      style.attrib["background-color"] = color
      styles.append(style)
	    
  print("Retrieving theme data from %s/themes/base16 ..." % basepath)
  
  for dir_path in glob.glob(basepath + "/themes/base16/*.theme"):
    if os.path.isfile(dir_path): 
      param, desc, color = readBase16Theme(dir_path)
      style = etree.Element("style")   
      style.attrib["name"] = desc
      style.attrib["id"] = "style-base16-" + param
      style.attrib["parameter"] = "base16\\" + param
      style.attrib["background-color"] = color
      styles.append(style)
 
  
  #print(etree.tostring(the_doc, pretty_print=True))
  
  # write result to output file
  et = etree.ElementTree(the_doc)
  et.write('syntax-highlighter.xml', encoding="UTF-8", pretty_print=True, xml_declaration=True)
