VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Configuration: external configuration handler
' ----------------------------------------------------------------------------
Option Explicit

' Member variable
Private m_ConfigurationOkay As Boolean
Private colStyles As Collection
Private colLanguages As Collection
Private colFonts As Collection

'
' == Ctor ==
'
Private Sub Class_Initialize()
  m_ConfigurationOkay = False
  Set colStyles = New Collection
  Set colLanguages = New Collection
  Set colFonts = Nothing ' needed for refreshing logic below
        
  ' ----------------------------------------------
  ' look for style entries in configuration
  ' ----------------------------------------------
  ' set default language entry
  Dim oStyleDef As ESHStyleDefinition
  Set oStyleDef = New ESHStyleDefinition
  
  oStyleDef.InitiateProperties id:=ESHStyleDropdownMenu.STYLEID_DEFAULT, _
                               name:="Standard", _
                               parameter:="base16\default-light", _
                               bgColorHex:="f8f8f8"
  colStyles.Add oStyleDef, oStyleDef.id
    
  ' ----------------------------------------------
  ' look for language entries in configuration
  ' ----------------------------------------------
  ' set default language entry
  Dim oLangDef As ESHLanguageDefinition
  Set oLangDef = New ESHLanguageDefinition
  
  oLangDef.InitiateProperties id:=ESHLanguageDropdownMenu.LANGID_DEFAULT, _
                              name:="Standard", _
                              parameter:="guess"
  colLanguages.Add oLangDef, oLangDef.id
      
  ' ----------------------------------------------
  ' get static or XML configuration for styles
  ' languages
  ' ----------------------------------------------
  If (ESHPrefs.GetUseXmlConfigFile = 1) Then
    ESHHighlightData.AddHighlightDataXML colLanguages, colStyles
    ESHLogging.logDebug "Use XML configuration", "ESHConfiguration"
  Else
    ESHHighlightData.AddHighlightDataStatic colLanguages, colStyles
    ESHLogging.logDebug "Use static configuration", "ESHConfiguration"
  End If
     
  ' ----------------------------------------------
  ' look for fonts in configuration
  ' ----------------------------------------------
  RefreshFontList
  
  m_ConfigurationOkay = True
End Sub


' -----------------------------------------------
' refresh font collection from configuration
' -----------------------------------------------
Private Sub RefreshFontList()
  Dim strFontName As String
  
  If colFonts Is Nothing Then
    Set colFonts = New Collection

    colFonts.Add createFontEntry("Default", FONTID_NONE), FONTID_NONE
    colFonts.Add createFontEntry("Courier New", FONTID_COURIER_NEW), FONTID_COURIER_NEW
    colFonts.Add createFontEntry("Consolas", FONTID_CONSOLAS), FONTID_CONSOLAS
    colFonts.Add createFontEntry("Lucida Console", FONTID_LUCIDA_CONSOLE), FONTID_LUCIDA_CONSOLE
  
    strFontName = ESHPrefs.GetUserDefinedFont(1)
    If strFontName <> "" Then
      colFonts.Add createFontEntry(strFontName, FONTID_USER_DEFINED_1), FONTID_USER_DEFINED_1
    End If
  
    strFontName = ESHPrefs.GetUserDefinedFont(2)
    If strFontName <> "" Then
      colFonts.Add createFontEntry(strFontName, FONTID_USER_DEFINED_2), FONTID_USER_DEFINED_2
    End If
  
    strFontName = ESHPrefs.GetUserDefinedFont(3)
    If strFontName <> "" Then
      colFonts.Add createFontEntry(strFontName, FONTID_USER_DEFINED_3), FONTID_USER_DEFINED_3
    End If
  
    strFontName = ESHPrefs.GetUserDefinedFont(4)
    If strFontName <> "" Then
      colFonts.Add createFontEntry(strFontName, FONTID_USER_DEFINED_4), FONTID_USER_DEFINED_4
    End If
   
    ESHLogging.logDebug "font list refreshed", "PrepareFontList"
  End If
  
End Sub

Private Function createFontEntry(name As String, id As String) As ESHFontDefinition
  Dim oFontDef As New ESHFontDefinition
  oFontDef.InitiateProperties name, id
  Set createFontEntry = oFontDef
End Function

' -----------------------------------------------------
' invalidate the font list after configuration changes
' -----------------------------------------------------
Public Sub InvalidateFontList()
  ESHLogging.logTrace "invalidate font list", "InvalidateFontList"
  Set colFonts = Nothing
End Sub

'
' == Finder ==
'
Public Function FindLanguageDefinitionByName(name As String) As ESHLanguageDefinition

  Dim i As Integer
  Dim oLangDef As ESHLanguageDefinition
  
  For i = 1 To colLanguages.count
    Set oLangDef = colLanguages.Item(i)
    If oLangDef.name = name Then
      Set FindLanguageDefinitionByName = oLangDef
      Exit Function
    End If
  Next
  
End Function

Public Function FindStyleDefinitionByName(name As String) As ESHStyleDefinition

  Dim i As Integer
  Dim oStyleDef As ESHStyleDefinition
  
  For i = 1 To colStyles.count
    Set oStyleDef = colStyles.Item(i)
    If oStyleDef.name = name Then
      Set FindStyleDefinitionByName = oStyleDef
      Exit Function
    End If
  Next
  
End Function

Public Function FindStyleDefinitionById(id As String) As ESHStyleDefinition

  Dim i As Integer
  Dim oStyleDef As ESHStyleDefinition
  
  For i = 1 To colStyles.count
    Set oStyleDef = colLanguages.Item(i)
    If oStyleDef.id = id Then
      Set FindStyleDefinitionById = oStyleDef
      Exit Function
    End If
  Next
  
End Function

Public Function FindFontDefinitionById(id As String) As ESHFontDefinition

  Dim i As Integer
  Dim oFontDef As ESHFontDefinition
  
  For i = 1 To colFonts.count
    Set oFontDef = colLanguages.Item(i)
    If oFontDef.id = id Then
      Set FindFontDefinitionById = oFontDef
      Exit Function
    End If
  Next
  
End Function

'
' == Properties ==
'
Property Get ConfigurationOkay() As Boolean
  ConfigurationOkay = m_ConfigurationOkay
End Property

Property Get LanguageCount() As Integer
  LanguageCount = colLanguages.count
End Property

Property Get StyleCount() As Integer
  StyleCount = colStyles.count
End Property

Property Get FontCount() As Integer
  RefreshFontList
  FontCount = colFonts.count
End Property

Function SupportedStyles() As Collection
  Set SupportedStyles = colStyles
End Function

Function SupportedLanguages() As Collection
  Set SupportedLanguages = colLanguages
End Function

Function SupportedFonts() As Collection
  RefreshFontList
  Set SupportedFonts = colFonts
End Function

Public Function ToString() As String
  ToString = "HLConfiguration (languages=" & LanguageCount & _
             ", styles=" & StyleCount & ", " & FontCount & ")"
End Function


