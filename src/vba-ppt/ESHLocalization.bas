Attribute VB_Name = "ESHLocalization"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Localization: handler for translating IDs to text messages
' ----------------------------------------------------------------------------
Option Explicit

' ====================================================
' central localization module
' ====================================================

Public Const MSGID_RIBBON_MENUE_TAB_HEADER As String = "RIBBON_MENUE_TAB_HEADER"

Public Const MSGID_RIBBON_GRP_HIGHLIGHT As String = "RIBBON_GRP_HIGHLIGHT"
Public Const MSGID_RIBBON_GRP_FONT As String = "RIBBON_GRP_FONT"
Public Const MSGID_RIBBON_GRP_PREFS As String = "RIBBON_GRP_PREFS"
Public Const MSGID_RIBBON_GRP_DEVELOPMENT As String = "RIBBON_GRP_DEVELOPMENT"

Public Const MSGID_RIBBON_BTN_HIGHLIGHT As String = "RIBBON_BTN_HIGHLIGHT"
Public Const MSGID_RIBBON_BTN_IMPORT As String = "RIBBON_BTN_IMPORT"
Public Const MSGID_RIBBON_BTN_PREFS As String = "RIBBON_BTN_PREFS"

Public Const MSGID_RIBBON_DD_LANGUAGE As String = "RIBBON_DD_LANGUAGE"
Public Const MSGID_RIBBON_DD_STYLE As String = "RIBBON_DD_STYLE"
Public Const MSGID_RIBBON_DD_FONT_TYPE As String = "RIBBON_DD_FONT_TYPE"
Public Const MSGID_RIBBON_DD_BGC_STRATEGY As String = "RIBBON_DD_BGC_STRATEGY"
Public Const MSGID_RIBBON_DD_FRAME_STRATEGY As String = "RIBBON_DD_FRAME_STRATEGY"

Public Const MSGID_RIBBON_DDLBL_BGC_NO_ASSIGNMENT As String = "RIBBON_DDLBL_BGC_NO_ASSIGNMENT"
Public Const MSGID_RIBBON_DDLBL_BGC_TRANSPARENT As String = "RIBBON_DDLBL_BGC_TRANSPARENT"
Public Const MSGID_RIBBON_DDLBL_BGC_LIKE_STYLE As String = "RIBBON_DDLBL_BGC_LIKE_STYLE"
Public Const MSGID_RIBBON_DDLBL_BGC_COLOR1 As String = "RIBBON_DDLBL_BGC_COLOR1"
Public Const MSGID_RIBBON_DDLBL_BGC_COLOR2 As String = "RIBBON_DDLBL_BGC_COLOR2"

Public Const MSGID_RIBBON_DDLBL_FC_NO_ASSIGNMENT As String = "RIBBON_DDLBL_FC_NO_ASSIGNMENT"
Public Const MSGID_RIBBON_DDLBL_FC_TRANSPARENT As String = "RIBBON_DDLBL_FC_TRANSPARENT"
Public Const MSGID_RIBBON_DDLBL_FC_COLOR1 As String = "RIBBON_DDLBL_FC_COLOR1"
Public Const MSGID_RIBBON_DDLBL_FC_COLOR2 As String = "RIBBON_DDLBL_FC_COLOR2"

Public Const MSGID_RIBBON_DDLBL_FONT_NO_ASSIGNMENT As String = "RIBBON_DDLBL_FONT_NO_ASSIGNMENT"

' ----------------
' -- form prefs --
' ----------------
Public Const MSGID_PREFS As String = "PREFS"

' pages
Public Const MSGID_PREFS_PG_GENERAL As String = "PREFS_PG_GENERAL"
Public Const MSGID_PREFS_PG_LANGUAGE As String = "PREFS_PG_LANGUAGE"
Public Const MSGID_PREFS_PG_STYLE As String = "PREFS_PG_STYLE"
Public Const MSGID_PREFS_PG_FONT As String = "PREFS_PG_FONT"
Public Const MSGID_PREFS_PG_EXTENDED As String = "PREFS_PG_EXTENDED"
Public Const MSGID_PREFS_PG_ABOUT As String = "PREFS_PG_ABOUT"

' frames
Public Const MSGID_PREFS_FRM_PROCESSOR_PATH As String = "PREFS_FRM_PROCESSOR_PATH"
Public Const MSGID_PREFS_FRM_BG_COLOR As String = "PREFS_FRM_BG_COLOR"
Public Const MSGID_PREFS_FRM_FRAME_COLOR As String = "PREFS_FRM_FRAME_COLOR"
Public Const MSGID_PREFS_FRM_EXT_HIGHLIGHT_CONFIG As String = "FRM_EXT_HIGHLIGHT_CONFIG"
Public Const MSGID_PREFS_FRM_ADDITIONAL_FONTS As String = "PREFS_FRM_ADDITIONAL_FONTS"

' labels
Public Const MSGID_PREFS_LBL_PROCESSOR_PATH As String = "PREFS_LBL_PROCESSOR_PATH"
Public Const MSGID_PREFS_LBL_XML_CONFIG_FILE As String = "PREFS_LBL_XML_CONFIG_FILE"
Public Const MSGID_PREFS_LBL_COLOR As String = "PREFS_LBL_COLOR"
Public Const MSGID_PREFS_LBL_SUPPORTED_LANGS As String = "PREFS_LBL_SUPPORTED_LANGS"
Public Const MSGID_PREFS_LBL_CHOSEN_LANGS As String = "PREFS_LBL_CHOSEN_LANGS"
Public Const MSGID_PREFS_LBL_DESCRIPTION_LANGS As String = "PREFS_LBL_DESCRIPTION_LANGS"
Public Const MSGID_PREFS_LBL_SUPPORTED_STYLES As String = "PREFS_LBL_SUPPORTED_STYLES"
Public Const MSGID_PREFS_LBL_CHOSEN_STYLES As String = "PREFS_LBL_CHOSEN_STYLES"
Public Const MSGID_PREFS_LBL_DESCRIPTION_STYLES As String = "PREFS_LBL_DESCRIPTION_STYLES"
Public Const MSGID_PREFS_LBL_EXT_HIGHLIGHT_CONFIG As String = "PREFS_LBL_EXT_HIGHLIGHT_CONFIG"
Public Const MSGID_PREFS_LBL_ABOUT_TEXT As String = "PREFS_LBL_ABOUT_TEXT"
Public Const MSGID_PREFS_LBL_USER_DEFINED_FONT As String = "PREFS_LBL_USER_DEFINED_FONT"
Public Const MSGID_PREFS_LBL_DESCRIPTION_FONTS As String = "PREFS_LBL_DESCRIPTION_FONTS"
Public Const MSGID_PREFS_LBL_FONT_SELECTOR_HINT As String = "PREFS_LBL_FONT_SELECTOR_HINT"

' command button
Public Const MSGID_PREFS_CMD_CLOSE As String = "PREFS_CMD_CLOSE"

' checkboxes
Public Const MSGID_PREFS_CB_USE_EXT_HIGHLIGHT_CONFIG As String = "PREFS_CB_USE_EXT_HIGHLIGHT_CONFIG"

' dialogs
Public Const MSGID_PREFS_DLG_LOAD_PROCESSOR_PATH As String = "PREFS_DLG_LOAD_PROCESSOR_PATH"
Public Const MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE As String = "PREFS_DLG_LOAD_XML_CONFIG_FILE"
Public Const MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE_TYPE As String = "PREFS_DLG_LOAD_XML_CONFIG_FILE_TYPE"

' ----------------
' -- form init  --
' ----------------
Public Const MSGID_INIT As String = "INIT"
Public Const MSGID_INIT_BASE_PATH As String = "INIT_BASE_PATH"
Public Const MSGID_INIT_TEXT_INFO As String = "INIT_TEXT_INFO"
Public Const MSGID_INIT_CMD_OK As String = "INIT_CMD_OK"
Public Const MSGID_INIT_DLG_HIGHLIGHT_PATH As String = "INIT_DLG_HIGHLIGHT_PATH"

' ---------------------
' -- form status bar --
' ---------------------
Public Const MSGID_STATUS As String = "STATUS"
Public Const MSGID_STATUS_FRM_DONE As String = "STATUS_FRM_DONE"

' -------------------------
' -- form font selection --
' -------------------------
Public Const MSGID_FNTSEL As String = "FNTSEL"
Public Const MSGID_FNTSEL_FRM_FONT_PREVIEW As String = "FNTSEL_FRM_FONT_PREVIEW"
Public Const MSGID_FNTSEL_LBL_COLUMN_FONT As String = "FNTSEL_LBL_COLUMN_FONT"
Public Const MSGID_FNTSEL_LBL_COLUMN_MONOSPACE As String = "FNTSEL_LBL_COLUMN_MONOSPACE"
Public Const MSGID_FNTSEL_CMD_OK As String = "FNTSEL_CMD_OK"
Public Const MSGID_FNTSEL_CMD_CANCLEL As String = "FNTSEL_CMD_CANCEL"

' ----------------
' -- base logic --
' ----------------
Public Const MSGID_BASE_WELCOME As String = "BASE_WELCOME"
Public Const MSGID_BASE_CONF_CHECK As String = "BASE_CONF_CHECK"
Public Const MSGID_BASE_ONLY_ONE_TEXT_OBJECT As String = "BASE_ONLY_ONE_TEXT_OBJECT"
Public Const MSGID_BASE_FOUND_NO_TEXT_OBJECT As String = "BASE_FOUND_NO_TEXT_OBJECT"
Public Const MSGID_BASE_FOUND_NO_TEXT_IN_OBJECT As String = "BASE_FOUND_NO_TEXT_IN_OBJECT"
Public Const MSGID_BASE_FOUND_WRONG_OBJECT_TYPE As String = "BASE_FOUND_WRONG_OBJECT_TYPE"

' ---------------------
' file/folder dialogs
' ---------------------
Public Const MSGID_UTILS_FILE_DLG_CHOOSE As String = "UTILS_FILE_DLG_CHOOSE"
Public Const MSGID_UTILS_FOLDER_DLG_CHOOSE As String = "UTILS_FOLDER_DLG_CHOOSE"

' ---------------------
' -- HLConfiguration --
' ---------------------
Public Const MSGID_CFG_XML_PROCESSING_ERROR As String = "CFG_XML_PROCESSING_ERROR"
           
' ------------------------------------------
' member variable
' ------------------------------------------

Private colLocMsgs As Collection  ' singleton


' =======================================
' P U B L I C
' =======================================

' translate the message id to a localized text
Public Function GetMsg(sMsgId As String) As String

  If colLocMsgs Is Nothing Then
    Set colLocMsgs = CreateLocalizedMessages
  End If
  
  On Error GoTo missingKey
  GetMsg = colLocMsgs.Item(sMsgId)
  Exit Function
  
missingKey:
  GetMsg = "???"
  ESHLogging.logError "The given msgId [" & sMsgId & "] does not exist.", "GetMsg"
End Function

' =======================================
' I N T E R N A L
' =======================================

' creates a localized version of the text message collection
Private Function CreateLocalizedMessages() As Collection

  Dim colLocMsgs As Collection
  Set colLocMsgs = New Collection
  
  Dim guiLangId As Long
  guiLangId = Application.LanguageSettings.languageId(msoLanguageIDUI)
  
  ' for developing purposes a "hidden" registry key allows to overwrite the found lang id
  guiLangId = ESHPrefs.GetDevGuiLanguageId(guiLangId)
  
  ' a localized messages to collection
  Select Case guiLangId
    Case msoLanguageIDGerman, msoLanguageIDGermanAustria, msoLanguageIDGermanLiechtenstein, msoLanguageIDGermanLuxembourg
      ESHLogging.logTrace "found UI language German", "CreateLocalizedMessages"
      AddGermanTexts colLocMsgs
    Case Else
      ESHLogging.logTrace "use default UI language English", "CreateLocalizedMessages"
      AddEnglishTexts colLocMsgs
  End Select

  Set CreateLocalizedMessages = colLocMsgs
End Function

' set up all german GUI text messages
Sub AddGermanTexts(colLocMsgs As Collection)

  ' ribbon
  colLocMsgs.Add "Syntaxhervorhebung", MSGID_RIBBON_MENUE_TAB_HEADER
  colLocMsgs.Add "Syntaxhervorhebung", MSGID_RIBBON_GRP_HIGHLIGHT
  colLocMsgs.Add "Schriftart", MSGID_RIBBON_GRP_FONT
  colLocMsgs.Add "Einstellungen", MSGID_RIBBON_GRP_PREFS
  colLocMsgs.Add "Entwicklung", MSGID_RIBBON_GRP_DEVELOPMENT
      
  ' ribbon buttons
  colLocMsgs.Add "Syntaxhervorhebung Textblock", MSGID_RIBBON_BTN_HIGHLIGHT
  colLocMsgs.Add "Zwischenablage importieren", MSGID_RIBBON_BTN_IMPORT
  colLocMsgs.Add "Einstellungen", MSGID_RIBBON_BTN_PREFS
    
  ' ribbon dropdown menues
  colLocMsgs.Add "Sprache", MSGID_RIBBON_DD_LANGUAGE
  colLocMsgs.Add "Stil", MSGID_RIBBON_DD_STYLE
  colLocMsgs.Add "Schriftart", MSGID_RIBBON_DD_FONT_TYPE
  colLocMsgs.Add "Hintergrundfarbe", MSGID_RIBBON_DD_BGC_STRATEGY
  colLocMsgs.Add "Rahmenfarbe", MSGID_RIBBON_DD_FRAME_STRATEGY

  ' colors in background dropdown menue
  colLocMsgs.Add "keine Zuweisung", MSGID_RIBBON_DDLBL_BGC_NO_ASSIGNMENT
  colLocMsgs.Add "transparent", MSGID_RIBBON_DDLBL_BGC_TRANSPARENT
  colLocMsgs.Add "gem�� Stilvorgabe", MSGID_RIBBON_DDLBL_BGC_LIKE_STYLE
  colLocMsgs.Add "Hintergrundfarbe 1", MSGID_RIBBON_DDLBL_BGC_COLOR1
  colLocMsgs.Add "Hintergrundfarbe 2", MSGID_RIBBON_DDLBL_BGC_COLOR2
      
  ' colors in frame dropdown menue
  colLocMsgs.Add "keine Zuweisung", MSGID_RIBBON_DDLBL_FC_NO_ASSIGNMENT
  colLocMsgs.Add "transparent", MSGID_RIBBON_DDLBL_FC_TRANSPARENT
  colLocMsgs.Add "Rahmenfarbe 1", MSGID_RIBBON_DDLBL_FC_COLOR1
  colLocMsgs.Add "Rahmenfarbe 2", MSGID_RIBBON_DDLBL_FC_COLOR2
      
  ' fonts in font dropdown menue
  colLocMsgs.Add "keine Zuweisung", MSGID_RIBBON_DDLBL_FONT_NO_ASSIGNMENT
  
  ' form prefs
  colLocMsgs.Add "Einstellungen", MSGID_PREFS
  colLocMsgs.Add "Allgemein", MSGID_PREFS_PG_GENERAL
  colLocMsgs.Add "Sprachen", MSGID_PREFS_PG_LANGUAGE
  colLocMsgs.Add "Stile", MSGID_PREFS_PG_STYLE
  colLocMsgs.Add "Schriftarten", MSGID_PREFS_PG_FONT
  colLocMsgs.Add "Erweitert", MSGID_PREFS_PG_EXTENDED
  colLocMsgs.Add "�ber", MSGID_PREFS_PG_ABOUT
    
  colLocMsgs.Add "Highlight", MSGID_PREFS_FRM_PROCESSOR_PATH
  colLocMsgs.Add "Hintergrundfarben", MSGID_PREFS_FRM_BG_COLOR
  colLocMsgs.Add "Rahmenfarben", MSGID_PREFS_FRM_FRAME_COLOR
      
  colLocMsgs.Add "Installationspfad", MSGID_PREFS_LBL_PROCESSOR_PATH
  colLocMsgs.Add "Konfigurationsdatei", MSGID_PREFS_LBL_XML_CONFIG_FILE
  colLocMsgs.Add "Farbe", MSGID_PREFS_LBL_COLOR
      
  colLocMsgs.Add "Unterst�tzte Sprachen", MSGID_PREFS_LBL_SUPPORTED_LANGS
  colLocMsgs.Add "Gew�hlte Sprachen", MSGID_PREFS_LBL_CHOSEN_LANGS
  colLocMsgs.Add "W�hlen Sie die Sprachen aus, die in der Men�-Auswahl sichtbar sein sollen.", MSGID_PREFS_LBL_DESCRIPTION_LANGS
  
  colLocMsgs.Add "Unterst�tzte Stile", MSGID_PREFS_LBL_SUPPORTED_STYLES
  colLocMsgs.Add "Gew�hlte Stile", MSGID_PREFS_LBL_CHOSEN_STYLES
  colLocMsgs.Add "W�hlen Sie die Stile aus, die in der Men�-Auswahl sichtbar sein sollen.", MSGID_PREFS_LBL_DESCRIPTION_STYLES
  colLocMsgs.Add "Schlie�en", MSGID_PREFS_CMD_CLOSE
    
  colLocMsgs.Add "Highlight-Installationspfad ausw�hlen", MSGID_PREFS_DLG_LOAD_PROCESSOR_PATH
  colLocMsgs.Add "XML-Konfigurationsdatei ausw�hlen", MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE
  colLocMsgs.Add "XML-Dateien", MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE_TYPE

  colLocMsgs.Add "Externe XML-Konfigurationsdatei", MSGID_PREFS_FRM_EXT_HIGHLIGHT_CONFIG
  colLocMsgs.Add "XML-Konfigurationsdatei", MSGID_PREFS_LBL_EXT_HIGHLIGHT_CONFIG
  colLocMsgs.Add "Externe Konfiguration f�r Sprachen und Stile nutzen", MSGID_PREFS_CB_USE_EXT_HIGHLIGHT_CONFIG

  colLocMsgs.Add "Weitere Schriftarten", MSGID_PREFS_FRM_ADDITIONAL_FONTS
  colLocMsgs.Add "Schriftart", MSGID_PREFS_LBL_USER_DEFINED_FONT
  colLocMsgs.Add "W�hlen Sie die weiteren Schriftarten aus, die in der Men�-Auswahl sichtbar sein sollen.", MSGID_PREFS_LBL_DESCRIPTION_FONTS
  colLocMsgs.Add "Hinweis: Das Auslesen der Systemschriftarten ben�tigt etwas Zeit.", MSGID_PREFS_LBL_FONT_SELECTOR_HINT

  ' form init
  colLocMsgs.Add "Basiseinrichtung", MSGID_INIT
      
  colLocMsgs.Add "Highlight-Ordner", MSGID_INIT_BASE_PATH
  colLocMsgs.Add vbCrLf & "Vor der ersten Nutzung des Add-Ins muss der Installationsordner des Paketes Highlight konfiguriert werden.", MSGID_INIT_TEXT_INFO
  colLocMsgs.Add "OK", MSGID_INIT_CMD_OK
  colLocMsgs.Add "Highlight-Ordner ausw�hlen", MSGID_INIT_DLG_HIGHLIGHT_PATH
  
  ' form font selection
  colLocMsgs.Add "Auswahl der Schriftart", MSGID_FNTSEL
  colLocMsgs.Add "Vorschau", MSGID_FNTSEL_FRM_FONT_PREVIEW
  colLocMsgs.Add "Schriftart", MSGID_FNTSEL_LBL_COLUMN_FONT
  colLocMsgs.Add "Feste Breite", MSGID_FNTSEL_LBL_COLUMN_MONOSPACE
  colLocMsgs.Add "OK", MSGID_FNTSEL_CMD_OK
  colLocMsgs.Add "Abbruch", MSGID_FNTSEL_CMD_CANCLEL
  
  ' form status bar
  colLocMsgs.Add "In Bearbeitung ...", MSGID_STATUS
  colLocMsgs.Add "erledigt", MSGID_STATUS_FRM_DONE

  ' logic inside base module
  colLocMsgs.Add "Willkommen! " & vbCrLf & vbCrLf & "Bevor es richtig losgeht, muss noch eine Einstellung vorgenommen werden.", MSGID_BASE_WELCOME
  colLocMsgs.Add "Bitte pr�fen und vervollst�ndigen Sie zun�chst die Einstellungen!", MSGID_BASE_CONF_CHECK
  colLocMsgs.Add "Es darf nur genau ein Textobjekt ausgew�hlt werden!" & vbCrLf & "Aktuell sind es ", MSGID_BASE_ONLY_ONE_TEXT_OBJECT
  colLocMsgs.Add "Es wurde kein Textobjekt ausgew�hlt!", MSGID_BASE_FOUND_NO_TEXT_OBJECT
  colLocMsgs.Add "Das ausgew�hlte Objekt beinhaltet keinen Text!", MSGID_BASE_FOUND_NO_TEXT_IN_OBJECT
  colLocMsgs.Add "Es muss ein Textobjekt ausgew�hlt werden!", MSGID_BASE_FOUND_WRONG_OBJECT_TYPE

  ' file/folder dialogs
  colLocMsgs.Add "Ausw�hlen", MSGID_UTILS_FILE_DLG_CHOOSE
  colLocMsgs.Add "Ausw�hlen", MSGID_UTILS_FOLDER_DLG_CHOOSE

  ' HLConfiguration
  colLocMsgs.Add "Die gefundene Konfigurationsdatei kann nicht gelesen werden." & vbCrLf & vbCrLf & _
                 "Bitte unbedingt die Basiseinstellung pr�fen!", MSGID_CFG_XML_PROCESSING_ERROR
                 
  ' About
  colLocMsgs.Add "Emborado Syntax Highlighter" & vbCrLf & vbCrLf & _
                 ESHBaseModule.ADDIN_VERSION & vbCrLf & vbCrLf & _
                 "� 2022-2024 Claus Grewe <cg@emborado.de>" & vbCrLf & vbCrLf & _
                 "Ver�ffentlicht unter den Bedingungen der GNU GPL Lizenz", MSGID_PREFS_LBL_ABOUT_TEXT
 
End Sub

' set up all english GUI text messages
Sub AddEnglishTexts(colLocMsgs As Collection)

  ' ribbon
  colLocMsgs.Add "Highlighting", MSGID_RIBBON_MENUE_TAB_HEADER
  colLocMsgs.Add "Syntax Highlighting", MSGID_RIBBON_GRP_HIGHLIGHT
  colLocMsgs.Add "Font", MSGID_RIBBON_GRP_FONT
  colLocMsgs.Add "Preferences", MSGID_RIBBON_GRP_PREFS
  colLocMsgs.Add "Development", MSGID_RIBBON_GRP_DEVELOPMENT
      
  ' ribbon buttons
  colLocMsgs.Add "Highlight text block", MSGID_RIBBON_BTN_HIGHLIGHT
  colLocMsgs.Add "Import from clipboard", MSGID_RIBBON_BTN_IMPORT
  colLocMsgs.Add "Preferences", MSGID_RIBBON_BTN_PREFS
    
  ' ribbon dropdown menues
  colLocMsgs.Add "Language", MSGID_RIBBON_DD_LANGUAGE
  colLocMsgs.Add "Style", MSGID_RIBBON_DD_STYLE
  colLocMsgs.Add "Font", MSGID_RIBBON_DD_FONT_TYPE
  colLocMsgs.Add "Background color", MSGID_RIBBON_DD_BGC_STRATEGY
  colLocMsgs.Add "Frame color", MSGID_RIBBON_DD_FRAME_STRATEGY

  ' colors in background dropdown menue
  colLocMsgs.Add "no assignment", MSGID_RIBBON_DDLBL_BGC_NO_ASSIGNMENT
  colLocMsgs.Add "opaque", MSGID_RIBBON_DDLBL_BGC_TRANSPARENT
  colLocMsgs.Add "according to style", MSGID_RIBBON_DDLBL_BGC_LIKE_STYLE
  colLocMsgs.Add "background color 1", MSGID_RIBBON_DDLBL_BGC_COLOR1
  colLocMsgs.Add "background color 2", MSGID_RIBBON_DDLBL_BGC_COLOR2
      
  ' colors in frame dropdown menue
  colLocMsgs.Add "no assignment", MSGID_RIBBON_DDLBL_FC_NO_ASSIGNMENT
  colLocMsgs.Add "opaque", MSGID_RIBBON_DDLBL_FC_TRANSPARENT
  colLocMsgs.Add "frame color 1", MSGID_RIBBON_DDLBL_FC_COLOR1
  colLocMsgs.Add "frame color 2", MSGID_RIBBON_DDLBL_FC_COLOR2
      
  ' fonts in font dropdown menue
  colLocMsgs.Add "no assignment", MSGID_RIBBON_DDLBL_FONT_NO_ASSIGNMENT
  
  ' form prefs
  colLocMsgs.Add "Preferences", MSGID_PREFS
  colLocMsgs.Add "General", MSGID_PREFS_PG_GENERAL
  colLocMsgs.Add "Languages", MSGID_PREFS_PG_LANGUAGE
  colLocMsgs.Add "Styles", MSGID_PREFS_PG_STYLE
  colLocMsgs.Add "Fonts", MSGID_PREFS_PG_FONT
  colLocMsgs.Add "Extended", MSGID_PREFS_PG_EXTENDED
  colLocMsgs.Add "About", MSGID_PREFS_PG_ABOUT
     
  colLocMsgs.Add "Highlight", MSGID_PREFS_FRM_PROCESSOR_PATH
  colLocMsgs.Add "Background colors", MSGID_PREFS_FRM_BG_COLOR
  colLocMsgs.Add "Frame colors", MSGID_PREFS_FRM_FRAME_COLOR
      
  colLocMsgs.Add "Installation path", MSGID_PREFS_LBL_PROCESSOR_PATH
  colLocMsgs.Add "Configuration file", MSGID_PREFS_LBL_XML_CONFIG_FILE
  colLocMsgs.Add "Color", MSGID_PREFS_LBL_COLOR
      
  colLocMsgs.Add "Supported languages", MSGID_PREFS_LBL_SUPPORTED_LANGS
  colLocMsgs.Add "Selected languages", MSGID_PREFS_LBL_CHOSEN_LANGS
  colLocMsgs.Add "Choose those languages, you want to select from in the menu.", MSGID_PREFS_LBL_DESCRIPTION_LANGS
  
  colLocMsgs.Add "Supported Styles", MSGID_PREFS_LBL_SUPPORTED_STYLES
  colLocMsgs.Add "Selected Styles", MSGID_PREFS_LBL_CHOSEN_STYLES
  colLocMsgs.Add "Choose those styles, you want to select from in the menu.", MSGID_PREFS_LBL_DESCRIPTION_STYLES
  
  colLocMsgs.Add "Close", MSGID_PREFS_CMD_CLOSE
    
  colLocMsgs.Add "Select the Highlight installation path", MSGID_PREFS_DLG_LOAD_PROCESSOR_PATH
  colLocMsgs.Add "Select XML configuration file", MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE
  colLocMsgs.Add "XML files", MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE_TYPE

  colLocMsgs.Add "External XML configuration file", MSGID_PREFS_FRM_EXT_HIGHLIGHT_CONFIG
  colLocMsgs.Add "XML configuration file", MSGID_PREFS_LBL_EXT_HIGHLIGHT_CONFIG
  colLocMsgs.Add "Use of external configuration for languages and styles", MSGID_PREFS_CB_USE_EXT_HIGHLIGHT_CONFIG

  colLocMsgs.Add "Additional Fonts", MSGID_PREFS_FRM_ADDITIONAL_FONTS
  colLocMsgs.Add "Font", MSGID_PREFS_LBL_USER_DEFINED_FONT
  colLocMsgs.Add "Choose additional fonts, you want to select from menu.", MSGID_PREFS_LBL_DESCRIPTION_FONTS
  colLocMsgs.Add "Hint: Searching for system fonts takes a while ...", MSGID_PREFS_LBL_FONT_SELECTOR_HINT

  ' form init
  colLocMsgs.Add "Initial configuration", MSGID_INIT
      
  colLocMsgs.Add "Highlight folder", MSGID_INIT_BASE_PATH
  colLocMsgs.Add vbCrLf & "First of all the installation folder of the Highlight application must be configured.", MSGID_INIT_TEXT_INFO
  colLocMsgs.Add "OK", MSGID_INIT_CMD_OK
  colLocMsgs.Add "Select Highlight folder", MSGID_INIT_DLG_HIGHLIGHT_PATH
  
  ' form font selection
  colLocMsgs.Add "Font selection", MSGID_FNTSEL
  colLocMsgs.Add "Preview", MSGID_FNTSEL_FRM_FONT_PREVIEW
  colLocMsgs.Add "Font", MSGID_FNTSEL_LBL_COLUMN_FONT
  colLocMsgs.Add "Monospace", MSGID_FNTSEL_LBL_COLUMN_MONOSPACE
  colLocMsgs.Add "OK", MSGID_FNTSEL_CMD_OK
  colLocMsgs.Add "Cancel", MSGID_FNTSEL_CMD_CANCLEL
  
  ' form status bar
  colLocMsgs.Add "Running ...", MSGID_STATUS
  colLocMsgs.Add "finished", MSGID_STATUS_FRM_DONE

  ' logic inside base module
  colLocMsgs.Add "Welcome! " & vbCrLf & vbCrLf & "Before we can get started, there's a setting that needs to be made.", MSGID_BASE_WELCOME
  colLocMsgs.Add "Please check and fill in the needed configuration!", MSGID_BASE_CONF_CHECK
  colLocMsgs.Add "You are allowed to select only one text object!" & vbCrLf & "Currently selected objects: ", MSGID_BASE_ONLY_ONE_TEXT_OBJECT
  colLocMsgs.Add "No text object was selected!", MSGID_BASE_FOUND_NO_TEXT_OBJECT
  colLocMsgs.Add "The selected object contains no text!", MSGID_BASE_FOUND_NO_TEXT_IN_OBJECT
  colLocMsgs.Add "You must choose a text object!", MSGID_BASE_FOUND_WRONG_OBJECT_TYPE

  ' file/folder dialogs
  colLocMsgs.Add "Choose", MSGID_UTILS_FILE_DLG_CHOOSE
  colLocMsgs.Add "Choose", MSGID_UTILS_FOLDER_DLG_CHOOSE
  
  ' HLConfiguration
  colLocMsgs.Add "the found configuration file could not be read." & vbCrLf & vbCrLf & _
                "Please check the highlight base path!", MSGID_CFG_XML_PROCESSING_ERROR
                
    ' About
  colLocMsgs.Add "Emborado Syntax Highlighter" & vbCrLf & vbCrLf & _
                 ESHBaseModule.ADDIN_VERSION & vbCrLf & vbCrLf & _
                 "� 2022-2024 Claus Grewe <cg@emborado.de>" & vbCrLf & vbCrLf & _
                 "released under the terms of GNU GPL license", MSGID_PREFS_LBL_ABOUT_TEXT
                 
End Sub

' =======================================
' T E S T
' =======================================
Sub Test()
  Debug.Print "translated: " & ESHLocalization.GetMsg(MSGID_PREFS_LBL_COLOR)
  Debug.Print "translated: " & ESHLocalization.GetMsg("does not exst")
  
End Sub


