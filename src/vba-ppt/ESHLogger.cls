VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Logger implementation
' ----------------------------------------------------------------------------
Option Explicit

' Member variable
Private m_level As logLevel

'
' == Ctor ==
'
Private Sub Class_Initialize()
  m_level = lvlDebug
End Sub

'
' == Properties ==
'
Property Get level() As logLevel
  level = m_level
End Property

Property Let level(value As logLevel)
  m_level = value
End Property

'
' log
'
' IN:
'   sMessage   msg
'   eLogLevel  log level
'   sLogPoint  point of log in code
'
Sub log(sMessage As String, eLogLevel As logLevel, Optional sLogPoint As String)
  
  If eLogLevel >= m_level Then
    Debug.Print CStr(Now()) & LogLevelToString(eLogLevel) & LogPointToString(sLogPoint) & sMessage
  End If
  
End Sub

' =======================================
' I N T E R N A L
' =======================================

Private Function LogPointToString(Optional sLogPoint As String = "") As String

  If Len(sLogPoint) > 0 Then
    LogPointToString = sLogPoint & "|"
  Else
    LogPointToString = "-|"
  End If

End Function

Private Function LogLevelToString(eLogLevel As logLevel) As String

  Select Case eLogLevel
    Case logLevel.lvlInfo
      LogLevelToString = "|INFO |"
    Case logLevel.lvlWarn
      LogLevelToString = "|WARN |"
    Case logLevel.lvlError
      LogLevelToString = "|ERROR|"
    Case logLevel.lvlTrace
      LogLevelToString = "|TRACE|"
    Case logLevel.lvlDebug
      LogLevelToString = "|DEBUG|"
    Case Else
      LogLevelToString = "|     |"
  End Select

End Function


