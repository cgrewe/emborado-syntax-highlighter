Attribute VB_Name = "ESHHighlightData"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Highlight Data:
' contains the a static copy of the available languages and styles
' and the logic to read the values from a XML file (not used currently)
' ----------------------------------------------------------------------------
Option Explicit

' -------------------------------------------
' adds a static list of languages and styles
' to internal collection
' -------------------------------------------
Public Sub AddHighlightDataStatic(ByRef colLanguages As Collection, ByRef colStyles As Collection)
  AddHighlightLanguages colLanguages
  AddHighlightStyles colStyles
End Sub

' -------------------------------------------
' adds a list of languages and styles
' from external XML file to internal collection
' -------------------------------------------
Public Sub AddHighlightDataXML(ByRef colLanguages As Collection, ByRef colStyles As Collection)
  
  ' is XML file existing?
  If ESHUtils.FileExists(ESHPrefs.GetXmlConfigFile) = False Then
    ESHLogging.logError "there is no XML configuration file found", "AddHighlightDataXML"
    Exit Sub
  End If
  
  ' read in XML configuration file
  Dim oXml As MSXML2.DOMDocument60
  Set oXml = New MSXML2.DOMDocument60
    
  oXml.Load ESHPrefs.GetXmlConfigFile
  
  ' check for possible errors
  If oXml.parseError.ErrorCode <> 0 Then
    ESHLogging.logError "can't load config file: " & oXml.parseError, "AddHighlightDataXML"
    MsgBox ESHLocalization.GetMsg(MSGID_CFG_XML_PROCESSING_ERROR), _
           vbCritical, ESHBaseModule.ADDIN_NAME
    Exit Sub
  End If
   
  Dim oSeqNodes, oSeqNode As IXMLDOMNode
    
  ' ----------------------------------------------
  ' look for language entries in XML configuration
  ' ----------------------------------------------
  
  ' look for language entries
  Set oSeqNodes = oXml.SelectNodes("//languages/language")

  If oSeqNodes.Length = 0 Then
    ESHLogging.logError "no language configuration found", "AddHighlightDataXML"
    Exit Sub
  Else
    ' iterate through found languages
    For Each oSeqNode In oSeqNodes
      With oSeqNode.Attributes
        AddLanguage colLanguages, .getNamedItem("name").Text, _
                                  .getNamedItem("id").Text, _
                                  .getNamedItem("parameter").Text
         ESHLogging.logTrace "read " & .getNamedItem("name").Text, "AddHighlightDataXML"
      End With
    Next
  End If
  
  ' ----------------------------------------------
  ' look for style entries in XML configuration
  ' ----------------------------------------------
  ' look for language entries
  Set oSeqNodes = oXml.SelectNodes("//styles/style")

  If oSeqNodes.Length = 0 Then
    ESHLogging.logError "no style configuration found", "AddHighlightDataXML"
    Exit Sub
  Else
    ' iterate through found styles
    For Each oSeqNode In oSeqNodes
      With oSeqNode.Attributes
        AddStyle colStyles, .getNamedItem("name").Text, _
                            .getNamedItem("id").Text, _
                            .getNamedItem("parameter").Text, _
                            .getNamedItem("background-color").Text
                            
        ESHLogging.logTrace "read " & .getNamedItem("name").Text, "AddHighlightDataXML"
      End With
    Next
  End If
End Sub

Private Sub AddLanguage(colLanguages As Collection, name As String, id As String, parameter As String)
  Dim oLangDef As ESHLanguageDefinition
  Set oLangDef = New ESHLanguageDefinition
  
  oLangDef.InitiateProperties name, id, parameter
  colLanguages.Add oLangDef, oLangDef.id
End Sub

Private Sub AddStyle(colStyles As Collection, name As String, id As String, parameter As String, bgColorHex As String)

  Dim oStyleDef As ESHStyleDefinition
  Set oStyleDef = New ESHStyleDefinition
  
  oStyleDef.InitiateProperties name, id, parameter, bgColorHex
  colStyles.Add oStyleDef, oStyleDef.id
  
End Sub

' static list of supported languages
Private Sub AddHighlightLanguages(ByRef colLanguages As Collection)

  AddLanguage colLanguages, "ABAP/4", "lang-abap", "abap"
  AddLanguage colLanguages, "ABC", "lang-abc", "abc"
  AddLanguage colLanguages, "Advanced Backus-Naur Form", "lang-abnf", "abnf"
  AddLanguage colLanguages, "ActionScript", "lang-actionscript", "actionscript"
  AddLanguage colLanguages, "ADA95", "lang-ada", "ada"
  AddLanguage colLanguages, "Agda", "lang-agda", "agda"
  AddLanguage colLanguages, "ALAN Interactive Fiction Language", "lang-alan", "alan"
  AddLanguage colLanguages, "ALGOL 68", "lang-algol", "algol"
  AddLanguage colLanguages, "AMPL", "lang-ampl", "ampl"
  AddLanguage colLanguages, "AMTrix", "lang-amtrix", "amtrix"
  AddLanguage colLanguages, "AppleScript", "lang-applescript", "applescript"
  AddLanguage colLanguages, "Arc", "lang-arc", "arc"
  AddLanguage colLanguages, "ARM", "lang-arm", "arm"
  AddLanguage colLanguages, "AS/400 CL", "lang-as400cl", "as400cl"
  AddLanguage colLanguages, "ASCEND", "lang-ascend", "ascend"
  AddLanguage colLanguages, "AsciiDoc", "lang-asciidoc", "asciidoc"
  AddLanguage colLanguages, "Active Server Pages", "lang-asp", "asp"
  AddLanguage colLanguages, "Abstract", "lang-aspect", "aspect"
  AddLanguage colLanguages, "Generic Assembler", "lang-assembler", "assembler"
  AddLanguage colLanguages, "Applied Type System", "lang-ats", "ats"
  AddLanguage colLanguages, "AutoHotKey", "lang-autohotkey", "autohotkey"
  AddLanguage colLanguages, "AutoIt", "lang-autoit", "autoit"
  AddLanguage colLanguages, "Avenue", "lang-avenue", "avenue"
  AddLanguage colLanguages, "(G)AWK", "lang-awk", "awk"
  AddLanguage colLanguages, "Ballerina", "lang-ballerina", "ballerina"
  AddLanguage colLanguages, "MS DOS Batch", "lang-bat", "bat"
  AddLanguage colLanguages, "BBcode", "lang-bbcode", "bbcode"
  AddLanguage colLanguages, "BCPL", "lang-bcpl", "bcpl"
  AddLanguage colLanguages, "BibTeX", "lang-bibtex", "bibtex"
  AddLanguage colLanguages, "Biferno", "lang-biferno", "biferno"
  AddLanguage colLanguages, "Bison", "lang-bison", "bison"
  AddLanguage colLanguages, "Blitz Basic", "lang-blitzbasic", "blitzbasic"
  AddLanguage colLanguages, "BM Script", "lang-bms", "bms"
  AddLanguage colLanguages, "Backus-Naur Form", "lang-bnf", "bnf"
  AddLanguage colLanguages, "Boo", "lang-boo", "boo"
  AddLanguage colLanguages, "C and C++", "lang-c", "c"
  AddLanguage colLanguages, "Carbon", "lang-carbon", "carbon"
  AddLanguage colLanguages, "Ceylon", "lang-ceylon", "ceylon"
  AddLanguage colLanguages, "Charmm", "lang-charmm", "charmm"
  AddLanguage colLanguages, "CHILL", "lang-chill", "chill"
  AddLanguage colLanguages, "Chapel", "lang-chpl", "chpl"
  AddLanguage colLanguages, "Clean", "lang-clean", "clean"
  AddLanguage colLanguages, "ClearBasic", "lang-clearbasic", "clearbasic"
  AddLanguage colLanguages, "Clipper", "lang-clipper", "clipper"
  AddLanguage colLanguages, "Clojure", "lang-clojure", "clojure"
  AddLanguage colLanguages, "Clips", "lang-clp", "clp"
  AddLanguage colLanguages, "CMake", "lang-cmake", "cmake"
  AddLanguage colLanguages, "COBOL", "lang-cobol", "cobol"
  AddLanguage colLanguages, "Coffeescript", "lang-coffeescript", "coffeescript"
  AddLanguage colLanguages, "ColdFusion MX", "lang-coldfusion", "coldfusion"
  AddLanguage colLanguages, "Generic config files", "lang-conf", "conf"
  AddLanguage colLanguages, "Cpp2", "lang-cpp2", "cpp2"
  AddLanguage colLanguages, "CriticMarkup", "lang-critic", "critic"
  AddLanguage colLanguages, "Crack", "lang-crk", "crk"
  AddLanguage colLanguages, "Crystal", "lang-crystal", "crystal"
  AddLanguage colLanguages, "C#", "lang-csharp", "csharp"
  AddLanguage colLanguages, "CSS", "lang-css", "css"
  AddLanguage colLanguages, "Coffeescript Block Regex", "lang-cs_block_regex", "cs_block_regex"
  AddLanguage colLanguages, "D", "lang-d", "d"
  AddLanguage colLanguages, "Dart", "lang-dart", "dart"
  AddLanguage colLanguages, "delphi", "lang-delphi", "delphi"
  AddLanguage colLanguages, "Diff", "lang-diff", "diff"
  AddLanguage colLanguages, "Dockerfile", "lang-dockerfile", "dockerfile"
  AddLanguage colLanguages, "Device Tree Source", "lang-dts", "dts"
  AddLanguage colLanguages, "Dylan", "lang-dylan", "dylan"
  AddLanguage colLanguages, "Extended Backus-Naur Form", "lang-ebnf", "ebnf"
  AddLanguage colLanguages, "EBNF2", "lang-ebnf2", "ebnf2"
  AddLanguage colLanguages, "Eiffel", "lang-eiffel", "eiffel"
  AddLanguage colLanguages, "Elixir", "lang-elixir", "elixir"
  AddLanguage colLanguages, "Elm", "lang-elm", "elm"
  AddLanguage colLanguages, "E-Mail treated as Markup", "lang-email", "email"
  AddLanguage colLanguages, "ERB Templates", "lang-erb", "erb"
  AddLanguage colLanguages, "Erlang", "lang-erlang", "erlang"
  AddLanguage colLanguages, "Euphoria", "lang-euphoria", "euphoria"
  AddLanguage colLanguages, "EXAPUNKS", "lang-exapunks", "exapunks"
  AddLanguage colLanguages, "Excel Formulas", "lang-excel", "excel"
  AddLanguage colLanguages, "Express", "lang-express", "express"
  AddLanguage colLanguages, "Factor", "lang-factor", "factor"
  AddLanguage colLanguages, "FAME", "lang-fame", "fame"
  AddLanguage colLanguages, "fasm", "lang-fasm", "fasm"
  AddLanguage colLanguages, "Felix", "lang-felix", "felix"
  AddLanguage colLanguages, "Fish", "lang-fish", "fish"
  AddLanguage colLanguages, "Fortran 77", "lang-fortran77", "fortran77"
  AddLanguage colLanguages, "Fortran 90", "lang-fortran90", "fortran90"
  AddLanguage colLanguages, "Frink", "lang-frink", "frink"
  AddLanguage colLanguages, "F#", "lang-fsharp", "fsharp"
  AddLanguage colLanguages, "fstab config file", "lang-fstab", "fstab"
  AddLanguage colLanguages, "Java FX", "lang-fx", "fx"
  AddLanguage colLanguages, "Gambas", "lang-gambas", "gambas"
  AddLanguage colLanguages, "gdb", "lang-gdb", "gdb"
  AddLanguage colLanguages, "GDScript", "lang-gdscript", "gdscript"
  AddLanguage colLanguages, "Go", "lang-go", "go"
  AddLanguage colLanguages, "Graphviz", "lang-graphviz", "graphviz"
  AddLanguage colLanguages, "Haml (HTML Abstraction Markup Language)", "lang-haml", "haml"
  AddLanguage colLanguages, "Hare", "lang-hare", "hare"
  AddLanguage colLanguages, "Haskell", "lang-haskell", "haskell"
  AddLanguage colLanguages, "haXe", "lang-haxe", "haxe"
  AddLanguage colLanguages, "Hecl", "lang-hcl", "hcl"
  AddLanguage colLanguages, "HTML", "lang-html", "html"
  AddLanguage colLanguages, "Apache Config", "lang-httpd", "httpd"
  AddLanguage colLanguages, "Hugo", "lang-hugo", "hugo"
  AddLanguage colLanguages, "Icon", "lang-icon", "icon"
  AddLanguage colLanguages, "IDL", "lang-idl", "idl"
  AddLanguage colLanguages, "Interactive Data Language", "lang-idlang", "idlang"
  AddLanguage colLanguages, "Lua (for LuaTeX)", "lang-inc_luatex", "inc_luatex"
  AddLanguage colLanguages, "Informix", "lang-informix", "informix"
  AddLanguage colLanguages, "INI", "lang-ini", "ini"
  AddLanguage colLanguages, "Inno Setup", "lang-innosetup", "innosetup"
  AddLanguage colLanguages, "INTERLIS", "lang-interlis", "interlis"
  AddLanguage colLanguages, "IO", "lang-io", "io"
  AddLanguage colLanguages, "Jam", "lang-jam", "jam"
  AddLanguage colLanguages, "Jasmin", "lang-jasmin", "jasmin"
  AddLanguage colLanguages, "Java", "lang-java", "java"
  AddLanguage colLanguages, "Javascript", "lang-javascript", "javascript"
  AddLanguage colLanguages, "JSON", "lang-json", "json"
  AddLanguage colLanguages, "JavaServer Pages", "lang-jsp", "jsp"
  AddLanguage colLanguages, "JSX", "lang-jsx", "jsx"
  AddLanguage colLanguages, "Javascript Regex", "lang-js_regex", "js_regex"
  AddLanguage colLanguages, "Julia", "lang-julia", "julia"
  AddLanguage colLanguages, "Kotlin", "lang-kotlin", "kotlin"
  AddLanguage colLanguages, "LDAP", "lang-ldif", "ldif"
  AddLanguage colLanguages, "Less", "lang-less", "less"
  AddLanguage colLanguages, "Haskell LHS", "lang-lhs", "lhs"
  AddLanguage colLanguages, "Lilypond", "lang-lilypond", "lilypond"
  AddLanguage colLanguages, "Limbo", "lang-limbo", "limbo"
  AddLanguage colLanguages, "Linden Script", "lang-lindenscript", "lindenscript"
  AddLanguage colLanguages, "Lisp", "lang-lisp", "lisp"
  AddLanguage colLanguages, "Logtalk", "lang-logtalk", "logtalk"
  AddLanguage colLanguages, "Lotos", "lang-lotos", "lotos"
  AddLanguage colLanguages, "Lotus", "lang-lotus", "lotus"
  AddLanguage colLanguages, "Lua", "lang-lua", "lua"
  AddLanguage colLanguages, "Luban", "lang-luban", "luban"
  AddLanguage colLanguages, "Make", "lang-makefile", "makefile"
  AddLanguage colLanguages, "Maple", "lang-maple", "maple"
  AddLanguage colLanguages, "GitHub Flavored Markdown", "lang-markdown", "markdown"
  AddLanguage colLanguages, "Matlab", "lang-matlab", "matlab"
  AddLanguage colLanguages, "Maya", "lang-maya", "maya"
  AddLanguage colLanguages, "Mercury", "lang-mercury", "mercury"
  AddLanguage colLanguages, "Meson", "lang-meson", "meson"
  AddLanguage colLanguages, "Miranda", "lang-miranda", "miranda"
  AddLanguage colLanguages, "Modula2", "lang-mod2", "mod2"
  AddLanguage colLanguages, "Modula3", "lang-mod3", "mod3"
  AddLanguage colLanguages, "Modelica", "lang-modelica", "modelica"
  AddLanguage colLanguages, "MoonScript", "lang-moon", "moon"
  AddLanguage colLanguages, "MaxScript", "lang-ms", "ms"
  AddLanguage colLanguages, "mIRC Scripting", "lang-msl", "msl"
  AddLanguage colLanguages, "MSSQL", "lang-mssql", "mssql"
  AddLanguage colLanguages, "Magic eXtensible Markup", "lang-mxml", "mxml"
  AddLanguage colLanguages, "Notation3 (N3), N-Triples, Turtle, SPARQL", "lang-n3", "n3"
  AddLanguage colLanguages, "Nasal", "lang-nasal", "nasal"
  AddLanguage colLanguages, "NeXT Byte Codes", "lang-nbc", "nbc"
  AddLanguage colLanguages, "Nemerle", "lang-nemerle", "nemerle"
  AddLanguage colLanguages, "NetRexx", "lang-netrexx", "netrexx"
  AddLanguage colLanguages, "Nginx configuration", "lang-nginx", "nginx"
  AddLanguage colLanguages, "Nice", "lang-nice", "nice"
  AddLanguage colLanguages, "Nim", "lang-nim", "nim"
  AddLanguage colLanguages, "Nix Expression Language", "lang-nix", "nix"
  AddLanguage colLanguages, "NSIS", "lang-nsis", "nsis"
  AddLanguage colLanguages, "Not eXactly C", "lang-nxc", "nxc"
  AddLanguage colLanguages, "Oberon", "lang-oberon", "oberon"
  AddLanguage colLanguages, "Objective C", "lang-objc", "objc"
  AddLanguage colLanguages, "Objective Caml", "lang-ocaml", "ocaml"
  AddLanguage colLanguages, "Octave", "lang-octave", "octave"
  AddLanguage colLanguages, "OpenObjectRexx", "lang-oorexx", "oorexx"
  AddLanguage colLanguages, "Emacs Org-Mode", "lang-org", "org"
  AddLanguage colLanguages, "Object Script", "lang-os", "os"
  AddLanguage colLanguages, "Oz", "lang-oz", "oz"
  AddLanguage colLanguages, "Paradox", "lang-paradox", "paradox"
  AddLanguage colLanguages, "Pascal", "lang-pas", "pas"
  AddLanguage colLanguages, "Portable Document Format", "lang-pdf", "pdf"
  AddLanguage colLanguages, "Perl", "lang-perl", "perl"
  AddLanguage colLanguages, "PHP", "lang-php", "php"
  AddLanguage colLanguages, "Pike", "lang-pike", "pike"
  AddLanguage colLanguages, "PL/1", "lang-pl1", "pl1"
  AddLanguage colLanguages, "PL/Perl", "lang-plperl", "plperl"
  AddLanguage colLanguages, "PL/Python", "lang-plpython", "plpython"
  AddLanguage colLanguages, "PL/Tcl", "lang-pltcl", "pltcl"
  AddLanguage colLanguages, "PO translation", "lang-po", "po"
  AddLanguage colLanguages, "Polygen", "lang-polygen", "polygen"
  AddLanguage colLanguages, "Pony", "lang-pony", "pony"
  AddLanguage colLanguages, "POV-Ray", "lang-pov", "pov"
  AddLanguage colLanguages, "Microsoft PowerShell", "lang-powershell", "powershell"
  AddLanguage colLanguages, "Prolog", "lang-pro", "pro"
  AddLanguage colLanguages, "Progress", "lang-progress", "progress"
  AddLanguage colLanguages, "PostScript", "lang-ps", "ps"
  AddLanguage colLanguages, "PATROL", "lang-psl", "psl"
  AddLanguage colLanguages, "Pure", "lang-pure", "pure"
  AddLanguage colLanguages, "PureBASIC", "lang-purebasic", "purebasic"
  AddLanguage colLanguages, "PureScript", "lang-purescript", "purescript"
  AddLanguage colLanguages, "Pyrex", "lang-pyrex", "pyrex"
  AddLanguage colLanguages, "Python", "lang-python", "python"
  AddLanguage colLanguages, "Qore", "lang-q", "q"
  AddLanguage colLanguages, "QMake Project", "lang-qmake", "qmake"
  AddLanguage colLanguages, "QML", "lang-qml", "qml"
  AddLanguage colLanguages, "Qu", "lang-qu", "qu"
  AddLanguage colLanguages, "R", "lang-r", "r"
  AddLanguage colLanguages, "Rebol", "lang-rebol", "rebol"
  AddLanguage colLanguages, "Rego", "lang-rego", "rego"
  AddLanguage colLanguages, "Rexx", "lang-rexx", "rexx"
  AddLanguage colLanguages, "Relax NG", "lang-rnc", "rnc"
  AddLanguage colLanguages, "RPG", "lang-rpg", "rpg"
  AddLanguage colLanguages, "RPL Programming Language", "lang-rpl", "rpl"
  AddLanguage colLanguages, "reStructured Text", "lang-rst", "rst"
  AddLanguage colLanguages, "Ruby", "lang-ruby", "ruby"
  AddLanguage colLanguages, "Rust", "lang-rust", "rust"
  AddLanguage colLanguages, "PowerPC Assembler", "lang-s", "s"
  AddLanguage colLanguages, "Sequence Alignment Map (use with sam_seq.lua plug-in)", "lang-sam", "sam"
  AddLanguage colLanguages, "SAS", "lang-sas", "sas"
  AddLanguage colLanguages, "OpenSCAD", "lang-scad", "scad"
  AddLanguage colLanguages, "Scala", "lang-scala", "scala"
  AddLanguage colLanguages, "Scilab", "lang-scilab", "scilab"
  AddLanguage colLanguages, "Sass/SCSS", "lang-scss", "scss"
  AddLanguage colLanguages, "Bash", "lang-shellscript", "shellscript"
  AddLanguage colLanguages, "Slim (experimental)", "lang-slim", "slim"
  AddLanguage colLanguages, "SMALL", "lang-small", "small"
  AddLanguage colLanguages, "Smalltalk", "lang-smalltalk", "smalltalk"
  AddLanguage colLanguages, "Standard ML", "lang-sml", "sml"
  AddLanguage colLanguages, "SNMP", "lang-snmp", "snmp"
  AddLanguage colLanguages, "SNOBOL", "lang-snobol", "snobol"
  AddLanguage colLanguages, "Solidity", "lang-solidity", "solidity"
  AddLanguage colLanguages, "RPM Spec", "lang-spec", "spec"
  AddLanguage colLanguages, "SPIN SQL", "lang-spn", "spn"
  AddLanguage colLanguages, "PL/SQL", "lang-sql", "sql"
  AddLanguage colLanguages, "Squirrel", "lang-squirrel", "squirrel"
  AddLanguage colLanguages, "Stylus", "lang-styl", "styl"
  AddLanguage colLanguages, "SVG", "lang-svg", "svg"
  AddLanguage colLanguages, "Swift", "lang-swift", "swift"
  AddLanguage colLanguages, "Sybase SQL", "lang-sybase", "sybase"
  AddLanguage colLanguages, "Tcl/Tk", "lang-tcl", "tcl"
  AddLanguage colLanguages, "TCSH", "lang-tcsh", "tcsh"
  AddLanguage colLanguages, "Terraform", "lang-terraform", "terraform"
  AddLanguage colLanguages, "TeX and LaTeX", "lang-tex", "tex"
  AddLanguage colLanguages, "TOML", "lang-toml", "toml"
  AddLanguage colLanguages, "Transact-SQL", "lang-tsql", "tsql"
  AddLanguage colLanguages, "TSX (TypeScript with React)", "lang-tsx", "tsx"
  AddLanguage colLanguages, "TTCN3", "lang-ttcn3", "ttcn3"
  AddLanguage colLanguages, "Plain text", "lang-txt", "txt"
  AddLanguage colLanguages, "TypeScript", "lang-typescript", "typescript"
  AddLanguage colLanguages, "UPC (and C, technically)", "lang-upc", "upc"
  AddLanguage colLanguages, "Vala", "lang-vala", "vala"
  AddLanguage colLanguages, "Visual Basic", "lang-vb", "vb"
  AddLanguage colLanguages, "Verilog", "lang-verilog", "verilog"
  AddLanguage colLanguages, "VHDL", "lang-vhd", "vhd"
  AddLanguage colLanguages, "vimscript", "lang-vimscript", "vimscript"
  AddLanguage colLanguages, "vue.js (beta)", "lang-vue", "vue"
  AddLanguage colLanguages, "Web Assembly Text", "lang-wat", "wat"
  AddLanguage colLanguages, "Whiley", "lang-whiley", "whiley"
  AddLanguage colLanguages, "Wren", "lang-wren", "wren"
  AddLanguage colLanguages, "XML", "lang-xml", "xml"
  AddLanguage colLanguages, "SuperX++", "lang-xpp", "xpp"
  AddLanguage colLanguages, "Yaiff", "lang-yaiff", "yaiff"
  AddLanguage colLanguages, "Generic YAML", "lang-yaml", "yaml"
  AddLanguage colLanguages, "Ansible YAML", "lang-yaml_ansible", "yaml_ansible"
  AddLanguage colLanguages, "Yang", "lang-yang", "yang"
  AddLanguage colLanguages, "Zig", "lang-zig", "zig"
  AddLanguage colLanguages, "Zonnon", "lang-znn", "znn"

End Sub

' static list of supported stlyes
Private Sub AddHighlightStyles(ByRef colStyles As Collection)

  AddStyle colStyles, "Acid", "style-acid", "acid", "eeeeee"
  AddStyle colStyles, "vim aiseered", "style-aiseered", "aiseered", "400000"
  AddStyle colStyles, "Andes", "style-andes", "andes", "0f0f0f"
  AddStyle colStyles, "vim anotherdark", "style-anotherdark", "anotherdark", "323232"
  AddStyle colStyles, "vim autumn", "style-autumn", "autumn", "fff4e8"
  AddStyle colStyles, "vim baycomb", "style-baycomb", "baycomb", "11121a"
  AddStyle colStyles, "vim bclear", "style-bclear", "bclear", "ffffff"
  AddStyle colStyles, "vim biogoo", "style-biogoo", "biogoo", "d6d6d6"
  AddStyle colStyles, "Bipolar", "style-bipolar", "bipolar", "0f020a"
  AddStyle colStyles, "Black And Blue", "style-blacknblue", "blacknblue", "000000"
  AddStyle colStyles, "vim blue and green", "style-bluegreen", "bluegreen", "061A3E"
  AddStyle colStyles, "vim breeze", "style-breeze", "breeze", "005c70"
  AddStyle colStyles, "Bright", "style-bright", "bright", "ffffff"
  AddStyle colStyles, "vim camo", "style-camo", "camo", "232323"
  AddStyle colStyles, "vim candy", "style-candy", "candy", "000000"
  AddStyle colStyles, "vim clarity", "style-clarity", "clarity", "1F3055"
  AddStyle colStyles, "vim dante", "style-dante", "dante", "000000"
  AddStyle colStyles, "Dark Blue", "style-darkblue", "darkblue", "000040"
  AddStyle colStyles, "vim dark bone", "style-darkbone", "darkbone", "000000"
  AddStyle colStyles, "Darkness", "style-darkness", "darkness", "000000"
  AddStyle colStyles, "vscode darkplus", "style-darkplus", "darkplus", "1e1e1e"
  AddStyle colStyles, "vim darkslategray", "style-darkslategray", "darkslategray", "1d3e3e"
  AddStyle colStyles, "vim darkspectrum", "style-darkspectrum", "darkspectrum", "2A2A2A"
  AddStyle colStyles, "vim denim", "style-denim", "denim", "000038"
  AddStyle colStyles, "DuoTone Dark Earth", "style-duotone-dark-earth", "duotone-dark-earth", "2c2826"
  AddStyle colStyles, "DuoTone Dark Forest", "style-duotone-dark-forest", "duotone-dark-forest", "2a2d2a"
  AddStyle colStyles, "DuoTone Dark Sea", "style-duotone-dark-sea", "duotone-dark-sea", "1d262f"
  AddStyle colStyles, "DuoTone Dark Sky", "style-duotone-dark-sky", "duotone-dark-sky", "2c2734"
  AddStyle colStyles, "DuoTone Dark Space", "style-duotone-dark-space", "duotone-dark-space", "24242e"
  AddStyle colStyles, "vim dusk", "style-dusk", "dusk", "1f3048"
  AddStyle colStyles, "vim earendel", "style-earendel", "earendel", "ffffff"
  AddStyle colStyles, "Easter", "style-easter", "easter", "fff9b9"
  AddStyle colStyles, "Anjuta IDE", "style-edit-anjuta", "edit-anjuta", "ffffff"
  AddStyle colStyles, "BBEdit Classic", "style-edit-bbedit", "edit-bbedit", "ffffff"
  AddStyle colStyles, "Eclipse IDE", "style-edit-eclipse", "edit-eclipse", "ffffff"
  AddStyle colStyles, "Emacs Editor", "style-edit-emacs", "edit-emacs", "ffffff"
  AddStyle colStyles, "FASM Editor", "style-edit-fasm", "edit-fasm", "FFFFFF"
  AddStyle colStyles, "FlashDevelop", "style-edit-flashdevelop", "edit-flashdevelop", "ffffff"
  AddStyle colStyles, "Gedit Editor", "style-edit-gedit", "edit-gedit", "ffffff"
  AddStyle colStyles, "Godot Engine", "style-edit-godot", "edit-godot", "222026"
  AddStyle colStyles, "jEdit Editor", "style-edit-jedit", "edit-jedit", "ffffff"
  AddStyle colStyles, "Kwrite Editor", "style-edit-kwrite", "edit-kwrite", "E0EAEE"
  AddStyle colStyles, "Matlab Editor", "style-edit-matlab", "edit-matlab", "ffffff"
  AddStyle colStyles, "Visual Studio IDE", "style-edit-msvs2008", "edit-msvs2008", "ffffff"
  AddStyle colStyles, "Nedit Editor", "style-edit-nedit", "edit-nedit", "ffffff"
  AddStyle colStyles, "PureBASIC", "style-edit-purebasic", "edit-purebasic", "FFFFDF"
  AddStyle colStyles, "Vim Dark Editor", "style-edit-vim-dark", "edit-vim-dark", "000000"
  AddStyle colStyles, "Vim Editor", "style-edit-vim", "edit-vim", "ffffff"
  AddStyle colStyles, "XCode IDE", "style-edit-xcode", "edit-xcode", "ffffff"
  AddStyle colStyles, "vim ekvoli", "style-ekvoli", "ekvoli", "001535"
  AddStyle colStyles, "vim fine_blue", "style-fineblue", "fineblue", "f8f8f8"
  AddStyle colStyles, "vim freya", "style-freya", "freya", "2a2a2a"
  AddStyle colStyles, "vim fruit", "style-fruit", "fruit", "f8f8f8"
  AddStyle colStyles, "Github Source View", "style-github", "github", "ffffff"
  AddStyle colStyles, "Golden", "style-golden", "golden", "000000"
  AddStyle colStyles, "Green LCD", "style-greenlcd", "greenlcd", "003000"
  AddStyle colStyles, "vim kellys", "style-kellys", "kellys", "2a2b2f"
  AddStyle colStyles, "vim leo256", "style-leo", "leo", "000000"
  AddStyle colStyles, "Lucretia", "style-lucretia", "lucretia", "001C42"
  AddStyle colStyles, "vim manxome", "style-manxome", "manxome", "000000"
  AddStyle colStyles, "vim maroloccio", "style-maroloccio", "maroloccio", "1a202a"
  AddStyle colStyles, "vim Matrix", "style-matrix", "matrix", "000000"
  AddStyle colStyles, "Moe", "style-moe", "moe", "f6f6f6"
  AddStyle colStyles, "vim molokai", "style-molokai", "molokai", "272822"
  AddStyle colStyles, "vim moria", "style-moria", "moria", "202020"
  AddStyle colStyles, "vim navajo-night", "style-navajo-night", "navajo-night", "243a4d"
  AddStyle colStyles, "Navy", "style-navy", "navy", "000035"
  AddStyle colStyles, "vim neon", "style-neon", "neon", "303030"
  AddStyle colStyles, "vim night", "style-night", "night", "303040"
  AddStyle colStyles, "vim nightshimmer", "style-nightshimmer", "nightshimmer", "103040"
  AddStyle colStyles, "Nord", "style-nord", "nord", "2e3440"
  AddStyle colStyles, "vim nuvola", "style-nuvola", "nuvola", "F9F5F9"
  AddStyle colStyles, "vim olive", "style-olive", "olive", "333300"
  AddStyle colStyles, "Orion", "style-orion", "orion", "1E1616"
  AddStyle colStyles, "oXygenated rnc", "style-oxygenated", "oxygenated", "0f0f0f"
  AddStyle colStyles, "Pablo", "style-pablo", "pablo", "000000"
  AddStyle colStyles, "vim peaksea", "style-peaksea", "peaksea", "e0e0e0"
  AddStyle colStyles, "Print", "style-print", "print", "ffffff"
  AddStyle colStyles, "Random 01", "style-rand01", "rand01", "ffffff"
  AddStyle colStyles, "vim rdark", "style-rdark", "rdark", "1e2426"
  AddStyle colStyles, "vim relaxedgreen", "style-relaxedgreen", "relaxedgreen", "000000"
  AddStyle colStyles, "vim rootwater", "style-rootwater", "rootwater", "151b1d"
  AddStyle colStyles, "Seashell", "style-seashell", "seashell", "fff5ee"
  AddStyle colStyles, "Solarized Dark", "style-solarized-dark", "solarized-dark", "002b36"
  AddStyle colStyles, "Solarized Light", "style-solarized-light", "solarized-light", "fdf6e3"
  AddStyle colStyles, "Sourceforge.net", "style-sourceforge", "sourceforge", "f8f8f8"
  AddStyle colStyles, "vim tabula", "style-tabula", "tabula", "004A41"
  AddStyle colStyles, "vim TCSoft", "style-tcsoft", "tcsoft", "FFFFFF"
  AddStyle colStyles, "the", "style-the", "the", "ffffff"
  AddStyle colStyles, "Vampire", "style-vampire", "vampire", "000000"
  AddStyle colStyles, "White and Grey", "style-whitengrey", "whitengrey", "ffffff"
  AddStyle colStyles, "vim xoria256", "style-xoria256", "xoria256", "1c1c1c"
  AddStyle colStyles, "Zellner", "style-zellner", "zellner", "ffffff"
  AddStyle colStyles, "vim zenburn", "style-zenburn", "zenburn", "1f1f1f"
  AddStyle colStyles, "vim zmrok", "style-zmrok", "zmrok", "141414"
  AddStyle colStyles, "Base16 3024", "style-base16-3024", "base16\3024", "090300"
  AddStyle colStyles, "Base16 Apathy", "style-base16-apathy", "base16\apathy", "031A16"
  AddStyle colStyles, "Base16 Ashes", "style-base16-ashes", "base16\ashes", "1C2023"
  AddStyle colStyles, "Base16 Atelier Cave Light", "style-base16-atelier-cave-light", "base16\atelier-cave-light", "efecf4"
  AddStyle colStyles, "Base16 Atelier Cave", "style-base16-atelier-cave", "base16\atelier-cave", "19171c"
  AddStyle colStyles, "Base16 Atelier Dune Light", "style-base16-atelier-dune-light", "base16\atelier-dune-light", "fefbec"
  AddStyle colStyles, "Base16 Atelier Dune", "style-base16-atelier-dune", "base16\atelier-dune", "20201d"
  AddStyle colStyles, "Base16 Atelier Estuary Light", "style-base16-atelier-estuary-light", "base16\atelier-estuary-light", "f4f3ec"
  AddStyle colStyles, "Base16 Atelier Estuary", "style-base16-atelier-estuary", "base16\atelier-estuary", "22221b"
  AddStyle colStyles, "Base16 Atelier Forest Light", "style-base16-atelier-forest-light", "base16\atelier-forest-light", "f1efee"
  AddStyle colStyles, "Base16 Atelier Forest", "style-base16-atelier-forest", "base16\atelier-forest", "1b1918"
  AddStyle colStyles, "Base16 Atelier Heath Light", "style-base16-atelier-heath-light", "base16\atelier-heath-light", "f7f3f7"
  AddStyle colStyles, "Base16 Atelier Heath", "style-base16-atelier-heath", "base16\atelier-heath", "1b181b"
  AddStyle colStyles, "Base16 Atelier Lakeside Light", "style-base16-atelier-lakeside-light", "base16\atelier-lakeside-light", "ebf8ff"
  AddStyle colStyles, "Base16 Atelier Lakeside", "style-base16-atelier-lakeside", "base16\atelier-lakeside", "161b1d"
  AddStyle colStyles, "Base16 Atelier Plateau Light", "style-base16-atelier-plateau-light", "base16\atelier-plateau-light", "f4ecec"
  AddStyle colStyles, "Base16 Atelier Plateau", "style-base16-atelier-plateau", "base16\atelier-plateau", "1b1818"
  AddStyle colStyles, "Base16 Atelier Savanna Light", "style-base16-atelier-savanna-light", "base16\atelier-savanna-light", "ecf4ee"
  AddStyle colStyles, "Base16 Atelier Savanna", "style-base16-atelier-savanna", "base16\atelier-savanna", "171c19"
  AddStyle colStyles, "Base16 Atelier Seaside Light", "style-base16-atelier-seaside-light", "base16\atelier-seaside-light", "f4fbf4"
  AddStyle colStyles, "Base16 Atelier Seaside", "style-base16-atelier-seaside", "base16\atelier-seaside", "131513"
  AddStyle colStyles, "Base16 Atelier Sulphurpool Light", "style-base16-atelier-sulphurpool-light", "base16\atelier-sulphurpool-light", "f5f7ff"
  AddStyle colStyles, "Base16 Atelier Sulphurpool", "style-base16-atelier-sulphurpool", "base16\atelier-sulphurpool", "202746"
  AddStyle colStyles, "Base16 Bespin", "style-base16-bespin", "base16\bespin", "28211c"
  AddStyle colStyles, "Base16 Brewer", "style-base16-brewer", "base16\brewer", "0c0d0e"
  AddStyle colStyles, "Base16 Bright", "style-base16-bright", "base16\bright", "000000"
  AddStyle colStyles, "Base16 Brush Trees Dark", "style-base16-brushtrees-dark", "base16\brushtrees-dark", "485867"
  AddStyle colStyles, "Base16 Brush Trees", "style-base16-brushtrees", "base16\brushtrees", "E3EFEF"
  AddStyle colStyles, "Base16 Chalk", "style-base16-chalk", "base16\chalk", "151515"
  AddStyle colStyles, "Base16 Circus", "style-base16-circus", "base16\circus", "191919"
  AddStyle colStyles, "Base16 Classic Dark", "style-base16-classic-dark", "base16\classic-dark", "151515"
  AddStyle colStyles, "Base16 Classic Light", "style-base16-classic-light", "base16\classic-light", "F5F5F5"
  AddStyle colStyles, "Base16 Codeschool", "style-base16-codeschool", "base16\codeschool", "232c31"
  AddStyle colStyles, "Base16 Cupcake", "style-base16-cupcake", "base16\cupcake", "fbf1f2"
  AddStyle colStyles, "Base16 Cupertino", "style-base16-cupertino", "base16\cupertino", "5e5e5e"
  AddStyle colStyles, "Base16 Darktooth", "style-base16-darktooth", "base16\darktooth", "1D2021"
  AddStyle colStyles, "Base16 Default Dark", "style-base16-default-dark", "base16\default-dark", "181818"
  AddStyle colStyles, "Base16 Default Light", "style-base16-default-light", "base16\default-light", "f8f8f8"
  AddStyle colStyles, "Base16 Dracula", "style-base16-dracula", "base16\dracula", "282936"
  AddStyle colStyles, "Base16 Eighties", "style-base16-eighties", "base16\eighties", "2d2d2d"
  AddStyle colStyles, "Base16 Embers", "style-base16-embers", "base16\embers", "16130F"
  AddStyle colStyles, "Base16 Flat", "style-base16-flat", "base16\flat", "2C3E50"
  AddStyle colStyles, "Base16 Github", "style-base16-github", "base16\github", "ffffff"
  AddStyle colStyles, "Base16 Google Dark", "style-base16-google-dark", "base16\google-dark", "1d1f21"
  AddStyle colStyles, "Base16 Google Light", "style-base16-google-light", "base16\google-light", "ffffff"
  AddStyle colStyles, "Base16 Grayscale Dark", "style-base16-grayscale-dark", "base16\grayscale-dark", "101010"
  AddStyle colStyles, "Base16 Grayscale Light", "style-base16-grayscale-light", "base16\grayscale-light", "f7f7f7"
  AddStyle colStyles, "Base16 Green Screen", "style-base16-greenscreen", "base16\greenscreen", "001100"
  AddStyle colStyles, "Base16 Gruvbox dark, hard", "style-base16-gruvbox-dark-hard", "base16\gruvbox-dark-hard", "1d2021"
  AddStyle colStyles, "Base16 Gruvbox dark, medium", "style-base16-gruvbox-dark-medium", "base16\gruvbox-dark-medium", "282828"
  AddStyle colStyles, "Base16 Gruvbox dark, pale", "style-base16-gruvbox-dark-pale", "base16\gruvbox-dark-pale", "262626"
  AddStyle colStyles, "Base16 Gruvbox dark, soft", "style-base16-gruvbox-dark-soft", "base16\gruvbox-dark-soft", "32302f"
  AddStyle colStyles, "Base16 Gruvbox light, hard", "style-base16-gruvbox-light-hard", "base16\gruvbox-light-hard", "f9f5d7"
  AddStyle colStyles, "Base16 Gruvbox light, medium", "style-base16-gruvbox-light-medium", "base16\gruvbox-light-medium", "fbf1c7"
  AddStyle colStyles, "Base16 Gruvbox light, soft", "style-base16-gruvbox-light-soft", "base16\gruvbox-light-soft", "f2e5bc"
  AddStyle colStyles, "Base16 Harmonic16 Dark", "style-base16-harmonic-dark", "base16\harmonic-dark", "0b1c2c"
  AddStyle colStyles, "Base16 Harmonic16 Light", "style-base16-harmonic-light", "base16\harmonic-light", "f7f9fb"
  AddStyle colStyles, "Base16 Hopscotch", "style-base16-hopscotch", "base16\hopscotch", "322931"
  AddStyle colStyles, "Base16 iA Dark", "style-base16-ia-dark", "base16\ia-dark", "1a1a1a"
  AddStyle colStyles, "Base16 iA Light", "style-base16-ia-light", "base16\ia-light", "f6f6f6"
  AddStyle colStyles, "Base16 Icy", "style-base16-icy", "base16\icy", "021012"
  AddStyle colStyles, "Base16 IR Black", "style-base16-irblack", "base16\irblack", "000000"
  AddStyle colStyles, "Base16 Isotope", "style-base16-isotope", "base16\isotope", "000000"
  AddStyle colStyles, "Base16 Macintosh", "style-base16-macintosh", "base16\macintosh", "000000"
  AddStyle colStyles, "Base16 Marrakesh", "style-base16-marrakesh", "base16\marrakesh", "201602"
  AddStyle colStyles, "Base16 Materia", "style-base16-materia", "base16\materia", "263238"
  AddStyle colStyles, "Base16 Material Darker", "style-base16-material-darker", "base16\material-darker", "212121"
  AddStyle colStyles, "Base16 Material Lighter", "style-base16-material-lighter", "base16\material-lighter", "FAFAFA"
  AddStyle colStyles, "Base16 Material Palenight", "style-base16-material-palenight", "base16\material-palenight", "292D3E"
  AddStyle colStyles, "Base16 Material Vivid", "style-base16-material-vivid", "base16\material-vivid", "263238"
  AddStyle colStyles, "Base16 Material", "style-base16-material", "base16\material", "263238"
  AddStyle colStyles, "Base16 Mellow Purple", "style-base16-mellow-purple", "base16\mellow-purple", "1e0528"
  AddStyle colStyles, "Base16 Mexico Light", "style-base16-mexico-light", "base16\mexico-light", "f8f8f8"
  AddStyle colStyles, "Base16 Mocha", "style-base16-mocha", "base16\mocha", "3B3228"
  AddStyle colStyles, "Base16 Monokai", "style-base16-monokai", "base16\monokai", "272822"
  AddStyle colStyles, "Base16 Nord", "style-base16-nord", "base16\nord", "2E3440"
  AddStyle colStyles, "Base16 Ocean", "style-base16-ocean", "base16\ocean", "2b303b"
  AddStyle colStyles, "Base16 OceanicNext", "style-base16-oceanicnext", "base16\oceanicnext", "1B2B34"
  AddStyle colStyles, "Base16 One Light", "style-base16-one-light", "base16\one-light", "fafafa"
  AddStyle colStyles, "Base16 OneDark", "style-base16-onedark", "base16\onedark", "282c34"
  AddStyle colStyles, "Base16 Outrun Dark", "style-base16-outrun-dark", "base16\outrun-dark", "00002A"
  AddStyle colStyles, "Base16 Paraiso", "style-base16-paraiso", "base16\paraiso", "2f1e2e"
  AddStyle colStyles, "Base16 PhD", "style-base16-phd", "base16\phd", "061229"
  AddStyle colStyles, "Base16 Pico", "style-base16-pico", "base16\pico", "000000"
  AddStyle colStyles, "Base16 Pop", "style-base16-pop", "base16\pop", "000000"
  AddStyle colStyles, "Base16 Porple", "style-base16-porple", "base16\porple", "292c36"
  AddStyle colStyles, "Base16 Railscasts", "style-base16-railscasts", "base16\railscasts", "2b2b2b"
  AddStyle colStyles, "Base16 Rebecca", "style-base16-rebecca", "base16\rebecca", "292a44"
  AddStyle colStyles, "Base16 Seti UI", "style-base16-seti", "base16\seti", "151718"
  AddStyle colStyles, "Base16 Snazzy", "style-base16-snazzy", "base16\snazzy", "1e1f29"
  AddStyle colStyles, "Base16 Solar Flare", "style-base16-solarflare", "base16\solarflare", "18262F"
  AddStyle colStyles, "Base16 Solarized Dark", "style-base16-solarized-dark", "base16\solarized-dark", "002b36"
  AddStyle colStyles, "Base16 Solarized Light", "style-base16-solarized-light", "base16\solarized-light", "fdf6e3"
  AddStyle colStyles, "Base16 Spacemacs", "style-base16-spacemacs", "base16\spacemacs", "1f2022"
  AddStyle colStyles, "Base16 Summerfruit Dark", "style-base16-summerfruit-dark", "base16\summerfruit-dark", "151515"
  AddStyle colStyles, "Base16 Summerfruit Light", "style-base16-summerfruit-light", "base16\summerfruit-light", "FFFFFF"
  AddStyle colStyles, "Base16 Tomorrow Night", "style-base16-tomorrow-night", "base16\tomorrow-night", "1d1f21"
  AddStyle colStyles, "Base16 Tomorrow", "style-base16-tomorrow", "base16\tomorrow", "ffffff"
  AddStyle colStyles, "Base16 London Tube", "style-base16-tube", "base16\tube", "231f20"
  AddStyle colStyles, "Base16 Twilight", "style-base16-twilight", "base16\twilight", "1e1e1e"
  AddStyle colStyles, "Base16 Unikitty Dark", "style-base16-unikitty-dark", "base16\unikitty-dark", "2e2a31"
  AddStyle colStyles, "Base16 Unikitty Light", "style-base16-unikitty-light", "base16\unikitty-light", "ffffff"
  AddStyle colStyles, "Base16 Unikitty Reversible", "style-base16-unikitty-reversible", "base16\unikitty-reversible", "2e2a31"
  AddStyle colStyles, "Base16 Woodland", "style-base16-woodland", "base16\woodland", "231e18"
  AddStyle colStyles, "Base16 XCode Dusk", "style-base16-xcode-dusk", "base16\xcode-dusk", "282B35"

End Sub
