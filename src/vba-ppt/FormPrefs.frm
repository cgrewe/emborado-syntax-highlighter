VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FormPrefs 
   Caption         =   "Preferences"
   ClientHeight    =   6420
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   9555.001
   OleObjectBlob   =   "FormPrefs.frx":0000
   StartUpPosition =   1  'Fenstermitte
End
Attribute VB_Name = "FormPrefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' preferences form
' ----------------------------------------------------------------------------
Option Explicit

' ===============================================================
' initialize
' ===============================================================

'
' (re)activation of form
'
Private Sub UserForm_Activate()
  ' show extended page for further settings
  mpOptions.Pages.Item("pgExtended").visible = (ESHPrefs.GetExtendedConfigActive = 1)
End Sub

'
' initialization of form
'
Private Sub UserForm_Initialize()
  
  SetLocalizedUITexts
  
  ' add pictures in buttons
  Set cmdLoadProcessorPath.Picture = Application.CommandBars.GetImageMso("FileOpen", 16, 16)
  Set cmdLoadXmlConfigFile.Picture = Application.CommandBars.GetImageMso("FileOpen", 16, 16)
  
  ' prefill colors
  cmdBGColor1.BackColor = ESHPrefs.GetBackgroundColor(1)
  cmdBGColor2.BackColor = ESHPrefs.GetBackgroundColor(2)
  cmdFrameColor1.BackColor = ESHPrefs.GetFrameColor(1)
  cmdFrameColor2.BackColor = ESHPrefs.GetFrameColor(2)
  
  ' prefill fonts
  tbUserDefinedFont1.Text = ESHPrefs.GetUserDefinedFont(1)
  tbUserDefinedFont2.Text = ESHPrefs.GetUserDefinedFont(2)
  tbUserDefinedFont3.Text = ESHPrefs.GetUserDefinedFont(3)
  tbUserDefinedFont4.Text = ESHPrefs.GetUserDefinedFont(4)
  
  ' prefill textboxes
  tbProcessorPath.Text = ESHPrefs.GetProcessorPath
  tbXmlConfigFile.Text = ESHPrefs.GetXmlConfigFile
   
  'prefill checkboxes
  cbUseXmlConfigFile.value = ESHPrefs.GetUseXmlConfigFile
  
  If cbUseXmlConfigFile.value = True Then
    tbXmlConfigFile.enabled = True
  Else
    tbXmlConfigFile.enabled = False
  End If
    
  RefreshMultipleSelectionListBoxes
End Sub

'
' set localized texts to the GUI
'
Private Sub SetLocalizedUITexts()
  FormPrefs.Caption = ESHLocalization.GetMsg(MSGID_PREFS)
  mpOptions.Pages.Item("pgGeneral").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_GENERAL)
  mpOptions.Pages.Item("pgLanguage").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_LANGUAGE)
  mpOptions.Pages.Item("pgStyle").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_STYLE)
  mpOptions.Pages.Item("pgFont").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_FONT)
  mpOptions.Pages.Item("pgExtended").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_EXTENDED)
  mpOptions.Pages.Item("pgAbout").Caption = ESHLocalization.GetMsg(MSGID_PREFS_PG_ABOUT)
   
  frmProcessorPath.Caption = ESHLocalization.GetMsg(MSGID_PREFS_FRM_PROCESSOR_PATH)
  frmBackgroundColors.Caption = ESHLocalization.GetMsg(MSGID_PREFS_FRM_BG_COLOR)
  frmFrameColor.Caption = ESHLocalization.GetMsg(MSGID_PREFS_FRM_FRAME_COLOR)
  frmAdditionalFonts.Caption = ESHLocalization.GetMsg(MSGID_PREFS_FRM_ADDITIONAL_FONTS)
  
  lblProcessorPath.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_PROCESSOR_PATH)
  lblXmlConfigFile.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_XML_CONFIG_FILE)
  
  lblBGColor1.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_COLOR) & " 1"
  lblBGColor2.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_COLOR) & " 2"
  lblFrameColor1.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_COLOR) & " 1"
  lblFrameColor2.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_COLOR) & " 2"
  
  lblSupportedLanguages.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_SUPPORTED_LANGS)
  lblChosenLanguage.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_CHOSEN_LANGS)
  lblDescriptionLanguage.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_DESCRIPTION_LANGS)
  
  lblSupportedStyles.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_SUPPORTED_STYLES)
  lblChosenStyles.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_CHOSEN_STYLES)
  lblDescriptionStyles.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_DESCRIPTION_STYLES)
    
  lblUserDefinedFont1.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_USER_DEFINED_FONT) & " 1"
  lblUserDefinedFont2.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_USER_DEFINED_FONT) & " 2"
  lblUserDefinedFont3.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_USER_DEFINED_FONT) & " 3"
  lblUserDefinedFont4.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_USER_DEFINED_FONT) & " 4"
  lblDescriptionFonts.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_DESCRIPTION_FONTS)
  lblFontSelectorHint.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_FONT_SELECTOR_HINT)
  
  cmdClose.Caption = ESHLocalization.GetMsg(MSGID_PREFS_CMD_CLOSE)
  
  frmXmlConfigFile.Caption = ESHLocalization.GetMsg(MSGID_PREFS_FRM_EXT_HIGHLIGHT_CONFIG)
  cbUseXmlConfigFile.Caption = ESHLocalization.GetMsg(MSGID_PREFS_CB_USE_EXT_HIGHLIGHT_CONFIG)
  lblXmlConfigFile.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_XML_CONFIG_FILE)
  
  lblAbout.Caption = ESHLocalization.GetMsg(MSGID_PREFS_LBL_ABOUT_TEXT)
  
End Sub

' ===================================================
' setting and handling of background and frame colors
' ===================================================
Private Sub cmdBGColor1_Click()
  Dim lCurColor, lNextColor As Long
  lCurColor = ESHPrefs.GetBackgroundColor(1, &HF8F8F8)

  lNextColor = ESHColorChooser.DialogColor(lCurColor)

  If lNextColor > -1 Then
    cmdBGColor1.BackColor = lNextColor
    ESHPrefs.SetBackgroundColor 1, lNextColor
  End If
End Sub

Private Sub cmdBGColor2_Click()
  Dim lCurColor, lNextColor As Long
  lCurColor = ESHPrefs.GetBackgroundColor(2, &HFFFFFF)

  lNextColor = ESHColorChooser.DialogColor(lCurColor)

  If lNextColor > -1 Then
    cmdBGColor2.BackColor = lNextColor
    ESHPrefs.SetBackgroundColor 2, lNextColor
  End If
  
End Sub

Private Sub cmdFrameColor1_Click()
  Dim lCurColor, lNextColor As Long
  lCurColor = ESHPrefs.GetFrameColor(1, &HF8F8F8)

  lNextColor = ESHColorChooser.DialogColor(lCurColor)

  If lNextColor > -1 Then
    cmdFrameColor1.BackColor = lNextColor
    ESHPrefs.SetFrameColor 1, lNextColor
  End If
End Sub

Private Sub cmdFrameColor2_Click()
  Dim lCurColor, lNextColor As Long
  lCurColor = ESHPrefs.GetFrameColor(2, &HFFFFFF)

  lNextColor = ESHColorChooser.DialogColor(lCurColor)

  If lNextColor > -1 Then
    cmdFrameColor2.BackColor = lNextColor
    ESHPrefs.SetFrameColor 2, lNextColor
  End If
  
End Sub

' ===============================================
' setting and handling of processor path options
' ===============================================
Private Sub tbProcessorPath_Change()
  ESHPrefs.SetProcessorPath tbProcessorPath.Text
  
    ' refresh internal access state
  ESHExternalProcessor.CheckAccessState
 
  If ESHExternalProcessor.IsOkay Then
    tbProcessorPath.BackColor = &HC0FFC0
  Else
    tbProcessorPath.BackColor = &HC0C0FF
  End If
  
End Sub

Private Sub cmdLoadProcessorPath_Click()
  ESHUtils.LoadFolderDialogForTextBox tbProcessorPath, ESHLocalization.GetMsg(MSGID_PREFS_DLG_LOAD_PROCESSOR_PATH)
End Sub

' ===============================================
' setting and handling of XML configuration file options
' ===============================================
Private Sub tbXmlConfigFile_Change()

  ESHPrefs.SetXmlConfigFile tbXmlConfigFile.Text    ' store to reg
  ColorizeBoxBackground tbXmlConfigFile             ' adjust bachground color
  ESHPrefs.InvalidateConfiguration                  ' prepare reload of configuration
  
  If ESHUtils.FileExists(tbXmlConfigFile.Text) = False Then
    ESHLogging.logInfo "given config file does not exist", "tbXmlConfigFile_Change"
       
    ' clear all selection lists
    lstPreselectedLanguages.Clear
    lstPreselectedStyles.Clear
    lstAvailableLanguages.Clear
    lstAvailableStyles.Clear
  Else
    RefreshMultipleSelectionListBoxes
  End If
  
End Sub

Private Sub cmdLoadXmlConfigFile_Click()
  ESHUtils.LoadFileDialogForTextBox tbXmlConfigFile, _
                           ESHLocalization.GetMsg(MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE), _
                           ESHLocalization.GetMsg(MSGID_PREFS_DLG_LOAD_XML_CONFIG_FILE_TYPE), _
                           "*.xml"
End Sub

Private Sub cbUseXmlConfigFile_Click()

 tbXmlConfigFile.enabled = cbUseXmlConfigFile.value
 
 If cbUseXmlConfigFile.value = True Then
   ESHPrefs.SetUseXmlConfigFile 1
 Else
   ESHPrefs.SetUseXmlConfigFile 0
 End If
End Sub

' =========================================
' update dialog and options
' =========================================
Private Sub cmdClose_Click()
  
  FormPrefs.Hide                                  ' close dialog
  
  Dim i, j As Integer
  
  ' ----------------------
  ' -- handle languages --
  ' ----------------------
  Dim oLangDef As ESHLanguageDefinition
  Dim colPreselectedLanguages As Collection
  Set colPreselectedLanguages = New Collection
  
  ' get current language id
  Dim strCurrentLangId As String
  strCurrentLangId = ESHPrefs.GetCurrentLanguageId
  
  ' is the current language still in the preselection?
  Dim bCurrentLangAlive As Boolean
  bCurrentLangAlive = False
  
  ' get all preselected languages in a new ordered collection
  For i = 0 To lstPreselectedLanguages.ListCount - 1
    ESHLogging.logTrace "preselected (" & i & "): " & lstPreselectedLanguages.List(i), "cmdClose_Click"
    
    Set oLangDef = ESHPrefs.GetConfiguration.FindLanguageDefinitionByName(lstPreselectedLanguages.List(i))
    ESHLogging.logTrace "found it: " & oLangDef.ToString, "cmdClose_Click"
    
    colPreselectedLanguages.Add oLangDef
    
    ' check if current language ist still in preselection
    If oLangDef.id = strCurrentLangId Then bCurrentLangAlive = True
  Next
  
  ' reset current language to standard if missing in preselection
  If bCurrentLangAlive = False Then
    ESHLogging.logWarn "reset current language to standard", "cmdClose_Click"
    ESHPrefs.SetCurrentLanguageId "lang-default"
  End If
  
  ' store new ordered list
  SetIdsOfPreselectedLanguages colPreselectedLanguages
  
  ' invalidate dropdown menu
  ESHLanguageDropdownMenu.InvalidateLanguageList
  
  ' ----------------------
  ' --  handle styles   --
  ' ----------------------
  Dim oStyleDef As ESHStyleDefinition
  Dim colPreselectedStyles As Collection
  Set colPreselectedStyles = New Collection
  
  ' get current style id
  Dim strCurrentStyleId As String
  strCurrentStyleId = ESHPrefs.GetCurrentStyleId
  
  ' is the current style still in the preselection?
  Dim bCurrentStyleAlive As Boolean:
  bCurrentStyleAlive = False
  
  ' get all preselected languages in a new ordered collection
  For i = 0 To lstPreselectedStyles.ListCount - 1
    ESHLogging.logTrace "preselected (" & i & "): " & lstPreselectedStyles.List(i), "cmdClose_Click"
    
    Set oStyleDef = ESHPrefs.GetConfiguration.FindStyleDefinitionByName(lstPreselectedStyles.List(i))
    ESHLogging.logTrace "found it: " & oStyleDef.ToString, "cmdClose_Click"
    
    colPreselectedStyles.Add oStyleDef
    
    ' check if current style ist still in preselection
    If oStyleDef.id = strCurrentStyleId Then bCurrentStyleAlive = True
  Next
  
  ' reset current style to standard if missing in preselection
  If bCurrentStyleAlive = False Then
    ESHLogging.logWarn "reset current style to standard", "cmdClose_Click"
    ESHPrefs.SetCurrentStyleId "style-default"
  End If
  
  ' store new ordered list
  SetIdsOfPreselectedStyles colPreselectedStyles
  
  ' invalidate dropdown menu
  ESHStyleDropdownMenu.InvalidateStyleList
  
  ' ----------------------
  ' --  handle fonts   --
  ' ----------------------
  ' invalidate dropdown menu
  ESHFontTypeDropdownMenu.InvalidateFontList
  
  
  ' refresh controls in ribbon
  InvalidateControls
  
End Sub

' ============================================================
' handling of language entries in language page
' ============================================================

' move a single selected language entry downside
Private Sub btnLowerLanguage_Click()
  LowerItemInList lstPreselectedLanguages
End Sub

' move a single selected language entry upside
Private Sub btnRaiseLanguage_Click()
  RaiseItemInList lstPreselectedLanguages
End Sub

' sort language list in order
Private Sub btnSortLanguageAZ_Click()
  SortListBoxAZ lstPreselectedLanguages
End Sub

' sort language list in reverse order
Private Sub btnSortLanguageZA_Click()
  SortListBoxZA lstPreselectedLanguages
End Sub

' add a language entry to selection list
Private Sub cmdAddLanguage_Click()
  Dim i As Integer
  Dim counter As Integer
  counter = 0
  
  For i = 0 To lstAvailableLanguages.ListCount - 1
    If lstAvailableLanguages.Selected(i - counter) = True Then
      lstPreselectedLanguages.AddItem lstAvailableLanguages.List(i - counter)
      lstAvailableLanguages.RemoveItem (i - counter)
      counter = counter + 1
    End If
    
  Next i

  RefreshLanguagePreselectionAfterChanges
End Sub

' remove a language entry from selection list
Private Sub cmdRemoveLanguage_Click()
  Dim i As Integer
  Dim counter As Integer
  counter = 0

  For i = 0 To lstPreselectedLanguages.ListCount - 1
    If lstPreselectedLanguages.Selected(i - counter) Then
      lstAvailableLanguages.AddItem lstPreselectedLanguages.List(i - counter)
      lstPreselectedLanguages.RemoveItem (i - counter)
      counter = counter + 1
    End If
  Next i

  SortListBoxAZ lstAvailableLanguages
  
  RefreshLanguagePreselectionAfterChanges
  
End Sub

Private Sub lstPreselectedLanguages_Change()
  RefreshLanguagePreselectionAfterChanges
End Sub

' ============================================================
' handling of style entries in style page
' ============================================================

' add a style entry to selection list
Private Sub cmdAddStyle_Click()
  Dim i As Integer
  Dim counter As Integer
  counter = 0
  
  For i = 0 To lstAvailableStyles.ListCount - 1
    If lstAvailableStyles.Selected(i - counter) = True Then
      lstPreselectedStyles.AddItem lstAvailableStyles.List(i - counter)
      lstAvailableStyles.RemoveItem (i - counter)
      counter = counter + 1
    End If
    
  Next i

  RefreshStylePreselectionAfterChanges
End Sub

' remove a style entry from selection list
Private Sub cmdRemoveStyle_Click()
  Dim i As Integer
  Dim counter As Integer
  counter = 0

  For i = 0 To lstPreselectedStyles.ListCount - 1
    If lstPreselectedStyles.Selected(i - counter) Then
      lstAvailableStyles.AddItem lstPreselectedStyles.List(i - counter)
      lstPreselectedStyles.RemoveItem (i - counter)
      counter = counter + 1
    End If
  Next i

  SortListBoxAZ lstAvailableStyles
  
  RefreshStylePreselectionAfterChanges
  
End Sub

' move a single selected style entry upside
Private Sub btnRaiseStyle_Click()
  RaiseItemInList lstPreselectedStyles
End Sub

' move a single selected style entry downside
Private Sub btnLowerStyle_Click()
  LowerItemInList lstPreselectedStyles
End Sub

Private Sub lstPreselectedStyles_Change()
  RefreshStylePreselectionAfterChanges
End Sub

' ============================================================
' handling of font entries in font page
' ============================================================
' add an additional font to selection
Private Sub cmdAddUserDefinedFont1_Click()
  selectUserDefinedFont 1, tbUserDefinedFont1
End Sub

Private Sub cmdAddUserDefinedFont2_Click()
  selectUserDefinedFont 2, tbUserDefinedFont2
End Sub

Private Sub cmdAddUserDefinedFont3_Click()
  selectUserDefinedFont 3, tbUserDefinedFont3
End Sub

Private Sub cmdAddUserDefinedFont4_Click()
  selectUserDefinedFont 4, tbUserDefinedFont4
End Sub

' common logic for adding an additional font
Private Sub selectUserDefinedFont(index As Integer, tb As textBox)
  Dim Result As String

  Hide

  FormFontSelector.Show vbModal

  Result = FormFontSelector.Result
  If FormFontSelector.Cancelled = False Then
    tb.Text = Result
  End If

  lblFontSelectorHint.visible = False ' disable warning message

  Show
End Sub

' remove font selections
Private Sub cmdRemoveUserDefinedFont1_Click()
  tbUserDefinedFont1.Text = ""
  If ESHPrefs.GetCurrentFontId = FONTID_USER_DEFINED_1 Then
    ESHPrefs.SetCurrentFontId FONTID_NONE
  End If
End Sub

Private Sub cmdRemoveUserDefinedFont2_Click()
  clearUserDefinedFont tbUserDefinedFont2, FONTID_USER_DEFINED_2
End Sub

Private Sub cmdRemoveUserDefinedFont3_Click()
  clearUserDefinedFont tbUserDefinedFont3, FONTID_USER_DEFINED_3
End Sub

Private Sub cmdRemoveUserDefinedFont4_Click()
  clearUserDefinedFont tbUserDefinedFont4, FONTID_USER_DEFINED_4
End Sub

' changes in the four font selection boxes
Private Sub tbUserDefinedFont1_Change()
  persistUserDefinedFont tbUserDefinedFont1, 1
End Sub

Private Sub tbUserDefinedFont2_Change()
  persistUserDefinedFont tbUserDefinedFont2, 2
End Sub

Private Sub tbUserDefinedFont3_Change()
  persistUserDefinedFont tbUserDefinedFont3, 3
End Sub

Private Sub tbUserDefinedFont4_Change()
  persistUserDefinedFont tbUserDefinedFont4, 4
End Sub

' common logic for persisting the changes
Private Sub persistUserDefinedFont(tb As textBox, index As Integer)
  ESHPrefs.SetUserDefinedFont index, tb.Text
  
  If tb.Text = "" Then
    tb.BackColor = &HC0C0FF
  Else
    tb.BackColor = &HC0FFC0
  End If
  
End Sub

' common logic for persisting the changes
' id: font id
' tb: textbox
Private Sub clearUserDefinedFont(tb As textBox, id As String)
  
  tb.Text = ""
  If ESHPrefs.GetCurrentFontId = id Then
    ESHPrefs.SetCurrentFontId FONTID_NONE
  End If
  
End Sub


' ============================================================
' update logic after events in the lists
' ============================================================
Private Sub RefreshLanguagePreselectionAfterChanges()
  Dim i As Integer
  Dim iSelected As Integer
  iSelected = 0
   
  For i = 0 To lstPreselectedLanguages.ListCount - 1
    If lstPreselectedLanguages.Selected(i) Then iSelected = iSelected + 1
  Next
 
  If iSelected = 1 And lstPreselectedLanguages.ListCount > 1 Then
    btnRaiseLanguage.enabled = True
    btnLowerLanguage.enabled = True
  Else
    btnRaiseLanguage.enabled = False
    btnLowerLanguage.enabled = False
  End If
  
  If lstPreselectedLanguages.ListCount > 1 Then
    btnSortLanguageAZ.enabled = True
    btnSortLanguageZA.enabled = True
  Else
    btnSortLanguageAZ.enabled = False
    btnSortLanguageZA.enabled = False
  End If
  
End Sub

Private Sub RefreshStylePreselectionAfterChanges()
  Dim i As Integer
  Dim iSelected As Integer
  iSelected = 0
  
  For i = 0 To lstPreselectedStyles.ListCount - 1
    If lstPreselectedStyles.Selected(i) Then iSelected = iSelected + 1
  Next
 
  If iSelected = 1 And lstPreselectedStyles.ListCount > 1 Then
    btnRaiseStyle.enabled = True
    btnLowerStyle.enabled = True
  Else
    btnRaiseStyle.enabled = False
    btnLowerStyle.enabled = False
  End If
  
End Sub

' ============================================================
' update logic after configuration changes
' ============================================================
Private Sub RefreshMultipleSelectionListBoxes()
  
  Dim i As Integer
   
  ' get list of available languages
  Dim colSupportedLang As Collection
  Set colSupportedLang = ESHPrefs.GetConfiguration.SupportedLanguages

  ' get list of preselected languages in order
  Dim colPreselectedLang As New Collection
  ESHPrefs.GetPreselectedLanguages colPreselectedLang

  Dim oDefLang As ESHLanguageDefinition
   
  ' fill the list of available languages, but ignore the standard and
  ' all preselected languages
  With lstAvailableLanguages

    .Clear

    For i = 1 To colSupportedLang.count
      Set oDefLang = colSupportedLang.Item(i)
      
      If Not oDefLang.id = "lang-default" Then
        If Not ESHUtils.HasKey(colPreselectedLang, oDefLang.id) Then .AddItem oDefLang.name
      End If
      
    Next

  End With

  ' fill the preselected list box with all preselected entries in order
  With lstPreselectedLanguages

    .Clear

    For i = 1 To colPreselectedLang.count
      Set oDefLang = colPreselectedLang.Item(i)

      If Not oDefLang.id = "lang-default" Then
        .AddItem oDefLang.name
      End If
    Next

  End With
  
  ' the available languages list box on the left side is always in order
  SortListBoxAZ lstAvailableLanguages
  
  ' --------------------------------------------------------------------------
  
  ' get list of available styles
  Dim colSupportedStyles As Collection
  Set colSupportedStyles = ESHPrefs.GetConfiguration.SupportedStyles

  ' get list of preselected styles in order
  Dim colPreselectedStyles As New Collection
  ESHPrefs.GetPreselectedStyles colPreselectedStyles

  Dim oDefStyle As ESHStyleDefinition
  
  ' fill the list of available styles, but ignore the standard and
  ' all preselected styles
  With lstAvailableStyles

    .Clear

    For i = 1 To colSupportedStyles.count
      Set oDefStyle = colSupportedStyles.Item(i)
      
      If Not oDefStyle.id = "style-default" Then
        If Not ESHUtils.HasKey(colPreselectedStyles, oDefStyle.id) Then .AddItem oDefStyle.name
      End If
      
    Next

  End With
  
  ' fill the preselected styles list box with all preselected entries in order
  With lstPreselectedStyles

    .Clear

    For i = 1 To colPreselectedStyles.count
      Set oDefStyle = colPreselectedStyles.Item(i)

      If Not oDefStyle.id = "style-default" Then
        .AddItem oDefStyle.name
      End If
    Next

  End With
  
  ' the available styles list box on the left side is always in order
  SortListBoxAZ lstAvailableStyles
  
End Sub

' ====================================================================
' internal helper methods
' ====================================================================

'
' Colorize text box dependent on existence of referenced file
'
Private Sub ColorizeBoxBackground(tb As textBox)
  If ESHUtils.FileExists(tb.Text) Then
    tb.BackColor = &HC0FFC0
  Else
    tb.BackColor = &HC0C0FF
  End If
End Sub

'
' raise an item in a given list by one position upwards
'
Private Sub RaiseItemInList(selectedList As Variant)
  Dim i As Integer
  Dim temp As Variant
  
  With selectedList
  
    For i = 1 To .ListCount - 1
      If .Selected(i) Then
        .Selected(i) = False
        temp = .List(i - 1)
        .List(i - 1) = .List(i)
        .List(i) = temp
        .Selected(i - 1) = True
        Exit For
      End If
    Next i
  
  End With
  
End Sub

'
' lower an item in a given list by one position downwards
'
Private Sub LowerItemInList(selectedList As Variant)
  Dim i As Integer
  Dim temp As Variant
  
  With selectedList
  
    For i = 0 To .ListCount - 2
      If .Selected(i) Then
        .Selected(i) = False
        temp = .List(i + 1)
        .List(i + 1) = .List(i)
        .List(i) = temp
        .Selected(i + 1) = True
        Exit For
      End If
    Next i
  
  End With
End Sub

'
' sort the content of a given list box in alphabetibal order
'
Private Sub SortListBoxAZ(listBox As MSForms.listBox)

  Dim i As Long
  Dim j As Long
  Dim temp As Variant

  ' sort listBox in A-Z order
  With listBox
    For i = 0 To .ListCount - 2
      For j = i + 1 To .ListCount - 1
      
        'Debug.Print "list count " & .ListCount
        'Debug.Print "i: " & i & "; j: " & j & "; l(i): " & .list(i)
        
        If LCase(.List(i)) > LCase(.List(j)) Then
          temp = .List(j)
          .List(j) = .List(i)
          .List(i) = temp
        End If
      Next j
    Next i
  End With

End Sub

'
' sort the content of a given list box in reverse alphabetibal order
'
Private Sub SortListBoxZA(listBox As MSForms.listBox)

  Dim i As Long
  Dim j As Long
  Dim temp As Variant

  ' sort listBox in Z-A order
  With listBox
    For i = 0 To .ListCount - 2
      For j = i + 1 To .ListCount - 1
      
        'Debug.Print "list count " & .ListCount
        'Debug.Print "i: " & i & "; j: " & j & "; l(i): " & .list(i)
        
        If LCase(.List(i)) < LCase(.List(j)) Then
          temp = .List(j)
          .List(j) = .List(i)
          .List(i) = temp
        End If
      Next j
    Next i
  End With

End Sub


