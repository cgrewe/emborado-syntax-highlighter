Attribute VB_Name = "ESHLogging"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Logging: central logging instance
'
' Currently, only console logging is supported. The log level can be set by
' dev mode ribbon entry or by registry key.
' ----------------------------------------------------------------------------
Option Explicit

' ====================================================
' central logging module
' ====================================================

' log levels
Public Enum logLevel
 lvlTrace = 0
 lvlDebug = 1
 lvlWarn = 2
 lvlError = 3
 lvlInfo = 4
End Enum

' logger object (singleton)
Public logger As ESHLogger

' =======================================
' logging interface for usage
'
' IN:
' sMessage    log message
' sLogPoint   (optional) point in code
'
' =======================================
Public Sub logInfo(sMessage As String, Optional sLogPoint As String)
  log sMessage, logLevel.lvlInfo, sLogPoint
End Sub

Public Sub logWarn(sMessage As String, Optional sLogPoint As String)
  log sMessage, logLevel.lvlWarn, sLogPoint
End Sub

Public Sub logError(sMessage As String, Optional sLogPoint As String)
  log sMessage, logLevel.lvlError, sLogPoint
End Sub

Public Sub logTrace(sMessage As String, Optional sLogPoint As String)
  log sMessage, logLevel.lvlTrace, sLogPoint
End Sub

Public Sub logDebug(sMessage As String, Optional sLogPoint As String)
  log sMessage, logLevel.lvlDebug, sLogPoint
End Sub

' =======================================
' I N T E R N A L
' =======================================

'
' internal handling of singleton object
'
Private Sub log(sMessage As String, eLogLevel As logLevel, Optional sLogPoint As String)
  Static defLogger As New ESHLogger
  
  If (logger Is Nothing) Then
    Set logger = defLogger
    defLogger.level = ESHPrefs.GetLogLevel
  End If
  
  Call logger.log(sMessage, eLogLevel, sLogPoint)

End Sub
