Attribute VB_Name = "ESHTBoxBackgroundDropdownMenu"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Text Box Backgroud Color Dropdown Menu: handler for drop down menu
' ----------------------------------------------------------------------------
Option Explicit

' constants
Public Const TBBGID_NONE As String = "tbbg-none"
Public Const TBBGID_TRANSPARENT As String = "tbbg-transparent"
Public Const TBBGID_STYLE As String = "tbbg-style"
Public Const TBBGID_COLOR1 As String = "tbbg-color1"
Public Const TBBGID_COLOR2 As String = "tbbg-color2"

' ----------------------------------------------------------
' callbacks from the ribbon background dropdown list
' ----------------------------------------------------------

' Callback for GetLabel
Sub GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_DD_BGC_STRATEGY)
End Sub

' Callback for GetItemCount
Sub GetItemCount(control As IRibbonControl, ByRef count)
  count = 5
End Sub

' Callback for GetItemLabel
Sub GetItemLabel(control As IRibbonControl, index As Integer, ByRef label)
  Select Case index
    Case 0
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_BGC_NO_ASSIGNMENT)
    Case 1
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_BGC_TRANSPARENT)
    Case 2
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_BGC_LIKE_STYLE)
    Case 3
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_BGC_COLOR1)
    Case 4
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_BGC_COLOR2)
  End Select
End Sub

' Callback for GetItemID
Sub GetItemID(control As IRibbonControl, index As Integer, ByRef id)
  Select Case index
    Case 0
      id = TBBGID_NONE
    Case 1
      id = TBBGID_TRANSPARENT
    Case 2
      id = TBBGID_STYLE
    Case 3
      id = TBBGID_COLOR1
    Case 4
      id = TBBGID_COLOR2
  End Select
  
  'Debug.Print "GetItemID: " & index & " ; " & id
  
End Sub

' Callback for GetSelectedItemID
Sub GetSelectedItemID(control As IRibbonControl, ByRef index)
  ' get currently set BGC
  Dim sBGC As String
  sBGC = ESHPrefs.GetTextBoxBackground
  
  ' check plausability of found id string
  Select Case sBGC
    Case TBBGID_NONE, TBBGID_TRANSPARENT, TBBGID_STYLE, TBBGID_COLOR1, TBBGID_COLOR2
      index = sBGC
    Case Else
      index = TBBGID_NONE
      ESHLogging.logWarn "found background color id [" & sBGC & "] is unknown.", "GetSelectedItemID"
  End Select
  
End Sub

' Callback for onAction
Sub OnAction(control As IRibbonControl, id As String, index As Integer)

  ESHPrefs.SetTextBoxBackground id
    
  If bLoggingActive = True Then
    MsgBox "set textbox background: " & id & " / index: " & index
  End If
    
End Sub
