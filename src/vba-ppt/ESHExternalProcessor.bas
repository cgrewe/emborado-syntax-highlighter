Attribute VB_Name = "ESHExternalProcessor"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' external highlight processor
' ----------------------------------------------------------------------------
Option Explicit

' The availability of the highlight processor is essential for the add-in
' The add-in must distinguish between different scenarios of problems.
Public Enum ProcessorAccessState
  noConfiguration = -3
  pathInvalid = -2
  executableInvalid = -1
  okay = 1
End Enum

' -------------------------------------
' member variables
' -------------------------------------
Private m_AccessState As ProcessorAccessState

' -------------------------------------
' public methods
' -------------------------------------
Public Function IsOkay() As Boolean
  IsOkay = (m_AccessState = okay)
End Function

Public Function GetAccessState() As ProcessorAccessState
  GetAccessState = m_AccessState
End Function

Public Function CheckAccessState() As ProcessorAccessState
  m_AccessState = UpdateAccessState
  CheckAccessState = m_AccessState
End Function

' -----------------------------------------------------------
' run the external transformation script
' -----------------------------------------------------------
Sub RunHighlightProcessor(bShowHLProcessor As Boolean)

  FormStatusBar.OpenStatusBar (6)
  
  Dim iShowCmdCall As Integer: iShowCmdCall = 0
 
  ' get shell object
  Dim objShell As Object
  Set objShell = VBA.CreateObject("Wscript.Shell")
 
  Dim statement As String
  Dim parameter As String
                
  FormStatusBar.Progress
  
  ' prepare powershell script, optionally with further debug messages
  If bShowHLProcessor = False Then
    statement = "powershell -NoProfile -NoLogo -Command " & _
                Q("Add-Type -AssemblyName System.Windows.Forms; $inp = Get-Clipboard; $rtf = ( echo $inp | """ & _
                  ESHPrefs.GetProcessorPath & "\highlight.exe" & """ -O rtf --syntax " & ESHPrefs.GetCurrentLanguageDefinition.parameter & " --style " & _
                  ESHPrefs.GetCurrentStyleDefinition.parameter & _
                  " --no-trailing-nl) ; [Windows.Forms.Clipboard]::SetText($rtf, [Windows.Forms.TextDataFormat]::Rtf)")
  Else
    statement = "powershell -NoProfile -NoLogo -Command " & _
                Q("Add-Type -AssemblyName System.Windows.Forms; $inp = Get-Clipboard; echo $inp; $rtf = ( echo $inp | """ & _
                  ESHPrefs.GetProcessorPath & "\highlight.exe" & """ -O rtf --syntax " & ESHPrefs.GetCurrentLanguageDefinition.parameter & " --style " & _
                  ESHPrefs.GetCurrentStyleDefinition.parameter & _
                  " --no-trailing-nl) ; echo $rtf ; [Windows.Forms.Clipboard]::SetText($rtf, [Windows.Forms.TextDataFormat]::Rtf); pause;")
    iShowCmdCall = 1
  End If
  
  ESHLogging.logTrace "before call", "RunHighlightProcessor"
  
  ESHLogging.logDebug statement & ", " & iShowCmdCall & ", True", "RunHighlightProcessor"
  
  ' execute prepared statement
  FormStatusBar.Progress
  objShell.Run statement, iShowCmdCall, True
  FormStatusBar.Progress
  
  ESHLogging.logTrace "after call", "RunHighlightProcessor"
   
End Sub

' -------------------------------------
' internal methods
' -------------------------------------
Private Function UpdateAccessState() As ProcessorAccessState
 
  ' check registry for missing key
  If ESHPrefs.IsProcessorPathExisting = False Then
    UpdateAccessState = ProcessorAccessState.noConfiguration
    Exit Function
  End If
  
  Dim strProcPath As String
  strProcPath = ESHPrefs.GetProcessorPath
  
  ' check path for validity
  If strProcPath = vbNullString Then
    UpdateAccessState = ProcessorAccessState.pathInvalid
    Exit Function
  ElseIf Len(strProcPath) = 0 Then
    UpdateAccessState = ProcessorAccessState.pathInvalid
    Exit Function
  End If
  
  ' check path does exist
  If ESHUtils.FolderExists(strProcPath) = False Then
    UpdateAccessState = ProcessorAccessState.pathInvalid
    Exit Function
  End If
  
  ' check executable does exist
  If ESHUtils.FileExists(strProcPath & "\highlight.exe") = False Then
    UpdateAccessState = ProcessorAccessState.executableInvalid
    Exit Function
  End If
  
  ' bingo, we found an executable
  UpdateAccessState = ProcessorAccessState.okay
    
End Function

' qualify a string by surrounding double-quotes
Private Function Q(str As String) As String
  Q = """" & str & """"
End Function

' ================ TEST CODE ==============
Sub Test1()
  Debug.Print "Get state: " & ESHPrefs.GetProcessorPath
  Debug.Print "upd state: " & CheckAccessState
  Debug.Print "Get state: " & GetAccessState
End Sub

