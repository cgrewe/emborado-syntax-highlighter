VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHStyleDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Style definition class
' ----------------------------------------------------------------------------

' Class member variables
Private m_name As String
Private m_id As String
Private m_bgColorHex As String
Private m_parameter As String

' Ctor event - triggered when class created
Private Sub Class_Initialize()
End Sub

' Properties
Property Get name() As String
  name = m_name
End Property

Property Let name(value As String)
  m_name = value
End Property

Property Get id() As String
  id = m_id
End Property

Property Let id(value As String)
  m_id = value
End Property

Property Get parameter() As String
  parameter = m_parameter
End Property

Property Let parameter(value As String)
  m_parameter = value
End Property

Property Get BackgroundColor() As String
  BackgroundColor = m_bgColorHex
End Property

Property Let BackgroundColor(value As String)
  m_bgColorHex = value
End Property

Property Get BackgroundColorAsRGB() As Long
  Dim lColor As Long
  lColor = CLng("&h" & m_bgColorHex)
  
  Dim iRed, iGreen, iBlue As Integer
  
  iBlue = (lColor Mod 256)
  iGreen = (lColor \ 256) Mod 256
  iRed = (lColor \ 65536) Mod 256
  
  ESHLogging.logTrace "color: " & iRed & ", " & iGreen & ", " & iBlue, "BackgroundColorAsRGB"
  
  BackgroundColorAsRGB = RGB(iRed, iGreen, iBlue)
  
End Property

Public Sub InitiateProperties(name As String, id As String, parameter As String, bgColorHex As String)
  m_name = name
  m_id = id
  m_parameter = parameter
  m_bgColorHex = bgColorHex
End Sub

Public Function ToString() As String
  ToString = "style definition (" & m_name & ", " & m_id & ", " & m_parameter & ", " & m_bgColorHex & ")"
End Function

