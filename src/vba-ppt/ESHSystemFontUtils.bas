Attribute VB_Name = "ESHSystemFontUtils"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' System font handler
' The module collects the installed system fonts and provides a list
' with name and monospace indicator
'
' based on https://www.vbarchiv.net/api/api_enumfontfamiliesex.html
'
' ----------------------------------------------------------------------------
Option Explicit

' ------------------------------------------
' member variable
' ------------------------------------------
Private sharedSystemFontList As Collection
Private iFontsCounter As Integer, ttFontsCounter As Integer

#If VBA7 Then
  Declare PtrSafe Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As LongPtr)
  Declare PtrSafe Function EnumFontFamiliesEx Lib "gdi32" Alias "EnumFontFamiliesExA" (ByVal hdc As LongPtr, lpLogFont As LOGFONT, ByVal lpFontEnumProc As LongPtr, ByVal lParam As LongPtr, ByVal dwFlags As LongPtr) As Long
  Declare PtrSafe Function lstrlen Lib "kernel32" Alias "lstrlenA" (ByVal lpString As String) As Long
  Declare PtrSafe Function GetDC Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
#End If

' TODO: Solution for VBA < 7

' ====================================
' constants
' ====================================
Private Const TRUETYPE_FONTTYPE = &H4

' ====================================
' structures
' ====================================

' see https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-logfonta
Public Type LOGFONT
  lfHeight As Long
  lfWidth As Long
  lfEscapement As Long
  lfOrientation As Long
  lfWeight As Long
  lfItalic As Byte
  lfUnderline As Byte
  lfStrikeOut As Byte
  lfCharSet As Byte
  lfOutPrecision As Byte
  lfClipPrecision As Byte
  lfQuality As Byte
  lfPitchAndFamily As Byte
  lfFaceName As String * 32 ' fixed length
End Type

' see https://learn.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-enumlogfontexa
Private Type ENUMLOGFONTEX
  elfLogFont As LOGFONT
  elfFullName As String * 64
  elfStyle As String * 32
  elfScript As String * 32
End Type
 
' see https://learn.microsoft.com/de-de/windows/win32/api/wingdi/ns-wingdi-newtextmetrica
Private Type NEWTEXTMETRIC
  tmHeight As Long
  tmAscent As Long
  tmDescent As Long
  tmInternalLeading As Long
  tmExternalLeading As Long
  tmAveCharWidth As Long
  tmMaxCharWidth As Long
  tmWeight As Long
  tmOverhang As Long
  tmDigitizedAspectX As Long
  tmDigitizedAspectY As Long
  tmFirstChar As Byte
  tmLastChar As Byte
  tmDefaultChar As Byte
  tmBreakChar As Byte
  tmItalic As Byte
  tmUnderlined As Byte
  tmStruckOut As Byte
  tmPitchAndFamily As Byte
  tmCharSet As Byte
  ntmFlags As Long
  ntmSizeEM As Long
  ntmCellHeight As Long
  ntmAveWidth As Long
End Type
 
' see https://learn.microsoft.com/de-de/windows/win32/api/wingdi/ns-wingdi-textmetrica
Private Type TEXTMETRIC
  tmHeight As Long
  tmAscent As Long
  tmDescent As Long
  tmInternalLeading As Long
  tmExternalLeading As Long
  tmAveCharWidth As Long
  tmMaxCharWidth As Long
  tmWeight As Long
  tmOverhang As Long
  tmDigitizedAspectX As Long
  tmDigitizedAspectY As Long
  tmFirstChar As Byte
  tmLastChar As Byte
  tmDefaultChar As Byte
  tmBreakChar As Byte
  tmItalic As Byte
  tmUnderlined As Byte
  tmStruckOut As Byte
  tmPitchAndFamily As Byte
  tmCharSet As Byte
End Type

'
' callback function called for any font found
'
#If VBA7 Then
Public Function FntEnumProcCallbackFnct(ByVal FontDesc As LongPtr, ByVal TMetric As LongPtr, ByVal FontType As LongPtr, ByVal lParam As Long) As Long
#Else
Public Function FntEnumProcCallbackFnct(ByVal FontDesc As Long, ByVal TMetric As Long, ByVal FontType As Long, ByVal lParam As Long) As Long
#End If

  Dim LFont As ENUMLOGFONTEX, TM As TEXTMETRIC, NTM As NEWTEXTMETRIC
  Dim oFontDef As ESHSystemFont
  Dim fontName As String

  ' Fontinformationen in die Struktur kopieren
  MoveMemory LFont, ByVal FontDesc, Len(LFont)
 
  ' Erweiterte Textinformationen in die Struktur kopieren
  If CBool(FontType And TRUETYPE_FONTTYPE) = False Then
    MoveMemory TM, ByVal TMetric, Len(TM)
  Else
    MoveMemory NTM, ByVal TMetric, Len(NTM)
  End If
 
  iFontsCounter = iFontsCounter + 1
 
  ' filter for truetype fonts
  If CBool(FontType And TRUETYPE_FONTTYPE) = True Then
    ttFontsCounter = ttFontsCounter + 1
    fontName = Left$(LFont.elfLogFont.lfFaceName, lstrlen(LFont.elfLogFont.lfFaceName))
  
    ' remove vertical fonts
    If InStr(1, fontName, "@") = False Then
      Set oFontDef = New ESHSystemFont
      oFontDef.InitiateProperties fontName, (LFont.elfLogFont.lfPitchAndFamily And 1)
      sharedSystemFontList.Add oFontDef
    End If
  End If
 
  ' continue enumeration of fonts?
  If (iFontsCounter > 300) Then
    FntEnumProcCallbackFnct = 0
  Else
    FntEnumProcCallbackFnct = 1
  End If
End Function

' ------------------------------------------
' get font collection singleton
' ------------------------------------------
Public Function GetSystemFonts() As Collection
  Dim Retval As Long
  Dim LF As LOGFONT
  Dim i As Long, j As Long
  Dim vTemp As Variant
    
  ESHLogging.logTrace "get system fonts", "GetSystemFonts"

  If sharedSystemFontList Is Nothing Then
    Set sharedSystemFontList = New Collection
            
#If VBA7 Then

    iFontsCounter = 0
    ttFontsCounter = 0
    Retval = EnumFontFamiliesEx(GetDC(0), LF, AddressOf FntEnumProcCallbackFnct, 0&, 0&)
    ESHLogging.logTrace "found no font list, read in: " & Retval, "GetSystemFonts"
       
    ' lexical sorting font names by bubble sort
    For i = 1 To sharedSystemFontList.count - 1
      For j = i + 1 To sharedSystemFontList.count
        If sharedSystemFontList(i).name > sharedSystemFontList(j).name Then
          Set vTemp = sharedSystemFontList(j)           ' store the lexically lesser item
          sharedSystemFontList.Remove j                 ' remove the lesser item from collection
          sharedSystemFontList.Add vTemp, Before:=i     ' add the stored item in front of i-th element
        End If
      Next j
    Next i

#Else ' not VBA7
    ESHLogging.logInfo "not supported in 32 Bit", "GetSystemFonts"
#End If

  Else
    ESHLogging.logTrace "found shared font list", "GetSystemFonts"
  End If

  Set GetSystemFonts = sharedSystemFontList

End Function

Public Sub InvalidateSystemFonts()
  ESHLogging.logDebug "Invalidate configuration singleton", "InvalidateSystemFonts"
  Set sharedSystemFontList = Nothing
End Sub

Private Sub Test()
    Dim i As Integer
    Dim abc As Collection
    
    Set abc = GetSystemFonts
   
   If abc Is Nothing Then
   Debug.Print "nothing"
   End If
   
    For i = 1 To abc.count
      Debug.Print abc.Item(i).ToString
    Next
    
End Sub
