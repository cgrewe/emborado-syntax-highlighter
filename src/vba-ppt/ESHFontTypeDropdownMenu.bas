Attribute VB_Name = "ESHFontTypeDropdownMenu"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Font Type Dropdown Menu: handler for drop down menu
' ----------------------------------------------------------------------------
Option Explicit

' -------------------------------------
' constants
' -------------------------------------
Public Const FONTID_NONE As String = "font-none"
Public Const FONTID_CONSOLAS As String = "font-consolas"
Public Const FONTID_COURIER_NEW As String = "font-courier-new"
Public Const FONTID_LUCIDA_CONSOLE As String = "font-lucida-console"
Public Const FONTID_USER_DEFINED_1 As String = "font-user-defined-1"
Public Const FONTID_USER_DEFINED_2 As String = "font-user-defined-2"
Public Const FONTID_USER_DEFINED_3 As String = "font-user-defined-3"
Public Const FONTID_USER_DEFINED_4 As String = "font-user-defined-4"

' -------------------------------------
' member variables
' -------------------------------------

' --------------------------------------------
' invalidate the list after changes have done
' --------------------------------------------
Sub InvalidateFontList()
  ESHLogging.logTrace "invalidate font list", "InvalidateFontList"
  ESHPrefs.GetConfiguration.InvalidateFontList
End Sub

' ----------------------------------------------------------
' callbacks from the ribbon frame dropdown list
' ----------------------------------------------------------

'Callback for GetLabel
Sub GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_DD_FONT_TYPE)
End Sub

' Callback for GetItemCount
Sub GetItemCount(control As IRibbonControl, ByRef count)
  count = ESHPrefs.GetConfiguration.FontCount
  'Debug.Print "GetItemCount: " & count
End Sub

' Callback for GetItemLabel
Sub GetItemLabel(control As IRibbonControl, index As Integer, ByRef label)
  Select Case index
    Case 0
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_FONT_NO_ASSIGNMENT)

    Case Else
      label = ESHPrefs.GetConfiguration.SupportedFonts(index + 1).name
  End Select
End Sub

' Callback for GetItemID
Sub GetItemID(control As IRibbonControl, index As Integer, ByRef id)
  Select Case index
    Case 0
      id = FONTID_NONE
      
    Case Else
      id = ESHPrefs.GetConfiguration.SupportedFonts(index + 1).id
      
  End Select
  
  'Debug.Print "GetItemID: " & index & " ; " & id
  
End Sub

' Callback for GetSelectedItemID
Sub GetSelectedItemID(control As IRibbonControl, ByRef index)
  ' get currently set font
  Dim sFontType As String
  sFontType = ESHPrefs.GetCurrentFontId
  
  ' check plausability of found id string
  Select Case sFontType
    Case FONTID_NONE, FONTID_CONSOLAS, FONTID_COURIER_NEW, FONTID_LUCIDA_CONSOLE, _
         FONTID_USER_DEFINED_1, FONTID_USER_DEFINED_2, FONTID_USER_DEFINED_3, FONTID_USER_DEFINED_4
      index = sFontType
    Case Else
      index = FONTID_NONE
      ESHLogging.logWarn "found font id [" & sFontType & "] is unknown.", "GetSelectedItemID"
  End Select
  
End Sub

' Callback for OnAction
Sub OnAction(control As IRibbonControl, id As String, index As Integer)

  ESHPrefs.SetCurrentFontId id
  
  If bLoggingActive = True Then
    MsgBox "set font: " & id & " / index: " & index
  End If
    
End Sub

