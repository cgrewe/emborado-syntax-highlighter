Attribute VB_Name = "ESHPrefs"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Prefs: central handler for all kind of registry-based preferences and settings
' ----------------------------------------------------------------------------
Option Explicit

' keys for options in registry
Private Const REG_BASE As String = "HKEY_CURRENT_USER\SOFTWARE\Emborado\SyntaxHighlighter\"

Private Const KEY_CURRENT_LANGUAGE As String = "CurrentLanguage"                ' currently chosen language
Private Const KEY_CURRENT_STYLE As String = "CurrentStyle"                      ' currently chosen highlight style
Private Const KEY_CURRENT_FONT_TYPE As String = "CurrentFontType"               ' currently uses font type
Private Const KEY_HIGHLIGHT_PROCESSOR_PATH As String = "HighlightProcessorPath" ' absolute path to highlight processor
Private Const KEY_TEXTBOX_BACKGROUND As String = "TextBoxBackground"            ' currently chosen background coloring strategy
Private Const KEY_TEXTBOX_FRAME As String = "TextBoxFrame"                      ' currently chosen frame coloring strategy
Private Const KEY_SELECTABLE_LANGUAGES As String = "SelectableLanguageList"     ' list of preselected languages
Private Const KEY_SELECTABLE_STYLES As String = "SelectableStyleList"           ' list of preselected styles
Private Const KEY_BACKGROUND_COLOR As String = "Colors\Background"              ' color definitions
Private Const KEY_FRAME_COLOR As String = "Colors\Frame"
Private Const KEY_USER_DEFINED_FONT As String = "Fonts\UserDefinedFont"         ' user defined font

' these keys are needed for development or observation
Private Const KEY_DEV_MODE_ACTIVE As String = "DevMode\DevModeActive"           ' activate development menu
Private Const KEY_DEV_GUI_LANGUAGE_ID As String = "DevGuiLanguageId"            ' overwrites read system language Id
Private Const KEY_LOG_LEVEL As String = "LogLevel"                              ' sets log level

' extended configuration (only enabled by reg key)
Private Const KEY_EXTENDED_CONFIG_ACTIVE As String = "ExtendedConfigActive"     ' extended config page active
Private Const KEY_XML_CONFIG_FILE As String = "Extended\XmlConfigFile"          ' absolute path to XML configuration file
Private Const KEY_USE_XML_CONFIG_FILE As String = "Extended\UseXmlConfigFile"   ' use of XML configuration file

' -----------------------
'  default option values
' -----------------------
Private Const DEFVAL_CURRENT_LANGUAGE As String = ESHLanguageDropdownMenu.LANGID_DEFAULT
Private Const DEFVAL_CURRENT_STYLE As String = ESHStyleDropdownMenu.STYLEID_DEFAULT
Private Const DEFVAL_CURRENT_FONT_TYPE As String = ESHFontTypeDropdownMenu.FONTID_NONE
Private Const DEFVAL_INITIALIZED As String = "false"
Private Const DEFVAL_HIGHLIGHT_PROCESSOR_PATH As String = ""
Private Const DEFVAL_XML_CONFIG_FILE As String = ""
Private Const DEFVAL_TEXTBOX_BACKGROUND As String = ESHTBoxBackgroundDropdownMenu.TBBGID_NONE
Private Const DEFVAL_TEXTBOX_FRAME As String = ESHTBoxFrameDropdownMenu.TBFID_NONE
Private Const DEFVAL_COLOR As Long = &HF8F8F8
Private Const DEFVAL_USE_XML_CONFIG_FILE As String = "false"

' ------------------------------------------
' member variable
' ------------------------------------------
Private objSharedExtConfig As ESHConfiguration

' ------------------------------------------
' get configuration singleton
' ------------------------------------------
Public Function GetConfiguration() As ESHConfiguration

  If objSharedExtConfig Is Nothing Then
    'Debug.Print "no shared object found!"
    Set objSharedExtConfig = New ESHConfiguration
  Else
    'Debug.Print "found shared object found"
  End If

  Set GetConfiguration = objSharedExtConfig

End Function

Public Sub InvalidateConfiguration()
  ESHLogging.logDebug "Invalidate configuration singleton", "InvalidateConfiguration"
  Set objSharedExtConfig = Nothing
End Sub

' ------------------------------------------
' get current language definition
' ------------------------------------------
Public Function GetCurrentLanguageDefinition() As ESHLanguageDefinition

  Dim strLangId As String
  strLangId = GetCurrentLanguageId
  
  ESHLogging.logDebug "get current language id " & strLangId, "GetCurrentLanguageDefinition"
  
  Set GetCurrentLanguageDefinition = GetConfiguration.SupportedLanguages.Item(strLangId)
 
End Function

' ------------------------------------------
' get current style definition
' ------------------------------------------
Public Function GetCurrentStyleDefinition() As ESHStyleDefinition

  Dim strStyleId As String
  strStyleId = GetCurrentStyleId
  ESHLogging.logDebug "get current style id " & strStyleId, "GetCurrentStyleDefinition"
  
  Set GetCurrentStyleDefinition = GetConfiguration.SupportedStyles.Item(strStyleId)
 
End Function

' ------------------------------------------
' get current font definition
' ------------------------------------------
Public Function GetCurrentFontDefinition() As ESHFontDefinition

  Dim strFontId As String
  strFontId = GetCurrentFontId
  ESHLogging.logDebug "get current font id " & strFontId, "GetCurrentFontDefinition"
  
  Set GetCurrentFontDefinition = GetConfiguration.SupportedFonts.Item(strFontId)
 
End Function

' ---------------------------------
' setter & getter for registry
' ---------------------------------

' set current language id
Sub SetCurrentLanguageId(languageId As String)
  RegKeySave KEY_CURRENT_LANGUAGE, languageId
End Sub

' get current language id
Function GetCurrentLanguageId() As String
  GetCurrentLanguageId = RegKeyRead(KEY_CURRENT_LANGUAGE, DEFVAL_CURRENT_LANGUAGE)
End Function

' set current style id
Sub SetCurrentStyleId(styleId As String)
  RegKeySave KEY_CURRENT_STYLE, styleId
End Sub

' get current style id
Function GetCurrentStyleId() As String
  GetCurrentStyleId = RegKeyRead(KEY_CURRENT_STYLE, DEFVAL_CURRENT_STYLE)
End Function

' set current font type
Sub SetCurrentFontId(FontType As String)
  RegKeySave KEY_CURRENT_FONT_TYPE, FontType
End Sub

' get current font type
Function GetCurrentFontId() As String
  GetCurrentFontId = RegKeyRead(KEY_CURRENT_FONT_TYPE, DEFVAL_CURRENT_FONT_TYPE)
End Function

' get processor installation path
Function GetProcessorPath() As String
  GetProcessorPath = RegKeyRead(KEY_HIGHLIGHT_PROCESSOR_PATH, DEFVAL_HIGHLIGHT_PROCESSOR_PATH)
End Function

' set  processor installation path
Sub SetProcessorPath(strProcessorPath As String)
  RegKeySave KEY_HIGHLIGHT_PROCESSOR_PATH, strProcessorPath
End Sub

' check processor installation path for existence
Function IsProcessorPathExisting() As String
  IsProcessorPathExisting = RegKeyExists(KEY_HIGHLIGHT_PROCESSOR_PATH)
End Function

' get XML config file
Function GetXmlConfigFile() As String
  GetXmlConfigFile = RegKeyRead(KEY_XML_CONFIG_FILE, DEFVAL_XML_CONFIG_FILE)
End Function

' set XML config file
Sub SetXmlConfigFile(xmlConfigFile As String)
  RegKeySave KEY_XML_CONFIG_FILE, xmlConfigFile
End Sub

' get text box background
Function GetTextBoxBackground() As String
  GetTextBoxBackground = RegKeyRead(KEY_TEXTBOX_BACKGROUND, DEFVAL_TEXTBOX_BACKGROUND)
End Function

' set text box background
Sub SetTextBoxBackground(bgStrategy As String)
  RegKeySave KEY_TEXTBOX_BACKGROUND, bgStrategy
End Sub

' get text box frame
Function GetTextBoxFrame() As String
  GetTextBoxFrame = RegKeyRead(KEY_TEXTBOX_FRAME, DEFVAL_TEXTBOX_FRAME)
End Function

' set text box frame
Sub SetTextBoxFrame(frameStrategy As String)
  RegKeySave KEY_TEXTBOX_FRAME, frameStrategy
End Sub

' get background color
Function GetBackgroundColor(index As Integer, Optional lDefColor As Long = DEFVAL_COLOR) As Variant
  GetBackgroundColor = RegKeyRead(KEY_BACKGROUND_COLOR & index, lDefColor)
End Function

' Set background color
Sub SetBackgroundColor(index As Integer, lColor As Long)
  RegKeySave KEY_BACKGROUND_COLOR & index, lColor, "REG_DWORD"
End Sub

' Get frame color
Function GetFrameColor(index As Integer, Optional lDefColor As Long = DEFVAL_COLOR) As Variant
  GetFrameColor = RegKeyRead(KEY_FRAME_COLOR & index, lDefColor)
End Function

' Set frame color
Sub SetFrameColor(index As Integer, lColor As Long)
  RegKeySave KEY_FRAME_COLOR & index, lColor, "REG_DWORD"
End Sub

' get text box user defined font X
Function GetUserDefinedFont(index As Integer) As String
  GetUserDefinedFont = RegKeyRead(KEY_USER_DEFINED_FONT & index, "")
End Function

' set text box user defined X
Sub SetUserDefinedFont(index As Integer, fontName As String)
  RegKeySave KEY_USER_DEFINED_FONT & index, fontName
End Sub

' Get useXmlConfigFile
Function GetUseXmlConfigFile(Optional lUseXmlConfigFile As Long = 0) As Variant
  GetUseXmlConfigFile = RegKeyRead(KEY_USE_XML_CONFIG_FILE, lUseXmlConfigFile)
End Function

' Set useXmlConfigFile
Sub SetUseXmlConfigFile(lUseXmlConfigFile As Long)
  RegKeySave KEY_USE_XML_CONFIG_FILE, lUseXmlConfigFile, "REG_DWORD"
End Sub

' Get devModeActive
Function GetDevModeActive(Optional lDevModeActive As Long = 0) As Variant
  GetDevModeActive = RegKeyRead(KEY_DEV_MODE_ACTIVE, lDevModeActive)
End Function

' Set devModeActive
Sub SetDevModeActive(lDevModeActive As Long)
  RegKeySave KEY_DEV_MODE_ACTIVE, lDevModeActive, "REG_DWORD"
End Sub

' Get extendedConfigActive
Function GetExtendedConfigActive(Optional lExtendedConfigActive As Long = 0) As Variant
  GetExtendedConfigActive = RegKeyRead(KEY_EXTENDED_CONFIG_ACTIVE, lExtendedConfigActive)
End Function

' Set extendedConfigActive
Sub SetExtendedConfigActive(lExtendedConfigActive As Long)
  RegKeySave KEY_EXTENDED_CONFIG_ACTIVE, lExtendedConfigActive, "REG_DWORD"
End Sub

' Get devGuiLanguageId
Function GetDevGuiLanguageId(Optional lDevGuiLanguageId As Long = 0) As Variant
  GetDevGuiLanguageId = RegKeyRead(KEY_DEV_GUI_LANGUAGE_ID, lDevGuiLanguageId)
End Function

' Get logLevel (default: warn)
Function GetLogLevel(Optional eLogLevel As Integer = logLevel.lvlWarn) As Variant
  Dim logLevel As Variant
  
  logLevel = RegKeyRead(KEY_LOG_LEVEL, eLogLevel)
  
  ' if value is outside of accepted values, set default
  If (logLevel < lvlTrace Or logLevel > lvlInfo) Then
    logLevel = lvlWarn
  End If
    
  GetLogLevel = logLevel
End Function

' -----------------------------------------------
' get language preselection collection in order
' -----------------------------------------------
Sub GetPreselectedLanguages(ByRef colPreselectedLanguages As Collection)
  Dim strValue, strReg As String
  
  strValue = "lang-default"                      ' default is always set
  
  ' look into registry and add stored language Ids
  strReg = RegKeyRead(KEY_SELECTABLE_LANGUAGES, "")
  ESHLogging.logDebug "stored language ids: " & strReg, "GetPreselectedLanguages"
  
  If Len(strReg) > 0 Then
    strValue = strValue & "," & strReg
  End If
  
  ESHLogging.logDebug "use language ids: " & strValue, "GetPreselectedLanguages"
  
  ' split the ID list string
  Dim arrSplitLangIds As Variant
  arrSplitLangIds = Split(strValue, ",")
  ESHLogging.logDebug "number of language ids: " & UBound(arrSplitLangIds) + 1, "GetPreselectedLanguages"
       
  ' get collection of all available languages
  Dim colSupportedLang As Collection
  Set colSupportedLang = ESHPrefs.GetConfiguration.SupportedLanguages
  
  Dim index As Integer
  Dim oLangDef As ESHLanguageDefinition
  Dim sKey As String
  
  ' iterate over the sublist of chosen language IDs and add
  ' the full descriptions to the selectable collection
  For index = 0 To UBound(arrSplitLangIds)
    sKey = arrSplitLangIds(index)
    
    If ESHUtils.HasKey(colSupportedLang, sKey) Then
      Set oLangDef = colSupportedLang.Item(sKey)
      ESHLogging.logTrace "add language to preselection: " & oLangDef.id, "GetPreselectedLanguages"
      colPreselectedLanguages.Add oLangDef, oLangDef.id
    Else
      ESHLogging.logWarn "language id not found: " & sKey, "GetPreselectedLanguages"
    End If
  Next
  
End Sub

' -----------------------------------------------
' store preselection list of languages
' -----------------------------------------------
Sub SetIdsOfPreselectedLanguages(colList As Collection)
  Dim strValue As String
  
  Dim index As Integer
  Dim oItem As ESHLanguageDefinition
  
  For index = 1 To colList.count
    Set oItem = colList.Item(index)
    
    If StrComp(oItem.id, "lang-default") <> 0 Then
      strValue = strValue & oItem.id
      
      ' add a comma as separator
      If index < colList.count Then
        strValue = strValue & ","
      End If
      
    End If
  Next
  
  ESHLogging.logDebug "write preselected languages to registry: " & strValue, "SetIdsOfPreselectedLanguages"
  RegKeySave KEY_SELECTABLE_LANGUAGES, strValue
  
End Sub

' -----------------------------------------------
' get style preselection collection
' -----------------------------------------------
Sub GetPreselectedStyles(ByRef colPreselectedStyles As Collection)
  Dim strValue, strReg As String
  
  strValue = "style-default"                     ' default is always set
  
  ' look into registry and add stored style Ids
  strReg = RegKeyRead(KEY_SELECTABLE_STYLES, "")
  ESHLogging.logDebug "stored style ids: " & strReg, "GetPreselectedStyles"
  
  If Len(strReg) > 0 Then
    strValue = strValue & "," & strReg
  End If
  
  ESHLogging.logDebug "use style ids: " & strValue, "GetPreselectedStyles"
  
  ' split the ID list string
  Dim arrSplitStyleIds As Variant
  arrSplitStyleIds = Split(strValue, ",")
  
  ESHLogging.logDebug "number of style ids: " & UBound(arrSplitStyleIds) + 1, "GetPreselectedStyles"
  
  ' get collection of all available styles
  Dim colSupportedStyles As Collection
  Set colSupportedStyles = ESHPrefs.GetConfiguration.SupportedStyles
  
  Dim index As Integer
  Dim oStyleDef As ESHStyleDefinition
  Dim sKey As String
    
  ' iterate over the sublist of chosen style IDs and add
  ' the full descriptions to the selectable collection
  For index = 0 To UBound(arrSplitStyleIds)
    sKey = arrSplitStyleIds(index)
    
    If ESHUtils.HasKey(colSupportedStyles, sKey) Then
      Set oStyleDef = colSupportedStyles.Item(sKey)
      ESHLogging.logTrace "add style to preselection: " & oStyleDef.id, "GetPreselectedStyles"
      colPreselectedStyles.Add oStyleDef, oStyleDef.id
    Else
      ESHLogging.logWarn "style id not found: " & sKey, "GetPreselectedStyles"
    End If
  Next
  
End Sub

' -----------------------------------------------
' store preselection list of styles
' -----------------------------------------------
Sub SetIdsOfPreselectedStyles(colList As Collection)
  Dim strValue As String
  
  Dim index As Integer
  Dim oItem As ESHStyleDefinition
  
  For index = 1 To colList.count
    Set oItem = colList.Item(index)
    
    If StrComp(oItem.id, "style-default") <> 0 Then
      strValue = strValue & oItem.id
      
      ' add a comma as separator
      If index < colList.count Then
        strValue = strValue & ","
      End If
      
    End If
  Next
  
  ESHLogging.logDebug "write preselected styles to registry: " & strValue, "SetIdsOfPreselectedStyles"
  RegKeySave KEY_SELECTABLE_STYLES, strValue
  
End Sub

' -----------------------------------------------
' get font preselection collection in order
' -----------------------------------------------
Sub GetPreselectedFonts(ByRef colPreselectedFonts As Collection)
  Dim i As Long
 
  ' get collection of available fonts'
  Dim colSupportedFonts As Collection
  Set colSupportedFonts = ESHPrefs.GetConfiguration.SupportedFonts
  
  For i = 1 To colSupportedFonts.count
   colPreselectedFonts.Add colSupportedFonts(i)
  Next i
  
End Sub

' -----------------------------------------------
' internal helpers
' -----------------------------------------------

' reads the value for the registry key
' if the key cannot be found, the default value is returned
Private Function RegKeyRead( _
        strRegKey As String, _
        strDefault As Variant) As Variant
  
  Dim oWS As Object
  Dim strFQRegKey As String
  
  On Error GoTo ErrorHandler
  
  ' access Windows scripting
  Set oWS = CreateObject("WScript.Shell")
  strFQRegKey = REG_BASE & strRegKey
  
  ESHLogging.logTrace "read regkey: " & strFQRegKey, "RegKeyRead"
  
  ' read key from registry
  RegKeyRead = oWS.RegRead(strFQRegKey)
  
  Set oWS = Nothing
  Exit Function
  
ErrorHandler:
  ESHLogging.logTrace "use reg default: " & strDefault, "RegKeyRead"

  ' key was not found
  RegKeyRead = strDefault

End Function

' sets the registry key strRegKey to the value strValue with type strType
' if strType is omitted, the value will be saved as string.
' if strRegKey wasn't found, a new registry key will be created.
Private Sub RegKeySave( _
        strRegKey As String, _
        strValue As Variant, _
        Optional strType As String = "REG_SZ")

  Dim oWS As Object
  Dim strFQRegKey As String

  ' access Windows scripting
  Set oWS = CreateObject("WScript.Shell")
  strFQRegKey = REG_BASE & strRegKey
  
  ' write registry key
  oWS.RegWrite strFQRegKey, strValue, strType
  Set oWS = Nothing
  
  ESHLogging.logTrace "store regkey: " & strRegKey & "; value: " & strValue, "RegKeySave"
End Sub

'returns True if the registry key strRegKey was found
'and False if not
Function RegKeyExists( _
         strRegKey As String) As Boolean
  Dim oWS As Object
  Dim strFQRegKey As String
  
  On Error GoTo ErrorHandler
  
  'access Windows scripting
  Set oWS = CreateObject("WScript.Shell")
  strFQRegKey = REG_BASE & strRegKey
  
  ESHLogging.logTrace "check regkey: " & strFQRegKey, "RegKeyExists"
  
  'try to read the registry key
  oWS.RegRead strFQRegKey
  
  'key was found
  RegKeyExists = True
  Exit Function
  
ErrorHandler:
  'key was not found
  ESHLogging.logTrace "key " & strRegKey & " does not exist", "RegKeyExists"
  
  RegKeyExists = False
End Function


