Attribute VB_Name = "ESHLanguageDropdownMenu"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Language Dropdown Menu: handler for drop down menu
' ----------------------------------------------------------------------------
Option Explicit

' -------------------------------------
' constants
' -------------------------------------
Public Const LANGID_DEFAULT As String = "lang-default"

' -------------------------------------
' member variables
' -------------------------------------
Private objSharedLanguages As Collection

' --------------------------------------------
' get singleton list of selectable languages
' --------------------------------------------
Public Function GetLanguageList() As Collection

  If objSharedLanguages Is Nothing Then
  
    Set objSharedLanguages = New Collection
    ESHPrefs.GetPreselectedLanguages objSharedLanguages
     
  Else
    'Debug.Print "reuse shared language collection"
  End If

  Set GetLanguageList = objSharedLanguages

End Function

' --------------------------------------------
' invalidate the list after changes have done
' --------------------------------------------
Sub InvalidateLanguageList()
  ESHLogging.logTrace "invalidate language list", "InvalidateLanguageList"
  Set objSharedLanguages = Nothing
End Sub

' --------------------------------------------
' callbacks from the ribbon dropdown list
' --------------------------------------------

'Callback for ddLanguage getLabel
Sub GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_DD_LANGUAGE)
End Sub

'Callback for ddLanguage onAction
Sub OnAction(control As IRibbonControl, id As String, index As Integer)
  
  ESHPrefs.SetCurrentLanguageId id
    
  If bLoggingActive = True Then
    MsgBox "Language: " & id & " / index: " & index
  End If
  
End Sub

' Callback for ddLanguage getItemCount
Sub GetItemCount(control As IRibbonControl, ByRef count)
  count = GetLanguageList.count
End Sub

' Callback for ddLanguage getItemLabel
Sub GetItemLabel(control As IRibbonControl, index As Integer, ByRef label)
  Dim oLangDef As ESHLanguageDefinition
  Set oLangDef = GetLanguageList.Item(index + 1)
  label = oLangDef.name
End Sub

' Callback for ddLanguage getItemID
Sub GetItemID(control As IRibbonControl, index As Integer, ByRef id)
  Dim oLangDef As ESHLanguageDefinition
  Set oLangDef = GetLanguageList.Item(index + 1)
  id = oLangDef.id
End Sub

' Callback for ddLanguage getSelectedItemID
Sub GetSelectedItemID(control As IRibbonControl, ByRef index)
  index = ESHPrefs.GetCurrentLanguageId()
End Sub
