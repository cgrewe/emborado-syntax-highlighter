VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHLanguageDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Language definition class
' ----------------------------------------------------------------------------
Option Explicit

' Class member variables
Private m_name As String
Private m_id As String
Private m_parameter As String
Private m_preselected As Boolean

' Ctor event - triggered when class created
Private Sub Class_Initialize()
  m_preselected = False
End Sub

' Properties
Property Get name() As String
  name = m_name
End Property

Property Let name(value As String)
  m_name = value
End Property

Property Get id() As String
  id = m_id
End Property

Property Let id(value As String)
  m_id = value
End Property

Property Get parameter() As String
  parameter = m_parameter
End Property

Property Let parameter(value As String)
  m_parameter = value
End Property

Property Get preselected() As Boolean
  preselected = m_preselected
End Property

Property Let preselected(value As Boolean)
  m_preselected = value
End Property

Public Sub InitiateProperties(name As String, id As String, parameter As String, Optional preselected As Boolean)
  m_name = name
  m_id = id
  m_parameter = parameter
  m_preselected = preselected
End Sub

Public Function ToString() As String
  ToString = "language definition (" & m_name & ", " & m_id & ", " & m_parameter & ", " & m_preselected & ")"
End Function

