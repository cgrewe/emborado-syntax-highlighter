Attribute VB_Name = "ESHTBoxFrameDropdownMenu"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Text Box Frame Color Dropdown Menu: handler for drop down menu
' ----------------------------------------------------------------------------
Option Explicit

' constants
Public Const TBFID_NONE As String = "tbf-none"
Public Const TBFID_TRANSPARENT As String = "tbf-transparent"
Public Const TBFID_COLOR1 As String = "tbf-color1"
Public Const TBFID_COLOR2 As String = "tbf-color2"

' ----------------------------------------------------------
' callbacks from the ribbon frame dropdown list
' ----------------------------------------------------------

'Callback for GetLabel
Sub GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_DD_FRAME_STRATEGY)
End Sub

' Callback for GetItemCount
Sub GetItemCount(control As IRibbonControl, ByRef count)
  count = 4
End Sub

' Callback for GetItemLabel
Sub GetItemLabel(control As IRibbonControl, index As Integer, ByRef label)
  Select Case index
    Case 0
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_FC_NO_ASSIGNMENT)
    Case 1
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_FC_TRANSPARENT)
    Case 2
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_FC_COLOR1)
    Case 3
      label = ESHLocalization.GetMsg(MSGID_RIBBON_DDLBL_FC_COLOR2)
  End Select
End Sub

' Callback for GetItemID
Sub GetItemID(control As IRibbonControl, index As Integer, ByRef id)
  Select Case index
    Case 0
      id = TBFID_NONE
    Case 1
      id = TBFID_TRANSPARENT
    Case 2
      id = TBFID_COLOR1
    Case 3
      id = TBFID_COLOR2
  End Select
  
  'Debug.Print "GetItemID: " & index & " ; " & id
  
End Sub

' Callback for GetSelectedItemID
Sub GetSelectedItemID(control As IRibbonControl, ByRef index)
  ' get currently set FC
  Dim sFC As String
  sFC = ESHPrefs.GetTextBoxFrame
  
  ' check plausability of found id string
  Select Case sFC
    Case TBFID_NONE, TBFID_TRANSPARENT, TBFID_COLOR1, TBFID_COLOR2
      index = sFC
    Case Else
      index = TBFID_NONE
      ESHLogging.logWarn "found frame color id [" & sFC & "] is unknown.", "GetSelectedItemID"
  End Select
  
End Sub

' Callback for OnAction
Sub OnAction(control As IRibbonControl, id As String, index As Integer)

  ESHPrefs.SetTextBoxFrame id
    
  If bLoggingActive = True Then
    MsgBox "set frame strategy: " & id & " / index: " & index
  End If
    
End Sub
