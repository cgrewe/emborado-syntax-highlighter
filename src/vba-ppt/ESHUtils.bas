Attribute VB_Name = "ESHUtils"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Utils: additional helpers
' ----------------------------------------------------------------------------
Option Explicit

Private gHourglassCursor As Long
Private gPrevCursor As Long

' see https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-loadcursora
Private Const IDC_WAIT As Long = 32514

#If VBA7 Then
  Private Declare PtrSafe Function SetCursor Lib "user32" (ByVal hCursor As Long) As Long
  Private Declare PtrSafe Function LoadCursor Lib "user32" Alias "LoadCursorA" (ByVal hInstance As Long, ByVal lpCursorName As Any) As Long
  Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As LongPtr)
#Else
  Private Declare Function SetCursor Lib "USER32" (ByVal hCursor As Long) As Long
  Private Declare Function LoadCursor Lib "USER32" Alias "LoadCursorA" (ByVal hInstance As Long, ByVal lpCursorName As Any) As Long
  Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds as Long)
#End If

'
' check if a given key is inside a collection
'
Public Function HasKey(col As Collection, key As String) As Boolean

  On Error GoTo incol
  col.Item key

incol:
  HasKey = (Err.number = 0)

End Function

'
' Check if given file is existing in file system
'
Public Function FileExists(sFileName As String) As Boolean

  If sFileName = "" Then
    FileExists = False
  Else
    Dim objFSO As Object
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    FileExists = objFSO.FileExists(sFileName)
  End If
  
End Function

'
' Check if given folder is existing in file system
'
Public Function FolderExists(sFolderName As String) As Boolean

  Dim sFolderExists As String
    
  sFolderExists = Dir(sFolderName, vbDirectory)

  If sFolderExists = "" Then
    FolderExists = False
  Else
    FolderExists = True
  End If

End Function

'
' common file dialog for configuration text boxes
'
Public Sub LoadFolderDialogForTextBox( _
        ByRef textBox As textBox, _
        strDlgTitle As String)

  Dim f As Office.FileDialog
  Set f = Application.FileDialog(msoFileDialogFolderPicker)

  With f
    .Title = strDlgTitle
    .AllowMultiSelect = False                    ' only one file choosable
    .ButtonName = ESHLocalization.GetMsg(MSGID_UTILS_FOLDER_DLG_CHOOSE)  ' button title
    .Filters.Clear                               ' clean filters
    .InitialFileName = textBox.Text              ' start directory
    .Show
  End With

  ' take chosen folder
  If f.SelectedItems.count > 0 Then
    textBox.Text = f.SelectedItems(1)
  End If
  
End Sub

'
' common file dialog for configuration text boxes
'
Public Sub LoadFileDialogForTextBox( _
        ByRef textBox As textBox, _
        strDlgTitle As String, _
        strFilterName As String, _
        strFilter As String)

  Dim f As Office.FileDialog
  Set f = Application.FileDialog(msoFileDialogFilePicker)

  With f
    .Title = strDlgTitle
    .AllowMultiSelect = False                    ' only one file choosable
    .ButtonName = ESHLocalization.GetMsg(MSGID_UTILS_FILE_DLG_CHOOSE)    ' button title
    .Filters.Clear                               ' clean filters
    .Filters.Add strFilterName, strFilter        ' select first filter
    .FilterIndex = 1                             ' select first filter
    .InitialFileName = textBox.Text              ' start directory
    .Show
  End With

  ' take chosen file
  If f.SelectedItems.count > 0 Then
    textBox.Text = f.SelectedItems(1)
  End If
  
End Sub

' set mouse icon to busy state
Public Sub SetMouseIconStateBusy()
  gHourglassCursor = LoadCursor(0, IDC_WAIT)
  gPrevCursor = SetCursor(gHourglassCursor)
  Debug.Print "change mouse icon to busy!"
End Sub

' unset mouse icon busy state
Public Sub UnsetMouseIconStateBusy()
  Call SetCursor(gPrevCursor)
  Debug.Print "change mouse icon to normal"
End Sub

