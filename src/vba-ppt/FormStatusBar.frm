VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FormStatusBar 
   Caption         =   "In Bearbeitung ..."
   ClientHeight    =   1215
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   6120
   OleObjectBlob   =   "FormStatusBar.frx":0000
   StartUpPosition =   1  'Fenstermitte
End
Attribute VB_Name = "FormStatusBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' status bar form
' ----------------------------------------------------------------------------
Option Explicit

Private totalNumber As Integer
Private counter As Integer

' ===============================================================
' initialize
' ===============================================================
Private Sub UserForm_Initialize()
  SetLocalizedTexts
  
  DoEvents
End Sub
  
Private Sub SetLocalizedTexts()

  FormStatusBar.Caption = ESHLocalization.GetMsg(MSGID_STATUS)
  
End Sub

' ===============================================================
' business logic
' ===============================================================
Sub OpenStatusBar(total As Integer)

  counter = 0
  totalNumber = total
  
  With Me
    .lblProgress.Width = 0
    .frmProgress.Caption = "0 % erledigt"
    .Show vbModeless
  End With

End Sub

' progress by one step
Sub Progress()

  counter = counter + 1
  DoEvents
  
  With Me
    .lblProgress.Width = 260 * counter / totalNumber
    .frmProgress.Caption = Round((counter / totalNumber) * 100, 0) & " % " & ESHLocalization.GetMsg(MSGID_STATUS_FRM_DONE)
  End With

  If counter = totalNumber Then
    Unload Me
  End If
  
End Sub
