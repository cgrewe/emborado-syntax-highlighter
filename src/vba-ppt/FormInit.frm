VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FormInit 
   Caption         =   "Basiseinrichtung"
   ClientHeight    =   3510
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   9255.001
   OleObjectBlob   =   "FormInit.frx":0000
   StartUpPosition =   1  'Fenstermitte
End
Attribute VB_Name = "FormInit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' initialization form
' ----------------------------------------------------------------------------
Option Explicit

' ===============================================================
' initialize
' ===============================================================
Private Sub UserForm_Initialize()
  SetLocalizedTexts
  Set cmdHighlightPath.Picture = Application.CommandBars.GetImageMso("FileOpen", 16, 16)
  lblGeneralInfo.Caption = ESHBaseModule.ADDIN_NAME & " | " & ESHBaseModule.ADDIN_VERSION
End Sub

Private Sub SetLocalizedTexts()
  FormInit.Caption = ESHLocalization.GetMsg(MSGID_INIT)
  frmBasePath.Caption = ESHLocalization.GetMsg(MSGID_INIT_BASE_PATH)
  tbInfoBasePath.Text = ESHLocalization.GetMsg(MSGID_INIT_TEXT_INFO)
  cmdOK.Caption = ESHLocalization.GetMsg(MSGID_INIT_CMD_OK)
End Sub

' ===============================================================
' button and field logic
' ===============================================================
Private Sub cmdHighlightPath_Click()
  ESHUtils.LoadFolderDialogForTextBox tbHighlightPath, ESHLocalization.GetMsg(MSGID_INIT_DLG_HIGHLIGHT_PATH)
End Sub

Private Sub cmdOK_Click()
 FormInit.Hide                                  ' close dialog
 
 ' reset the predefined language and style
 ESHPrefs.SetCurrentLanguageId ESHLanguageDropdownMenu.LANGID_DEFAULT
 ESHPrefs.SetCurrentStyleId ESHStyleDropdownMenu.STYLEID_DEFAULT

 ' refresh controls in ribbon
 InvalidateControls
End Sub

Private Sub tbHighlightPath_Change()
  ESHPrefs.SetProcessorPath tbHighlightPath.Text
 
  ' refresh internal access state
  ESHExternalProcessor.CheckAccessState
 
  If ESHExternalProcessor.IsOkay Then
    tbHighlightPath.BackColor = &HC0FFC0
    cmdOK.enabled = True
  Else
    tbHighlightPath.BackColor = &HC0C0FF
    cmdOK.enabled = False
  End If
  
End Sub

