Attribute VB_Name = "ESHColorChooser"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Color Chooser: system dialog
'
' based on:
' https://docs.microsoft.com/en-us/windows/win32/dlgbox/color-dialog-box
' http://www.office-loesung.de/ftopic539534_0_0_asc.php
' https://jeffpar.github.io/kbarchive/kb/153/Q153929/
' ----------------------------------------------------------------------------
Option Explicit

#If VBA7 Then

Private Type ChooseColorType
  lStructSize               As Long
  hwndOwner                 As LongPtr
  hInstance                 As LongPtr
  rgbResult                 As Long
  lpCustColors              As LongPtr
  flags                     As Long
  lCustData                 As LongPtr
  lpfnHook                  As LongPtr
  lpTemplateName            As String
End Type

#Else

Private Type ChooseColorType
  lStructSize               As Long
  hwndOwner                 As Long
  hInstance                 As Long
  rgbResult                 As Long
  lpCustColors              As Long
  flags                     As Long
  lCustData                 As Long
  lpfnHook                  As Long
  lpTemplateName            As String
End Type

#End If

Private Const CC_ANYCOLOR = &H100
Private Const CC_ENABLEHOOK = &H10
Private Const CC_ENABLETEMPLATE = &H20
Private Const CC_ENABLETEMPLATEHANDLE = &H40
Private Const CC_FULLOPEN = &H2
Private Const CC_PREVENTFULLOPEN = &H4
Private Const CC_RGBINIT = &H1
Private Const CC_SHOWHELP = &H8
Private Const CC_SOLIDCOLOR = &H80
 
#If VBA7 Then
Private Declare PtrSafe Function ChooseColor_Dlg Lib "comdlg32.dll" Alias "ChooseColorA" (pChoosecolor As ChooseColorType) As Long
#Else
Private Declare Function ChooseColor_Dlg Lib "comdlg32.dll" Alias "ChooseColorA" (pChoosecolor As ChooseColorType) As Long
#End If

'
' Color chooser
'
' VBA adapter to standard color chooser dialog.
'
' IN:  default color
' OUT: chosen color
'
Public Function DialogColor(ByVal lDefaultColor As Long) As Long
  Dim CC_T                  As ChooseColorType
  Dim lRetVal               As Long
  Static CustomColors(16)   As Long
 
  With CC_T
    .lStructSize = LenB(CC_T)
    .flags = CC_ANYCOLOR Or CC_FULLOPEN Or CC_PREVENTFULLOPEN Or CC_RGBINIT Or CC_SHOWHELP
    .rgbResult = lDefaultColor                   ' set the initial color of the dialog
    .lpCustColors = VarPtr(CustomColors(0))
  End With
  
  lRetVal = ChooseColor_Dlg(CC_T)
  
  If lRetVal = 0 Then                            ' cancelled by the user
    DialogColor = -1
  Else
    DialogColor = CC_T.rgbResult
  End If
  
End Function

'
' local test
'
Public Sub Demo()
  Dim color As Long
  color = &HFF4455
  color = DialogColor(color)
  Debug.Print "farbe: " & color
End Sub

