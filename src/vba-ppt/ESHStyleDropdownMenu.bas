Attribute VB_Name = "ESHStyleDropdownMenu"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Style Dropdown Menu: handler for drop down menu
' ----------------------------------------------------------------------------
Option Explicit

' -------------------------------------
' constants
' -------------------------------------
Public Const STYLEID_DEFAULT As String = "style-default"

' -------------------------------------
' member variables
' -------------------------------------
Private objSharedStyles As Collection

' --------------------------------------------
' get singleton list of selectable languages
' --------------------------------------------
Public Function GetStyleList() As Collection

  If objSharedStyles Is Nothing Then
  
    Set objSharedStyles = New Collection
    ESHPrefs.GetPreselectedStyles objSharedStyles
     
  Else
    'Debug.Print "reuse shared style collection"
  End If

  Set GetStyleList = objSharedStyles

End Function

' --------------------------------------------
' invalidate the list after changes have done
' --------------------------------------------
Sub InvalidateStyleList()
  ESHLogging.logTrace "invalidate style list", "InvalidateStyleList"
  Set objSharedStyles = Nothing
End Sub

' --------------------------------------------
' callbacks from the ribbon dropdown list
' --------------------------------------------

'Callback for ddStyle getLabel
Sub GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_DD_STYLE)
End Sub

'Callback for ddStyle onAction
Sub OnAction(control As IRibbonControl, id As String, index As Integer)

  ESHPrefs.SetCurrentStyleId id
    
  If bLoggingActive = True Then
    MsgBox "Style: " & id & " / index: " & index
  End If
  
End Sub

' Callback for ddStyle getItemCount
Sub GetItemCount(control As IRibbonControl, ByRef count)
  count = GetStyleList.count
End Sub

' Callback for ddStyle getItemLabel
Sub GetItemLabel(control As IRibbonControl, index As Integer, ByRef label)
  Dim oStyleDef As ESHStyleDefinition
  Set oStyleDef = GetStyleList.Item(index + 1)
  label = oStyleDef.name
End Sub

' Callback for ddStyle getItemID
Sub GetItemID(control As IRibbonControl, index As Integer, ByRef id)
  Dim oStyleDef As ESHStyleDefinition
  Set oStyleDef = GetStyleList.Item(index + 1)
  id = oStyleDef.id
End Sub

' Callback for ddStyle getSelectedItemID
Sub GetSelectedItemID(control As IRibbonControl, ByRef index)
  index = ESHPrefs.GetCurrentStyleId()
End Sub

