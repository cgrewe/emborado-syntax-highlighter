VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHSystemFont"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' system font definition class
' this class is used for reading fonts from the system (name, monospace)
' ----------------------------------------------------------------------------
Option Explicit

' Class member variables
Private m_name As String
Private m_monospace As Boolean

' Ctor event - triggered when class created
Private Sub Class_Initialize()
  m_monospace = False
End Sub

' Properties
Property Get name() As String
  name = m_name
End Property

Property Let name(value As String)
  m_name = value
End Property

Property Get monospace() As Boolean
  monospace = m_monospace
End Property

Property Let monospace(value As Boolean)
  m_monospace = value
End Property

Public Sub InitiateProperties(name As String, monospace As Boolean)
  m_name = name
  m_monospace = monospace
End Sub

Public Function ToString() As String
  ToString = "system font (" & m_name & ", " & m_monospace & ")"
End Function

