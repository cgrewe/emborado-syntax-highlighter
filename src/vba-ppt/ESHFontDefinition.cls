VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ESHFontDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' font definition class
' this class is used for font assignment (name, id)
' ----------------------------------------------------------------------------
Option Explicit

' Class member variables
Private m_name As String
Private m_id As String


' Ctor event - triggered when class created
Private Sub Class_Initialize()
End Sub

' Properties
Property Get name() As String
  name = m_name
End Property

Property Let name(value As String)
  m_name = value
End Property

Property Get id() As String
  id = m_id
End Property

Property Let id(value As String)
  m_id = value
End Property

Public Sub InitiateProperties(name As String, id As String)
  m_name = name
  m_id = id
End Sub

Public Function ToString() As String
  ToString = "font definition (" & m_name & ", " & m_id & ")"
End Function
