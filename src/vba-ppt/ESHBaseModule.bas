Attribute VB_Name = "ESHBaseModule"
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' Base Module: initialisation of ribbon and callbacks for ribbon events
' ----------------------------------------------------------------------------
Option Explicit

' constants
Public Const ADDIN_NAME As String = "Emborado Syntax Highlighter"
Public Const ADDIN_VERSION As String = "V1.2.0"

' global controlling flags
Public bLoggingActive As Boolean               ' logging active?
Public bShowHLProcessor As Boolean             ' show highlight processor?
Private bHighlightJobRunning As Boolean        ' running job active?

Dim objRibbon As IRibbonUI

' ----------------------------------------
' Set default values while loading ribbon
' ----------------------------------------
Sub OnLoad(ribbon As IRibbonUI)

  Set objRibbon = ribbon
    
  Dim accessState As ProcessorAccessState
  accessState = ESHExternalProcessor.CheckAccessState
  
  ' check if important registry entries are blank
  ' this happens right after installation, before user did initial configurations
  If accessState = ProcessorAccessState.noConfiguration Then
    ESHLogging.logInfo "the plugin has not been configured yet", "OnLoad"
    MsgBox ESHLocalization.GetMsg(MSGID_BASE_WELCOME), vbInformation, ADDIN_NAME
    FormInit.Show
    accessState = ESHExternalProcessor.CheckAccessState
  End If
      
  bHighlightJobRunning = False
  bLoggingActive = False
  bShowHLProcessor = False
  
  ESHLogging.logDebug "onload / style: " & GetCurrentStyleId & "; language: " & GetCurrentLanguageId, "OnLoad"
  
  ' another simple check
  If objRibbon Is Nothing Then
    ESHLogging.logError "ribbon was not correctly passed!", "OnLoad"
  Else
    ESHLogging.logInfo "ribbon successfully loaded", "OnLoad"
    ESHLogging.logInfo "processor access state: " & accessState, "OnLoad"
  End If
  
End Sub

' ----------------------------------------
' invalidation of controls enforces their
' "repainting" with all made changes
' ----------------------------------------
Public Sub InvalidateControls()
  objRibbon.InvalidateControl ("btnHighlight")
  objRibbon.InvalidateControl ("btnImportHighlighted")
  objRibbon.InvalidateControl ("ddLanguage")
  objRibbon.InvalidateControl ("ddStyle")
  objRibbon.InvalidateControl ("ddFontType")
  objRibbon.InvalidateControl ("ddBackground")
  objRibbon.InvalidateControl ("ddFrame")
End Sub

' ----------------------------------------
' state of ribbon visibility
' ----------------------------------------

' Callback for highlight group buttons getEnable
' The buttons are disabled as long as the basic path is not set.
' This happens at the very first start and when the
' registry contains valid path.
'
Sub grpHighlight_ButtonsGetEnabled(control As IRibbonControl, ByRef visible)
  visible = ESHExternalProcessor.IsOkay
End Sub

' Callback for group development isVisible
' This group is usually disable. The user can enable it by
' a registry key. There is no invalidation logic.
'
Sub grpDevelopment_IsVisible(control As IRibbonControl, ByRef visible)
  visible = (ESHPrefs.GetDevModeActive = 1)
End Sub

' Callback for tab highlight getLabel
Sub tabHighlight_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_MENUE_TAB_HEADER)
End Sub

' Callback for grpHighlight getLabel
Sub grpHighlight_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_GRP_HIGHLIGHT)
End Sub

'Callback for grpFont getLabel
Sub grpFont_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_GRP_FONT)
End Sub

' ----------------------------------------
' Callback for btnLogging onAction
' ----------------------------------------
Sub btnLogging_OnAction(control As IRibbonControl, pressed As Boolean)
  bLoggingActive = pressed
  ESHLogging.logInfo "logging state: " & bLoggingActive, "btnLogging_OnAction"
  
  ' invalidate dropdown for log level selection
  objRibbon.InvalidateControl ("ddLogLevel")
End Sub

' ----------------------------------------
' Callback for btnShowHLProcessor onAction
' ----------------------------------------
Sub btnShowHLProcessor_OnAction(control As IRibbonControl, pressed As Boolean)
  bShowHLProcessor = pressed
  ESHLogging.logInfo "show highlight processor? " & bShowHLProcessor, "btnShowHLProcessor_OnAction"
End Sub

' ----------------------------------------
' Callbacks for btnHighlight
' ----------------------------------------
' Callback for btnHighlight getLabel
Sub btnHighlight_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_BTN_HIGHLIGHT)
End Sub

' Callback for btnHighlight onAction
Sub btnHighlight_OnAction(control As IRibbonControl)
  
  If (bHighlightJobRunning) Then
    ESHLogging.logWarn "there is already a running highlight job active", "btnHighlight_OnAction"
    Exit Sub
  End If
  
  bHighlightJobRunning = True
  
  Dim lColor As Long
  Dim oTextShape As Shape
  
  ' extract a single shape with a text frame inside
  With ActiveWindow.Selection
      
    ' check if any selection if existing
    If .Type = ppSelectionShapes Then
          
      ' check number of selected objects
      If .ShapeRange.count <> 1 Then
        MsgBox ESHLocalization.GetMsg(MSGID_BASE_ONLY_ONE_TEXT_OBJECT) & _
        .ShapeRange.count & ".", _
        vbCritical, ADDIN_NAME
        bHighlightJobRunning = False
        Exit Sub
      End If
          
      ' there are different text boxes out there
      Select Case .ShapeRange.Type
      
      Case msoTextBox
        Set oTextShape = .ShapeRange(1)
        ESHLogging.logDebug "found text box shape", "btnHighlight_OnAction"
          
      Case msoAutoShape
        Set oTextShape = .ShapeRange(1)
        ESHLogging.logDebug "found auto shape", "btnHighlight_OnAction"
          
      Case msoGroup
        If .HasChildShapeRange Then
          If .ChildShapeRange.count = 1 Then
            ESHLogging.logDebug "single shape selected inside group", "btnHighlight_OnAction"
              
            Set oTextShape = .ChildShapeRange(1)
            ' check if selected shape is text box or auto shape
            If (oTextShape.Type <> msoTextBox And oTextShape.Type <> msoAutoShape) Then
              ESHLogging.logDebug "found single shape inside group is neither text box nor auto shape", "btnHighlight_OnAction"
            End If
              
              
          Else
            ESHLogging.logDebug "multiple shapes selected inside group", "btnHighlight_OnAction"
          End If
        End If
          
      End Select
        
      ' did we find a single text shape object?
      If oTextShape Is Nothing Then
        ESHLogging.logWarn "found other shape object of type: " & .ShapeRange.Type, "btnHighlight_OnAction"
        MsgBox ESHLocalization.GetMsg(MSGID_BASE_FOUND_NO_TEXT_OBJECT), vbCritical, ADDIN_NAME
        bHighlightJobRunning = False
        Exit Sub
      End If
           
      ' does the found shape really contains text?
      If oTextShape.HasTextFrame = msoFalse Then
        MsgBox ESHLocalization.GetMsg(MSGID_BASE_FOUND_NO_TEXT_IN_OBJECT), vbCritical, ADDIN_NAME
        bHighlightJobRunning = False
        Exit Sub
      End If
    
    Else
      ESHLogging.logWarn "found other selection object of type: " & .Type, "btnHighlight_OnAction"
      MsgBox ESHLocalization.GetMsg(MSGID_BASE_FOUND_WRONG_OBJECT_TYPE), vbCritical, ADDIN_NAME
      bHighlightJobRunning = False
      Exit Sub
    End If
        
  End With
    
  ' ----------------------------------------------------------
  ' at this point oTextShape is existing and contains text!
  ' let's tranform it!
  ' ----------------------------------------------------------
  
  With oTextShape
      
    ' copy current content to clipboard
    oTextShape.TextFrame.TextRange.Copy
                     
    ' run external highlight processor script
    ESHExternalProcessor.RunHighlightProcessor bShowHLProcessor
     
    ' now a very strange part.
    ' We must wait for the clipboard, otherwise we get a 80004005 runtime error
    ' see: https://stackoverflow.com/questions/19186146/pastespecial-of-object-shapes-failed-vba
    DoEvents
    Sleep 500
    
    ' copy back the highlighted text to the selected text box
    .TextFrame.TextRange.Text = ""
    .TextFrame.TextRange.PasteSpecial ppPasteRTF

    ' correction for leading space in created RTF string
    ' TODO: no idea what intention the additional space is
    Dim foundLeadingSpace As TextRange
    Set foundLeadingSpace = .TextFrame.TextRange.Find(FindWhat:=" ")
    If Not (foundLeadingSpace Is Nothing) Then foundLeadingSpace.Delete

    ' -- several settings for bg, frame and fonts --
    Call Postprocessing(oTextShape)

  End With
  
  bHighlightJobRunning = False
End Sub

' ----------------------------------------
' Callbacks for btnImportHighlighted
' ----------------------------------------

' Callback for btnImportHighlighted getLabel
Sub btnImportHighlighted_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_BTN_IMPORT)
End Sub

' Callback for btnImportHighlighted onAction
Sub btnImportHighlighted_OnAction(control As IRibbonControl)
  
 If (bHighlightJobRunning) Then
    ESHLogging.logWarn "there is already a running highlight job active", "btnHighlight_OnAction"
    Exit Sub
  End If
  
  bHighlightJobRunning = True
  
  Dim myDocument As SlideRange
  Dim oTextShape As Shape
   
  ' run external highlight processor script
  ESHExternalProcessor.RunHighlightProcessor bShowHLProcessor
      
  ' now a very strange part.
  ' We must wait for the clipboard, otherwise we get a 80004005 runtime error
  ' see: https://stackoverflow.com/questions/19186146/pastespecial-of-object-shapes-failed-vba
  DoEvents
  Sleep 500
    
  ' create new text box
  Set myDocument = ActiveWindow.Selection.SlideRange
  Set oTextShape = myDocument.Shapes.AddTextbox(msoTextOrientationHorizontal, 100, 100, 600, 50)
            
  ' take over highlighted text
  'oTextShape.TextFrame.TextRange.InsertAfter(" ").PasteSpecial ppPasteRTF
  oTextShape.TextFrame.TextRange.PasteSpecial ppPasteRTF
  oTextShape.TextFrame.TextRange.Font.Size = 12
       
  ' correction for leading space in created RTF string
  ' TODO: no idea what intention the additional space is
  Dim foundLeadingSpace As TextRange
  Set foundLeadingSpace = oTextShape.TextFrame.TextRange.Find(FindWhat:=" ")
  If Not (foundLeadingSpace Is Nothing) Then foundLeadingSpace.Delete
    
  ' -- several settings for bg, frame and fonts --
  Call Postprocessing(oTextShape)
 
  bHighlightJobRunning = False
  
End Sub

' ----------------------------------------
' Callbacks for btnPrefs
' ----------------------------------------

' Callback for btnPrefs getLabel
Sub btnPrefs_GetLabel(control As IRibbonControl, ByRef label)
  label = ESHLocalization.GetMsg(MSGID_RIBBON_BTN_PREFS)
End Sub

' Callback for btnPrefs onAction
Sub btnPrefs_OnAction(control As IRibbonControl)
 If ESHExternalProcessor.GetAccessState = ProcessorAccessState.noConfiguration Then
    ESHLogging.logWarn "not correctly configured before", "btnPrefs_OnAction"
    FormInit.Show                      ' shows basic preferences dialog
  Else
    FormPrefs.Show                     ' shows normal preferences dialog
  End If
End Sub

' ----------------------------------------
' Callback for ddLogLevel getSelectedItemID
' ----------------------------------------
Sub ddLogLevel_GetSelectedItemID(control As IRibbonControl, ByRef index)
  ' Debug.Print "level: " & HLLogging.logger.level

  Select Case ESHLogging.logger.level
    Case lvlInfo
      index = "ll-info"
    Case lvlError
      index = "ll-error"
    Case lvlWarn
      index = "ll-warn"
    Case lvlDebug
      index = "ll-debug"
    Case lvlTrace
      index = "ll-trace"
    Case Else
      index = "ll-warn"
  End Select
  
End Sub

' ----------------------------------------
' Callback for ddLogLevel onAction
' ----------------------------------------
Sub ddLogLevel_OnAction(control As IRibbonControl, id As String, index As Integer)

  Dim eLogLevel As logLevel
  
  Select Case id
    Case "ll-info"
      eLogLevel = logLevel.lvlInfo
    Case "ll-error"
      eLogLevel = logLevel.lvlError
    Case "ll-warn"
      eLogLevel = logLevel.lvlWarn
    Case "ll-debug"
      eLogLevel = logLevel.lvlDebug
    Case "ll-trace"
      eLogLevel = logLevel.lvlTrace
  End Select

  ESHLogging.logger.level = eLogLevel
  
  ESHLogging.logInfo "log level changed to: " & eLogLevel
End Sub

' ----------------------------------------
' Callback for ddLogLevel getEnabled
' ----------------------------------------
Sub ddLogLevel_IsButtonLoggingEnabled(control As IRibbonControl, ByRef enabled)
  enabled = bLoggingActive
End Sub

' ===========================================
' I N T E R N A L    H E L P E R
' ===========================================

' ----------------------------------------
' sets background, font, frame etc.
' ----------------------------------------
Private Sub Postprocessing(oTextShape As Shape)
  Dim lColor As Long

  With oTextShape
     
    ' -- check background strategy --
    Select Case ESHPrefs.GetTextBoxBackground
    Case TBBGID_NONE
      ESHLogging.logDebug "no changes to background", "Postprocessing"
        
    Case TBBGID_TRANSPARENT
      ESHLogging.logDebug "set transparent background", "Postprocessing"
      oTextShape.Fill.Transparency = 1
        
      ' -- set according to style --
    Case TBBGID_STYLE
      
      Dim oStyleDef As ESHStyleDefinition
      Set oStyleDef = GetCurrentStyleDefinition
                      
      lColor = oStyleDef.BackgroundColorAsRGB
        
      ESHLogging.logDebug "set background color according to style: " & lColor, "Postprocessing"
      oTextShape.Fill.ForeColor.RGB = lColor
      oTextShape.Fill.Transparency = 0

        
      ' -- set to color definitions --
    Case TBBGID_COLOR1
      lColor = ESHPrefs.GetBackgroundColor(1)
      oTextShape.Fill.ForeColor.RGB = lColor
      oTextShape.Fill.Transparency = 0

    Case TBBGID_COLOR2
      lColor = ESHPrefs.GetBackgroundColor(2)
      oTextShape.Fill.ForeColor.RGB = lColor
      oTextShape.Fill.Transparency = 0
      
    End Select
          
    FormStatusBar.Progress
    
    ' -- set frame --
    Select Case ESHPrefs.GetTextBoxFrame
      
    Case TBFID_NONE
      ESHLogging.logDebug "don't change frame line", "Postprocessing"
      
    Case TBFID_TRANSPARENT
      oTextShape.Line.visible = False
      ESHLogging.logDebug "set frame line invisible", "Postprocessing"
       
    Case TBFID_COLOR1
      oTextShape.Line.ForeColor.RGB = ESHPrefs.GetFrameColor(1)
      oTextShape.Line.visible = True
      ESHLogging.logDebug "set frame color 1: " & ESHPrefs.GetFrameColor(1), "Postprocessing"
      
    Case TBFID_COLOR2
      oTextShape.Line.ForeColor.RGB = ESHPrefs.GetFrameColor(2)
      oTextShape.Line.visible = True
      ESHLogging.logDebug "set frame color 2: " & ESHPrefs.GetFrameColor(2), "Postprocessing"
        
    End Select
    
    FormStatusBar.Progress
    
    ' -- set font type --
    Dim curFontId, curFontName As String
    Dim oFontDef As ESHFontDefinition
    curFontId = ESHPrefs.GetCurrentFontId
    
    Set oFontDef = ESHPrefs.GetCurrentFontDefinition
    
    Select Case curFontId
      Case FONTID_NONE
      ' do nothing!
      
      Case Else
      .TextFrame.TextRange.Font.name = oFontDef.name
       ESHLogging.logDebug "set font " & oFontDef.name, "Postprocessing"
                
    End Select
      
    FormStatusBar.Progress
    
  End With
 
End Sub

' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  OTHER TEST STUFF FOR DEV TESTS
' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Callback for SelectionInfo onAction
Sub SelectionInfo(control As IRibbonControl)

  Dim n As Integer

  Debug.Print "-=-=---------------------------------------"
  Debug.Print "+ type: " & ActiveWindow.Selection.Type
    
  With ActiveWindow.Selection
    
    If .Type = ppSelectionShapes Then
      Debug.Print "# number of selected shapes: " & .ShapeRange.count
      
      If .HasChildShapeRange Then
        Debug.Print "# number of children: " & .ChildShapeRange.count
          
        If .ChildShapeRange.count = 1 Then
          MsgBox "type: " & .ChildShapeRange(1).Type & vbCrLf & _
                                                     "text: " & .ChildShapeRange(1).HasTextFrame & vbCrLf & _
                                                     "textBox: " & (msoTextBox = .ChildShapeRange(1).Type Or msoAutoShape = .ChildShapeRange(1).Type)
            
        End If
          
      End If
      
      For n = 1 To .ShapeRange.count
        Debug.Print "# (" & n & "): " & .ShapeRange(n).Type
        Debug.Print "# (" & n & "): text frame = " & .ShapeRange(n).HasTextFrame
        
      Next n

    Else
      Debug.Print "# number of selected shapes: " & .ShapeRange.count
    End If
  End With
    
  Dim str As String
  Select Case TypeName(ActiveWindow.Selection)
  Case "Nothing"
    str = "No selection made."
  Case "Range"
    str = "You selected the range: "             ' & ActiveWindow.Selection.Address
  Case "Picture"
    str = "You selected a picture."
  Case Else
    str = "You selected a " & TypeName(ActiveWindow.Selection) & "."
  End Select
  'MsgBox str
  Debug.Print str
    
End Sub

'Callback for ButtonTest5 onAction
Sub CheckReg(control As IRibbonControl)
  Dim i As Integer
  Dim h As ESHConfiguration
  Set h = ESHPrefs.GetConfiguration()
  

  ''''''''''''''''''''''''''''''''''
  Dim b As Boolean
  b = True
  If objRibbon Is Nothing Then
    b = False
  End If
  
  
  Debug.Print "objRibbon: " & b
  ''''''''''''''''''''''''''''''
  
  Dim lang  As ESHLanguageDefinition
  Set lang = New ESHLanguageDefinition
  
  'lang.InitiateProperties "XML", "lang-xml", "xml"
  
  lang.InitiateProperties "Groovy", "lang-groovy", "groovy"
  
  Dim ll As Collection
  Set ll = ESHLanguageDropdownMenu.GetLanguageList
  
  Debug.Print "old lang: " & ll.count
  For i = 1 To ll.count
    Debug.Print "qq: " & ll.Item(i).id
  Next
  
  ll.Add lang, lang.id
   
  Debug.Print "new lang: " & ll.count
   
   
  ESHPrefs.SetIdsOfPreselectedLanguages ll
  ESHLanguageDropdownMenu.InvalidateLanguageList
 
End Sub

