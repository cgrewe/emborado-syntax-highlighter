VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FormFontSelector 
   Caption         =   "Font Selector"
   ClientHeight    =   6810
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4755
   OleObjectBlob   =   "FormFontSelector.frx":0000
   StartUpPosition =   1  'Fenstermitte
End
Attribute VB_Name = "FormFontSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' ----------------------------------------------------------------------------
' Emborado Syntax Highlighter, (C) 2022-2024 by C. Grewe
'
' This software is released under the terms of the GNU General Public License.
'
' font selector form
' ----------------------------------------------------------------------------
Option Explicit

' ===============================================================
' member variables
' ===============================================================
Private m_Result As String
Private m_Cancelled As Boolean

' ===============================================================
' initialize
' ===============================================================
Private Sub UserForm_Initialize()
  cmdFontSelectorOkay.enabled = False
  m_Result = ""
  m_Cancelled = False
  
  SetLocalizedTexts
  
End Sub
  
Private Sub SetLocalizedTexts()

  FormFontSelector.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL)
  frmFontPreview.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL_FRM_FONT_PREVIEW)
  lblColumnFontName.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL_LBL_COLUMN_FONT)
  lblColumnMonospace.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL_LBL_COLUMN_MONOSPACE)
  cmdFontSelectorOkay.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL_CMD_OK)
  cmdFontSelectorCancel.Caption = ESHLocalization.GetMsg(MSGID_FNTSEL_CMD_CANCLEL)
  
End Sub

' ===============================================================
' activate
' ===============================================================
Private Sub UserForm_Activate()
  Dim i As Integer
  Dim colSystemFonts As Collection
  Dim oSysFont As ESHSystemFont
  Dim mono As Boolean
  
  Set colSystemFonts = ESHSystemFontUtils.GetSystemFonts
  
  ' brutal, but effective.
  lstAvailableFonts.Clear
  
  For i = 1 To colSystemFonts.count
    Set oSysFont = colSystemFonts.Item(i)
    
    lstAvailableFonts.AddItem
    lstAvailableFonts.List(i - 1, 0) = oSysFont.name

    If (oSysFont.monospace = True) Then
      lstAvailableFonts.List(i - 1, 1) = "*"
    End If
  Next
 
  ' reset font for preview area
  lblPreview.Font = "Tahoma" ' TODO: default font given somewhere?
  m_Cancelled = False
  
  ESHLogging.logTrace "font selector activated", "UserForm_Activate"
  
End Sub

' ===============================================================
' internal functions
' ===============================================================
Private Sub lstAvailableFonts_Click()
  cmdFontSelectorOkay.enabled = True
  m_Result = lstAvailableFonts.List(lstAvailableFonts.ListIndex)
  lblPreview.Font = m_Result
  ESHLogging.logTrace "font selected: " & m_Result, "lstAvailableFonts_Click"
End Sub

Private Sub cmdFontSelectorCancel_Click()
  m_Result = ""
  m_Cancelled = True
  Me.Hide
End Sub

Private Sub cmdFontSelectorOkay_Click()
  Me.Hide
End Sub

' special logic for cancelation by X
' the problem is the missing return value, that is needed by the caller
' https://excelmacromastery.com/
Private Sub UserForm_QueryClose(Cancel As Integer, _
                                CloseMode As Integer)
    
  ' Prevent the form being unloaded
  If CloseMode = vbFormControlMenu Then Cancel = True
    
  ' Hide the Userform and set cancelled to true
  Hide
  m_Cancelled = True
    
End Sub

' ===============================================================
' properties
' ===============================================================
Public Property Get Result() As String
  Result = m_Result
End Property

Public Property Get Cancelled() As Variant
  Cancelled = m_Cancelled
End Property

' ===============================================================
' internal helpers
' ===============================================================


