<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
  <xsl:strip-space elements="*"/>   
  <xsl:param name="quote">"</xsl:param>
     
  <xsl:template match="languages/language">
  AddLanguage colLanguages, <xsl:value-of select="$quote"/><xsl:value-of select="@name"/><xsl:value-of select="$quote"/>, <xsl:value-of select="$quote"/><xsl:value-of select="@id"/><xsl:value-of select="$quote"/>, <xsl:value-of select="$quote"/><xsl:value-of select="@parameter"/><xsl:value-of select="$quote"/>
  </xsl:template>	
</xsl:stylesheet>